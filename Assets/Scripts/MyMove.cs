﻿using UnityEngine;
using System.Collections;

public class MyMove : MonoBehaviour
{
	public float horizontalSpeed = 2.0F;
	public float verticalSpeed = 2.0F;

	void Update()
	{
		float h = horizontalSpeed * Input.GetAxis("Mouse X");
		float v = -verticalSpeed * Input.GetAxis("Mouse Y");
		gameObject.transform.Rotate(v, h, 0);
//		Vector3 newRotation = gameObject.transform.rotation.eulerAngles;
//		newRotation.z = 0;
//		gameObject.transform.rotation = Quaternion.Euler(newRotation);
	}
}