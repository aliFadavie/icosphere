﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FixMesh : MonoBehaviour {
	private Vector2[] newUV;
	void Start() {
		Mesh mesh = GetComponent<MeshFilter>().mesh;

		Vector3[] normals = mesh.normals;
		for (int i=0;i<normals.Length;i++)
			normals[i] = -normals[i];
		mesh.normals = normals;

		int[] triangles = mesh.triangles;
		for (int i=0;i<triangles.Length;i+=3){
			int temp = triangles[i];
			triangles[i] = triangles[i + 1];
			triangles[i + 1] = temp;
		}
		mesh.triangles=triangles;
		newUV = new Vector2[mesh.vertices.Length];
		TextCoord (mesh);
		mesh.uv = newUV;


		//CorrectSeam (mesh);
		//correctPole (mesh);

	}
	private void TextCoord(Mesh _mesh){
		for (int i = 0; i < _mesh.vertices.Length; i++) {
				
			Vector3 v = _mesh.vertices[i];

			newUV[i].x = 0.5f + Mathf.Atan2 (v.z, v.x) / (2.0f * Mathf.PI);
			newUV[i].y = 0.5f - Mathf.Asin (v.y) / Mathf.PI;
		}
	} 

	private int[] DetectWrappedUVCoordinates(Mesh mesh)
	{
		List<int> indices = new List<int>();
		for (int i = 0; i < mesh.triangles.Length; i+=3)
		{
			int a = mesh.triangles[i];
			int b = mesh.triangles[i+1];
			int c = mesh.triangles[i + 2];

			Vector3 texA = new Vector3(mesh.uv [a].x,mesh.uv [a].y, 0);
			Vector3 texB = new Vector3(mesh.uv [b].x,mesh.uv [b].y, 0);
			Vector3 texC = new Vector3(mesh.uv [c].x,mesh.uv [c].y, 0);
			Vector3 texNormal = Vector3.Cross(texB - texA, texC - texA);
			if (texNormal.z < 0) {
				indices.Add (i);
			}
		}
		return indices.ToArray();
	}
	private void FixWrappedUV(int[] wrapped, Mesh sphereMesh)
	{
		List<Vector3> vertices = new List<Vector3>(sphereMesh.vertices);
		List<Vector2> UVs = new List<Vector2>(sphereMesh.uv);

		int verticeIndex = vertices.Count - 1;
		Dictionary<int, int> visited = new Dictionary<int, int>();
		foreach (int i in wrapped)
		{
			int a = sphereMesh.triangles[i];
			int b = sphereMesh.triangles[i+1];
			int c = sphereMesh.triangles[i+2];
			Vector3 A = sphereMesh.vertices[a];
			Vector3 B = sphereMesh.vertices[b];
			Vector3 C = sphereMesh.vertices[c];
			if (sphereMesh.uv[a].x < 0.25f)
			{
				int tempA = a;
				if (!visited.TryGetValue(a, out tempA))
				{
					Vector2 tmpUV=sphereMesh.uv[a];
					tmpUV.x += 1;
					vertices.Add(A);
					UVs.Add (tmpUV);
					verticeIndex++;
					visited[a] = verticeIndex;
					tempA = verticeIndex;
				}
				a = tempA;
			}
			if (sphereMesh.uv[b].x< 0.25f)
			{
				int tempB = b;
				if (!visited.TryGetValue(b, out tempB))
				{
					Vector2 tmpUV=sphereMesh.uv[b];
					tmpUV.x+= 1;
					vertices.Add(B);
					UVs.Add (tmpUV);
					verticeIndex++;
					visited[b] = verticeIndex;
					tempB = verticeIndex;
				}
				b = tempB;
			}
			if (sphereMesh.uv[c].x < 0.25f)
			{
				int tempC = c;
				if (!visited.TryGetValue(c, out tempC))
				{
					Vector2 tmpUV=sphereMesh.uv[c];
					tmpUV.x+= 1;
					vertices.Add(C);
					UVs.Add (tmpUV);
					verticeIndex++;
					visited[c] = verticeIndex;
					tempC = verticeIndex;
				}
				c = tempC;
			}
			sphereMesh.triangles[i]= a;
			sphereMesh.triangles[i+1] = b;
			sphereMesh.triangles[i+2]= c;
		}
		int[] tmpTri = sphereMesh.triangles;
		sphereMesh.Clear ();
		sphereMesh.vertices = vertices.ToArray();
		sphereMesh.uv = UVs.ToArray();
		sphereMesh.triangles = tmpTri;
		sphereMesh.RecalculateNormals();
		sphereMesh.RecalculateBounds();


	}

	private void CorrectSeam(Mesh mesh)
	{
		List<int> newIndices = new List<int>();
		List<Vector3> vertices = new List<Vector3>(mesh.vertices);
		List<Vector2> UVs = new List<Vector2>(mesh.uv);

		// Whenever a vertex is split, add its original and new indices to the dictionary to avoid creating duplicates.
		Dictionary<int, int> correctionList = new Dictionary<int, int>();
		bool counterClockwise=true; //True when Normals pointing toward inside(when it is false we have to sue "cull front" in the shader  ) 
		for (int i = mesh.triangles.Length - 3; i >= 0; i -= 3)
		{
			// See if the texture coordinates appear in counter-clockwise order.
			// If so, the triangle needs to be rectified.
			Vector3 v0 = new Vector3(mesh.uv[mesh.triangles[i + 0]].x,mesh.uv[mesh.triangles[i + 0]].y, 0);
			Vector3 v1 = new Vector3(mesh.uv[mesh.triangles[i + 1]].x,mesh.uv[mesh.triangles[i + 1]].y, 0);
			Vector3 v2 = new Vector3(mesh.uv[mesh.triangles[i + 2]].x,mesh.uv[mesh.triangles[i + 2]].y, 0);

			Vector3 cross = Vector3.Cross(v0 - v1, v2 - v1);

			if (counterClockwise && cross.z <= 0 || !counterClockwise && cross.z > 0) {
				// This should only happen if the face crosses a texture boundary.
				for (int j = i; j < i + 3; j++) {
					int index = mesh.triangles [j];

					Vector3 vertex = mesh.vertices [index];
					// 0.9 UV fudge factor - should be able to get rid of this when I get more sleep.
					if (mesh.uv [index].x >= 0.9f) {
						// Need to correct this vertex.
						if (correctionList.ContainsKey (index))
							newIndices.Add (correctionList [index]);
						else {
							Vector2 texCoord = mesh.uv[index];
							texCoord.x -= 1;
							vertices.Add (vertex);
							UVs.Add (texCoord);
							int correctedVertexIndex = vertices.Count - 1;

							correctionList.Add (index, correctedVertexIndex);

							newIndices.Add (correctedVertexIndex);
						}
					} else
						newIndices.Add (index);
				}
			} else {
				newIndices.Add (mesh.triangles [i]);
				newIndices.Add (mesh.triangles [i + 1]);
				newIndices.Add (mesh.triangles [i + 2]);
			}
		}

		mesh.Clear();
		mesh.vertices = vertices.ToArray();
		mesh.uv = UVs.ToArray();
		mesh.triangles=newIndices.ToArray();

	}
		

	private void correctPole( Mesh mesh){
		List<Vector3> vertices = new List<Vector3>(mesh.vertices);
		List<Vector2> UVs = new List<Vector2>(mesh.uv);
		List<int> indices = new List<int>(mesh.triangles);

		int newVertexIndex = vertices.Count - 1;

		List<int> faces3 = new List<int>();
		for (int i = 0; i < mesh.triangles.Length; i+=3)
		{
			if (mesh.uv[mesh.triangles[i]].y > 0.99f || mesh.uv[mesh.triangles[i]].y<0.01f)
			{
				newVertexIndex++;
				vertices.Add(mesh.vertices[mesh.triangles[i]]);
				Vector3 newVertex = vertices[newVertexIndex];
				Vector2 tmpUV;
				float offset = 0.1f;

				tmpUV.x = (mesh.uv[mesh.triangles[i+1]].x+ mesh.uv[mesh.triangles[i+2]].x) * 0.5f + offset;
				tmpUV.y = mesh.uv[mesh.triangles[i]].y;   
				UVs.Add(tmpUV);
				// vertex1 is pole, duplicate it.

				indices[i]=newVertexIndex;
			}
			else if (mesh.uv[mesh.triangles[i+1]].y > 0.99f  || mesh.uv[mesh.triangles[i+1]].y<0.01f)
			{
				newVertexIndex++;
				vertices.Add(mesh.vertices[mesh.triangles[i+1]]);
				Vector3 newVertex = vertices[newVertexIndex];
				Vector2 tmpUV;
				float offset = 0.1f;

				tmpUV.x = (mesh.uv[mesh.triangles[i]].x+ mesh.uv[mesh.triangles[i+2]].x) * 0.5f + offset;
				tmpUV.y = mesh.uv[mesh.triangles[i+1]].y;
				UVs.Add(tmpUV);
				// vertex2 is pole, duplicate it.

				indices[i+1]=newVertexIndex;

			}
			else if (mesh.uv[mesh.triangles[i+2]].y > 0.99f  || mesh.uv[mesh.triangles[i+2]].y<0.01f)
			{
				newVertexIndex++;
				vertices.Add(mesh.vertices[mesh.triangles[i+2]]);
				Vector3 newVertex = vertices[newVertexIndex];
				Vector2 tmpUV;
				float offset = 0.1f;
		
				tmpUV.x = (mesh.uv[mesh.triangles[i]].x+ mesh.uv[mesh.triangles[i+1]].x) * 0.5f + offset;
				tmpUV.y = mesh.uv[mesh.triangles[i+2]].y;
				UVs.Add(tmpUV);
				// vertex3 is pole, duplicate it.
				indices[i+2]=newVertexIndex;
			}
		}

		mesh.Clear ();
		mesh.vertices = vertices.ToArray ();
		mesh.uv = UVs.ToArray ();
		mesh.triangles = indices.ToArray ();
	} 

}