﻿using UnityEngine;
using System.Collections;

public class Cam : MonoBehaviour {
	public int resWidth = 1920; 
	public int resHeight = 1080;

	private bool takeHiResShot = false;
	private bool record = false;
	//GameObject mainCamera;

	public static string ScreenShotName(int width, int height,string rotation) {
		string str="/Users/Amiz/Desktop/screenshots/";			//Application.dataPath, 
		str+=rotation;
		str+= string.Format("Screen_{0}x{1}_{2}.png", width, height, 
		System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
		return str;
	}

//	void Start(){
//		mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
//
//	}
	void LateUpdate() {
		takeHiResShot |= Input.GetKeyDown("k");
		record |= Input.GetKeyDown("r");
		if (takeHiResShot) {
			Camera camera = GetComponent<Camera>();

			RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
			camera.targetTexture = rt;
			Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
			camera.Render();
			RenderTexture.active = rt;
			screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
			camera.targetTexture = null;
			RenderTexture.active = null; // JC: added to avoid errors
			Destroy(rt);
			byte[] bytes = screenShot.EncodeToPNG();
			string filename = ScreenShotName(resWidth, resHeight,"Manual");
			System.IO.File.WriteAllBytes(filename, bytes);
			Debug.Log(string.Format("Took screenshot to: {0}", filename));
			takeHiResShot = false;
			record = false;
		}
		if (record) {
			//Camera camera = mainCamera.GetComponent<Camera>();
			Camera camera = GetComponent<Camera>();
			RenderTexture rt = new RenderTexture (resWidth, resHeight, 24);
			camera.transform.localRotation = Quaternion.identity;
			camera.transform.Rotate (new Vector3 (0, 90, 0));
			//camera.transform.LookAt(new Vector3 (0, 90, 0));
			//transform.LookAt(-1*);	
			//transform.Rotate(new Vector3 (0,90,0));
			camera.targetTexture = rt;
				Texture2D screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render ();
				RenderTexture.active = rt;
				screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy (rt);
				byte[] bytes = screenShot.EncodeToPNG ();
			string filename = ScreenShotName (resWidth, resHeight,"Forward");
				System.IO.File.WriteAllBytes (filename, bytes);
				Debug.Log (string.Format ("Took screenshot to: {0}", filename));

				rt = new RenderTexture (resWidth, resHeight, 24);
			camera.transform.Rotate (new Vector3 (-90, 0, 0));
				camera.targetTexture = rt;
				screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render ();
				RenderTexture.active = rt;
				screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy (rt);
				bytes = screenShot.EncodeToPNG ();
			filename = ScreenShotName (resWidth, resHeight,"Up");
				System.IO.File.WriteAllBytes (filename, bytes);
				Debug.Log (string.Format ("Took screenshot to: {0}", filename));

				rt = new RenderTexture (resWidth, resHeight, 24);
			camera.transform.localRotation = Quaternion.identity;
			camera.transform.Rotate (new Vector3 (0, 180, 0));
				camera.targetTexture = rt;
				screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render ();
				RenderTexture.active = rt;
				screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy (rt);
				bytes = screenShot.EncodeToPNG ();
			filename = ScreenShotName (resWidth, resHeight,"Right");

				System.IO.File.WriteAllBytes (filename, bytes);
				Debug.Log (string.Format ("Took screenshot to: {0}", filename));

				 rt = new RenderTexture (resWidth, resHeight, 24);
			camera.transform.localRotation = Quaternion.identity;
			camera.transform.Rotate (new Vector3 (0, -90, 0));
				camera.targetTexture = rt;
				screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render ();
				RenderTexture.active = rt;
				screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy (rt);
				 bytes = screenShot.EncodeToPNG ();
			filename = ScreenShotName (resWidth, resHeight,"Back");
				System.IO.File.WriteAllBytes (filename, bytes);
				Debug.Log (string.Format ("Took screenshot to: {0}", filename));

				rt = new RenderTexture (resWidth, resHeight, 24);
			camera.transform.localRotation = Quaternion.identity;
			camera.transform.Rotate (new Vector3 (90, 90, 0));
			camera.targetTexture = rt;
				screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render ();
				RenderTexture.active = rt;
				screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy (rt);
				bytes = screenShot.EncodeToPNG ();
			filename = ScreenShotName (resWidth, resHeight,"Down");
				System.IO.File.WriteAllBytes (filename, bytes);
				Debug.Log (string.Format ("Took screenshot to: {0}", filename));

				rt = new RenderTexture (resWidth, resHeight, 24);
			camera.transform.localRotation = Quaternion.identity;
			camera.targetTexture = rt;
				screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render ();
				RenderTexture.active = rt;
				screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy (rt);
				bytes = screenShot.EncodeToPNG ();
			filename = ScreenShotName (resWidth, resHeight,"Left");
				System.IO.File.WriteAllBytes (filename, bytes);
				Debug.Log (string.Format ("Took screenshot to: {0}", filename));

			takeHiResShot = false;
			record = false;
			}
	}
}