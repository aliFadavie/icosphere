﻿Shader "Custom/Panaroma" {
	Properties {
       _MainTex ("Base (RGB)", 2D) = "white" {}
   }
   SubShader {

Pass {  
   	//Cull Front  //either revert normals or use culling

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"
			#define PI 3.141592653589793238462643383279

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = half2(0, 0);
				return o;
			}

			[maxvertexcount(3)]
            void geom(triangle v2f input[3], inout TriangleStream<v2f> OutputStream)
            {
                v2f test[3];
                //float3 normal = normalize(cross(input[1].worldPosition.xyz - input[0].worldPosition.xyz, input[2].worldPosition.xyz - input[0].worldPosition.xyz));
                for(int i = 0; i < 3; i++)
                {
                    test[i].vertex = input[i].vertex;
                    test[i].texcoord.x=0.5-atan2(input[i].vertex.z,input[i].vertex.x)/(2*3.14159);
					test[i].texcoord.y=0.5+asin(input[i].vertex.y)/3.14159;
			
                }
                half3 v0=half3(test[0].texcoord.x,test[0].texcoord.y,0);
                half3 v1=half3(test[1].texcoord.x,test[1].texcoord.y,0);
                half3 v2=half3(test[2].texcoord.x,test[2].texcoord.y,0);
                half3 Vcross= cross(v0 - v1, v2 - v1);
                if(Vcross.z>0 || Vcross.z<=0){
                	for(int i=0;i<3;i++){
						if(test[i].texcoord.x>0.9f)
						test[i].texcoord.x-=1;
						}
                }
                //Fix poles
				if (test[0].texcoord.y > 0.99f || test[0].texcoord.y<0.01f)
					test[0].texcoord.x=(test[1].texcoord.x+test[2].texcoord.x)*0.5f;
				else if (test[1].texcoord.y > 0.99f || test[1].texcoord.y<0.01f)
					test[1].texcoord.x=(test[0].texcoord.x+test[2].texcoord.x)*0.5f;
				else if (test[2].texcoord.y > 0.99f || test[2].texcoord.y<0.01f)
						test[2].texcoord.x=(test[0].texcoord.x+test[1].texcoord.x)*0.5f;
					


                for(int i=0;i<3;i++)
					OutputStream.Append(test[i]);
			
                




            }
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.texcoord);
				//if(i.texcoord.y>=0.97|| i.texcoord.y<=0.03 )// using this for debuging 
				//	col=fixed4(1,0,0,1);
				return col;
			}
		ENDCG
	}
}
}

