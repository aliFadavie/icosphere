Problem:
After calculating the texture's UV coordinates correspond to the geometry's latitude and longitude, there is gonna be two issues that need to be fixed.

-There's a texture seam from one pole to another one since wrapping 2D rectangular image around sphere cause one strip of triangles sharing vertexes that have UVs with the drastic differences.(jump from something around 0.97 to 0.03) 

-There's a swirly distortion at each pole due to the vertex sharing on the poles using only one vertex on each pole for the entire edge of the texture causing this. 

Solution:
The best solution -if it is possible- is using a cubic image instead of an Equirectangular image(creating a cubic image from Equirectangular image). With cubic image we are not gonna have any artifacts. However, for Equirectangular case, we can detect the triangles that cause the seam and fix the problem by changing the seam to a  sharp almost invisible line

 To prevent the second problem, we have to use six vertices per pole so we can rotate them along with their triangles. Again a number of triangles don't change, only for each faces on the pole we calculate new UV. This UV can be calculated by adding an offset value to the original value of the UV or by averaging the UVs of the other two vertices of the face.     
<To be noted that only in horizontal coordinate we get stretch so we don't recalculate the vertical coordinate>
Optimization:
Using the Vertex shader to calculation the UVs is possible, however, since we don't have access to other vertices of triangles in order to get the desired output we have to use a Geometry shader as well. This decrease the speed so it is not the best solution.  
We know that vertices don't change during the application so UV calculation is gonna be a redundant recalculation. We can speed up the process by precalculating UVs on CPU then passing them to GPU. This way we don't need Geometry shader and for Vertex/Fragment shader we can use a simple built-in unity shader.