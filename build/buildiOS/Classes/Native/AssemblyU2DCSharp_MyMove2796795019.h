﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyMove
struct  MyMove_t2796795019  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MyMove::horizontalSpeed
	float ___horizontalSpeed_2;
	// System.Single MyMove::verticalSpeed
	float ___verticalSpeed_3;

public:
	inline static int32_t get_offset_of_horizontalSpeed_2() { return static_cast<int32_t>(offsetof(MyMove_t2796795019, ___horizontalSpeed_2)); }
	inline float get_horizontalSpeed_2() const { return ___horizontalSpeed_2; }
	inline float* get_address_of_horizontalSpeed_2() { return &___horizontalSpeed_2; }
	inline void set_horizontalSpeed_2(float value)
	{
		___horizontalSpeed_2 = value;
	}

	inline static int32_t get_offset_of_verticalSpeed_3() { return static_cast<int32_t>(offsetof(MyMove_t2796795019, ___verticalSpeed_3)); }
	inline float get_verticalSpeed_3() const { return ___verticalSpeed_3; }
	inline float* get_address_of_verticalSpeed_3() { return &___verticalSpeed_3; }
	inline void set_verticalSpeed_3(float value)
	{
		___verticalSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
