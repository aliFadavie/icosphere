﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCam
struct  TouchCam_t4105149786  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 TouchCam::firstpoint
	Vector3_t2243707580  ___firstpoint_2;
	// UnityEngine.Vector3 TouchCam::secondpoint
	Vector3_t2243707580  ___secondpoint_3;
	// System.Single TouchCam::xAngle
	float ___xAngle_4;
	// System.Single TouchCam::yAngle
	float ___yAngle_5;
	// System.Single TouchCam::xAngTemp
	float ___xAngTemp_6;
	// System.Single TouchCam::yAngTemp
	float ___yAngTemp_7;
	// UnityEngine.GameObject TouchCam::playRot
	GameObject_t1756533147 * ___playRot_8;

public:
	inline static int32_t get_offset_of_firstpoint_2() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___firstpoint_2)); }
	inline Vector3_t2243707580  get_firstpoint_2() const { return ___firstpoint_2; }
	inline Vector3_t2243707580 * get_address_of_firstpoint_2() { return &___firstpoint_2; }
	inline void set_firstpoint_2(Vector3_t2243707580  value)
	{
		___firstpoint_2 = value;
	}

	inline static int32_t get_offset_of_secondpoint_3() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___secondpoint_3)); }
	inline Vector3_t2243707580  get_secondpoint_3() const { return ___secondpoint_3; }
	inline Vector3_t2243707580 * get_address_of_secondpoint_3() { return &___secondpoint_3; }
	inline void set_secondpoint_3(Vector3_t2243707580  value)
	{
		___secondpoint_3 = value;
	}

	inline static int32_t get_offset_of_xAngle_4() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___xAngle_4)); }
	inline float get_xAngle_4() const { return ___xAngle_4; }
	inline float* get_address_of_xAngle_4() { return &___xAngle_4; }
	inline void set_xAngle_4(float value)
	{
		___xAngle_4 = value;
	}

	inline static int32_t get_offset_of_yAngle_5() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___yAngle_5)); }
	inline float get_yAngle_5() const { return ___yAngle_5; }
	inline float* get_address_of_yAngle_5() { return &___yAngle_5; }
	inline void set_yAngle_5(float value)
	{
		___yAngle_5 = value;
	}

	inline static int32_t get_offset_of_xAngTemp_6() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___xAngTemp_6)); }
	inline float get_xAngTemp_6() const { return ___xAngTemp_6; }
	inline float* get_address_of_xAngTemp_6() { return &___xAngTemp_6; }
	inline void set_xAngTemp_6(float value)
	{
		___xAngTemp_6 = value;
	}

	inline static int32_t get_offset_of_yAngTemp_7() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___yAngTemp_7)); }
	inline float get_yAngTemp_7() const { return ___yAngTemp_7; }
	inline float* get_address_of_yAngTemp_7() { return &___yAngTemp_7; }
	inline void set_yAngTemp_7(float value)
	{
		___yAngTemp_7 = value;
	}

	inline static int32_t get_offset_of_playRot_8() { return static_cast<int32_t>(offsetof(TouchCam_t4105149786, ___playRot_8)); }
	inline GameObject_t1756533147 * get_playRot_8() const { return ___playRot_8; }
	inline GameObject_t1756533147 ** get_address_of_playRot_8() { return &___playRot_8; }
	inline void set_playRot_8(GameObject_t1756533147 * value)
	{
		___playRot_8 = value;
		Il2CppCodeGenWriteBarrier(&___playRot_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
