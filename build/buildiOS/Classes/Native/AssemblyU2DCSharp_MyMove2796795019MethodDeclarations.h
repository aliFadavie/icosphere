﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyMove
struct MyMove_t2796795019;

#include "codegen/il2cpp-codegen.h"

// System.Void MyMove::.ctor()
extern "C"  void MyMove__ctor_m807702674 (MyMove_t2796795019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyMove::Update()
extern "C"  void MyMove_Update_m3933786771 (MyMove_t2796795019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
