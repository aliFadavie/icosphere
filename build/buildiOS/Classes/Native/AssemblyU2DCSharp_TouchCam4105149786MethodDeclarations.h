﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchCam
struct TouchCam_t4105149786;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchCam::.ctor()
extern "C"  void TouchCam__ctor_m1345353997 (TouchCam_t4105149786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchCam::Start()
extern "C"  void TouchCam_Start_m1317043845 (TouchCam_t4105149786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchCam::Update()
extern "C"  void TouchCam_Update_m1459278428 (TouchCam_t4105149786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
