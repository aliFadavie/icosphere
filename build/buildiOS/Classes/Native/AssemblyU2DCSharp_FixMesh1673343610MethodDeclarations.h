﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FixMesh
struct FixMesh_t1673343610;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"

// System.Void FixMesh::.ctor()
extern "C"  void FixMesh__ctor_m2345129751 (FixMesh_t1673343610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixMesh::Start()
extern "C"  void FixMesh_Start_m481463467 (FixMesh_t1673343610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixMesh::TextCoord(UnityEngine.Mesh)
extern "C"  void FixMesh_TextCoord_m2805995387 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ____mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FixMesh::DetectWrappedUVCoordinates(UnityEngine.Mesh)
extern "C"  Int32U5BU5D_t3030399641* FixMesh_DetectWrappedUVCoordinates_m1892403419 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixMesh::FixWrappedUV(System.Int32[],UnityEngine.Mesh)
extern "C"  void FixMesh_FixWrappedUV_m498452425 (FixMesh_t1673343610 * __this, Int32U5BU5D_t3030399641* ___wrapped0, Mesh_t1356156583 * ___sphereMesh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixMesh::CorrectSeam(UnityEngine.Mesh)
extern "C"  void FixMesh_CorrectSeam_m3944530567 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixMesh::correctPole(UnityEngine.Mesh)
extern "C"  void FixMesh_correctPole_m3924534843 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
