﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixMesh
struct  FixMesh_t1673343610  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2[] FixMesh::newUV
	Vector2U5BU5D_t686124026* ___newUV_2;

public:
	inline static int32_t get_offset_of_newUV_2() { return static_cast<int32_t>(offsetof(FixMesh_t1673343610, ___newUV_2)); }
	inline Vector2U5BU5D_t686124026* get_newUV_2() const { return ___newUV_2; }
	inline Vector2U5BU5D_t686124026** get_address_of_newUV_2() { return &___newUV_2; }
	inline void set_newUV_2(Vector2U5BU5D_t686124026* value)
	{
		___newUV_2 = value;
		Il2CppCodeGenWriteBarrier(&___newUV_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
