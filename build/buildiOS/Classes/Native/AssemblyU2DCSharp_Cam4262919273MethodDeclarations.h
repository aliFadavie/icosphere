﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cam
struct Cam_t4262919273;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Cam::.ctor()
extern "C"  void Cam__ctor_m2174110286 (Cam_t4262919273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Cam::ScreenShotName(System.Int32,System.Int32,System.Int32)
extern "C"  String_t* Cam_ScreenShotName_m1636204419 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cam::LateUpdate()
extern "C"  void Cam_LateUpdate_m455040653 (Cam_t4262919273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
