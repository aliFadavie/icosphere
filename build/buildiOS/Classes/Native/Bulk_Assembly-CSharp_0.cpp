﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Cam
struct Cam_t4262919273;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Object
struct Il2CppObject;
// FixMesh
struct FixMesh_t1673343610;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// MyMove
struct MyMove_t2796795019;
// TouchCam
struct TouchCam_t4105149786;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cam4262919273.h"
#include "AssemblyU2DCSharp_Cam4262919273MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_FixMesh1673343610.h"
#include "AssemblyU2DCSharp_FixMesh1673343610MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083.h"
#include "AssemblyU2DCSharp_MyMove2796795019.h"
#include "AssemblyU2DCSharp_MyMove2796795019MethodDeclarations.h"
#include "AssemblyU2DCSharp_TouchCam4105149786.h"
#include "AssemblyU2DCSharp_TouchCam4105149786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m633060157(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3026937449_m1170669043(__this, method) ((  MeshFilter_t3026937449 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cam::.ctor()
extern "C"  void Cam__ctor_m2174110286 (Cam_t4262919273 * __this, const MethodInfo* method)
{
	{
		__this->set_resWidth_2(((int32_t)1920));
		__this->set_resHeight_3(((int32_t)1080));
		__this->set_takeHiResShot_4((bool)0);
		__this->set_record_5((bool)0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Cam::ScreenShotName(System.Int32,System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1652237509;
extern Il2CppCodeGenString* _stringLiteral779232039;
extern const uint32_t Cam_ScreenShotName_m1636204419_MetadataUsageId;
extern "C"  String_t* Cam_ScreenShotName_m1636204419 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cam_ScreenShotName_m1636204419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_1 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = L_0;
		int32_t L_3 = ___width0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_2;
		int32_t L_7 = ___height1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		int32_t L_11 = ___rotation2;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		ObjectU5BU5D_t3614634134* L_14 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_15 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_15;
		String_t* L_16 = DateTime_ToString_m1473013667((&V_0), _stringLiteral779232039, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1652237509, L_14, /*hidden argument*/NULL);
		V_1 = L_17;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_18 = V_1;
		return L_18;
	}
}
// System.Void Cam::LateUpdate()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderTexture_t2666733923_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m633060157_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029383;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral2932563776;
extern const uint32_t Cam_LateUpdate_m455040653_MetadataUsageId;
extern "C"  void Cam_LateUpdate_m455040653 (Cam_t4262919273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cam_LateUpdate_m455040653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t189460977 * V_0 = NULL;
	RenderTexture_t2666733923 * V_1 = NULL;
	Texture2D_t3542995729 * V_2 = NULL;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	String_t* V_4 = NULL;
	Camera_t189460977 * V_5 = NULL;
	RenderTexture_t2666733923 * V_6 = NULL;
	Texture2D_t3542995729 * V_7 = NULL;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	String_t* V_9 = NULL;
	{
		bool L_0 = __this->get_takeHiResShot_4();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral372029383, /*hidden argument*/NULL);
		__this->set_takeHiResShot_4((bool)((int32_t)((int32_t)L_0|(int32_t)L_1)));
		bool L_2 = __this->get_record_5();
		bool L_3 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral372029392, /*hidden argument*/NULL);
		__this->set_record_5((bool)((int32_t)((int32_t)L_2|(int32_t)L_3)));
		bool L_4 = __this->get_takeHiResShot_4();
		if (!L_4)
		{
			goto IL_00f9;
		}
	}
	{
		Camera_t189460977 * L_5 = Component_GetComponent_TisCamera_t189460977_m633060157(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m633060157_MethodInfo_var);
		V_0 = L_5;
		int32_t L_6 = __this->get_resWidth_2();
		int32_t L_7 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_8 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_8, L_6, L_7, ((int32_t)24), /*hidden argument*/NULL);
		V_1 = L_8;
		Camera_t189460977 * L_9 = V_0;
		RenderTexture_t2666733923 * L_10 = V_1;
		NullCheck(L_9);
		Camera_set_targetTexture_m3925036117(L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_resWidth_2();
		int32_t L_12 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_13 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_13, L_11, L_12, 3, (bool)0, /*hidden argument*/NULL);
		V_2 = L_13;
		Camera_t189460977 * L_14 = V_0;
		NullCheck(L_14);
		Camera_Render_m2021402646(L_14, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_15 = V_1;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_16 = V_2;
		int32_t L_17 = __this->get_resWidth_2();
		int32_t L_18 = __this->get_resHeight_3();
		Rect_t3681755626  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Rect__ctor_m1220545469(&L_19, (0.0f), (0.0f), (((float)((float)L_17))), (((float)((float)L_18))), /*hidden argument*/NULL);
		NullCheck(L_16);
		Texture2D_ReadPixels_m1120832672(L_16, L_19, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_20 = V_0;
		NullCheck(L_20);
		Camera_set_targetTexture_m3925036117(L_20, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = V_2;
		NullCheck(L_22);
		ByteU5BU5D_t3397334013* L_23 = Texture2D_EncodeToPNG_m2680110528(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = __this->get_resWidth_2();
		int32_t L_25 = __this->get_resHeight_3();
		String_t* L_26 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_24, L_25, ((int32_t)10), /*hidden argument*/NULL);
		V_4 = L_26;
		String_t* L_27 = V_4;
		ByteU5BU5D_t3397334013* L_28 = V_3;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		String_t* L_29 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		__this->set_takeHiResShot_4((bool)0);
		__this->set_record_5((bool)0);
	}

IL_00f9:
	{
		bool L_31 = __this->get_record_5();
		if (!L_31)
		{
			goto IL_05ae;
		}
	}
	{
		Camera_t189460977 * L_32 = Component_GetComponent_TisCamera_t189460977_m633060157(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m633060157_MethodInfo_var);
		V_5 = L_32;
		int32_t L_33 = __this->get_resWidth_2();
		int32_t L_34 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_35 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_35, L_33, L_34, ((int32_t)24), /*hidden argument*/NULL);
		V_6 = L_35;
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_37 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_LookAt_m3314153180(L_36, L_37, /*hidden argument*/NULL);
		Camera_t189460977 * L_38 = V_5;
		RenderTexture_t2666733923 * L_39 = V_6;
		NullCheck(L_38);
		Camera_set_targetTexture_m3925036117(L_38, L_39, /*hidden argument*/NULL);
		int32_t L_40 = __this->get_resWidth_2();
		int32_t L_41 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_42 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_42, L_40, L_41, 3, (bool)0, /*hidden argument*/NULL);
		V_7 = L_42;
		Camera_t189460977 * L_43 = V_5;
		NullCheck(L_43);
		Camera_Render_m2021402646(L_43, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_44 = V_6;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_45 = V_7;
		int32_t L_46 = __this->get_resWidth_2();
		int32_t L_47 = __this->get_resHeight_3();
		Rect_t3681755626  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Rect__ctor_m1220545469(&L_48, (0.0f), (0.0f), (((float)((float)L_46))), (((float)((float)L_47))), /*hidden argument*/NULL);
		NullCheck(L_45);
		Texture2D_ReadPixels_m1120832672(L_45, L_48, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_49 = V_5;
		NullCheck(L_49);
		Camera_set_targetTexture_m3925036117(L_49, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_50 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_51 = V_7;
		NullCheck(L_51);
		ByteU5BU5D_t3397334013* L_52 = Texture2D_EncodeToPNG_m2680110528(L_51, /*hidden argument*/NULL);
		V_8 = L_52;
		int32_t L_53 = __this->get_resWidth_2();
		int32_t L_54 = __this->get_resHeight_3();
		String_t* L_55 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_53, L_54, 0, /*hidden argument*/NULL);
		V_9 = L_55;
		String_t* L_56 = V_9;
		ByteU5BU5D_t3397334013* L_57 = V_8;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_56, L_57, /*hidden argument*/NULL);
		String_t* L_58 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_58, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		int32_t L_60 = __this->get_resWidth_2();
		int32_t L_61 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_62 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_62, L_60, L_61, ((int32_t)24), /*hidden argument*/NULL);
		V_6 = L_62;
		Transform_t3275118058 * L_63 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_64 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_LookAt_m3314153180(L_63, L_64, /*hidden argument*/NULL);
		Camera_t189460977 * L_65 = V_5;
		RenderTexture_t2666733923 * L_66 = V_6;
		NullCheck(L_65);
		Camera_set_targetTexture_m3925036117(L_65, L_66, /*hidden argument*/NULL);
		int32_t L_67 = __this->get_resWidth_2();
		int32_t L_68 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_69 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_69, L_67, L_68, 3, (bool)0, /*hidden argument*/NULL);
		V_7 = L_69;
		Camera_t189460977 * L_70 = V_5;
		NullCheck(L_70);
		Camera_Render_m2021402646(L_70, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_71 = V_6;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_72 = V_7;
		int32_t L_73 = __this->get_resWidth_2();
		int32_t L_74 = __this->get_resHeight_3();
		Rect_t3681755626  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Rect__ctor_m1220545469(&L_75, (0.0f), (0.0f), (((float)((float)L_73))), (((float)((float)L_74))), /*hidden argument*/NULL);
		NullCheck(L_72);
		Texture2D_ReadPixels_m1120832672(L_72, L_75, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_76 = V_5;
		NullCheck(L_76);
		Camera_set_targetTexture_m3925036117(L_76, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_77 = V_6;
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_78 = V_7;
		NullCheck(L_78);
		ByteU5BU5D_t3397334013* L_79 = Texture2D_EncodeToPNG_m2680110528(L_78, /*hidden argument*/NULL);
		V_8 = L_79;
		int32_t L_80 = __this->get_resWidth_2();
		int32_t L_81 = __this->get_resHeight_3();
		String_t* L_82 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_80, L_81, 1, /*hidden argument*/NULL);
		V_9 = L_82;
		String_t* L_83 = V_9;
		ByteU5BU5D_t3397334013* L_84 = V_8;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_83, L_84, /*hidden argument*/NULL);
		String_t* L_85 = V_9;
		String_t* L_86 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_85, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		int32_t L_87 = __this->get_resWidth_2();
		int32_t L_88 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_89 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_89, L_87, L_88, ((int32_t)24), /*hidden argument*/NULL);
		V_6 = L_89;
		Transform_t3275118058 * L_90 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_91 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_LookAt_m3314153180(L_90, L_91, /*hidden argument*/NULL);
		Camera_t189460977 * L_92 = V_5;
		RenderTexture_t2666733923 * L_93 = V_6;
		NullCheck(L_92);
		Camera_set_targetTexture_m3925036117(L_92, L_93, /*hidden argument*/NULL);
		int32_t L_94 = __this->get_resWidth_2();
		int32_t L_95 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_96 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_96, L_94, L_95, 3, (bool)0, /*hidden argument*/NULL);
		V_7 = L_96;
		Camera_t189460977 * L_97 = V_5;
		NullCheck(L_97);
		Camera_Render_m2021402646(L_97, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_98 = V_6;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_98, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_99 = V_7;
		int32_t L_100 = __this->get_resWidth_2();
		int32_t L_101 = __this->get_resHeight_3();
		Rect_t3681755626  L_102;
		memset(&L_102, 0, sizeof(L_102));
		Rect__ctor_m1220545469(&L_102, (0.0f), (0.0f), (((float)((float)L_100))), (((float)((float)L_101))), /*hidden argument*/NULL);
		NullCheck(L_99);
		Texture2D_ReadPixels_m1120832672(L_99, L_102, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_103 = V_5;
		NullCheck(L_103);
		Camera_set_targetTexture_m3925036117(L_103, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_104 = V_6;
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_105 = V_7;
		NullCheck(L_105);
		ByteU5BU5D_t3397334013* L_106 = Texture2D_EncodeToPNG_m2680110528(L_105, /*hidden argument*/NULL);
		V_8 = L_106;
		int32_t L_107 = __this->get_resWidth_2();
		int32_t L_108 = __this->get_resHeight_3();
		String_t* L_109 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_107, L_108, 2, /*hidden argument*/NULL);
		V_9 = L_109;
		String_t* L_110 = V_9;
		ByteU5BU5D_t3397334013* L_111 = V_8;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
		String_t* L_112 = V_9;
		String_t* L_113 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_112, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_113, /*hidden argument*/NULL);
		int32_t L_114 = __this->get_resWidth_2();
		int32_t L_115 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_116 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_116, L_114, L_115, ((int32_t)24), /*hidden argument*/NULL);
		V_6 = L_116;
		Transform_t3275118058 * L_117 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_118 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		Transform_LookAt_m3314153180(L_117, L_118, /*hidden argument*/NULL);
		Camera_t189460977 * L_119 = V_5;
		RenderTexture_t2666733923 * L_120 = V_6;
		NullCheck(L_119);
		Camera_set_targetTexture_m3925036117(L_119, L_120, /*hidden argument*/NULL);
		int32_t L_121 = __this->get_resWidth_2();
		int32_t L_122 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_123 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_123, L_121, L_122, 3, (bool)0, /*hidden argument*/NULL);
		V_7 = L_123;
		Camera_t189460977 * L_124 = V_5;
		NullCheck(L_124);
		Camera_Render_m2021402646(L_124, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_125 = V_6;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_126 = V_7;
		int32_t L_127 = __this->get_resWidth_2();
		int32_t L_128 = __this->get_resHeight_3();
		Rect_t3681755626  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Rect__ctor_m1220545469(&L_129, (0.0f), (0.0f), (((float)((float)L_127))), (((float)((float)L_128))), /*hidden argument*/NULL);
		NullCheck(L_126);
		Texture2D_ReadPixels_m1120832672(L_126, L_129, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_130 = V_5;
		NullCheck(L_130);
		Camera_set_targetTexture_m3925036117(L_130, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_131 = V_6;
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_132 = V_7;
		NullCheck(L_132);
		ByteU5BU5D_t3397334013* L_133 = Texture2D_EncodeToPNG_m2680110528(L_132, /*hidden argument*/NULL);
		V_8 = L_133;
		int32_t L_134 = __this->get_resWidth_2();
		int32_t L_135 = __this->get_resHeight_3();
		String_t* L_136 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_134, L_135, 3, /*hidden argument*/NULL);
		V_9 = L_136;
		String_t* L_137 = V_9;
		ByteU5BU5D_t3397334013* L_138 = V_8;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_137, L_138, /*hidden argument*/NULL);
		String_t* L_139 = V_9;
		String_t* L_140 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_139, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		int32_t L_141 = __this->get_resWidth_2();
		int32_t L_142 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_143 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_143, L_141, L_142, ((int32_t)24), /*hidden argument*/NULL);
		V_6 = L_143;
		Transform_t3275118058 * L_144 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_145 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_LookAt_m3314153180(L_144, L_145, /*hidden argument*/NULL);
		Camera_t189460977 * L_146 = V_5;
		RenderTexture_t2666733923 * L_147 = V_6;
		NullCheck(L_146);
		Camera_set_targetTexture_m3925036117(L_146, L_147, /*hidden argument*/NULL);
		int32_t L_148 = __this->get_resWidth_2();
		int32_t L_149 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_150 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_150, L_148, L_149, 3, (bool)0, /*hidden argument*/NULL);
		V_7 = L_150;
		Camera_t189460977 * L_151 = V_5;
		NullCheck(L_151);
		Camera_Render_m2021402646(L_151, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_152 = V_6;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_152, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_153 = V_7;
		int32_t L_154 = __this->get_resWidth_2();
		int32_t L_155 = __this->get_resHeight_3();
		Rect_t3681755626  L_156;
		memset(&L_156, 0, sizeof(L_156));
		Rect__ctor_m1220545469(&L_156, (0.0f), (0.0f), (((float)((float)L_154))), (((float)((float)L_155))), /*hidden argument*/NULL);
		NullCheck(L_153);
		Texture2D_ReadPixels_m1120832672(L_153, L_156, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_157 = V_5;
		NullCheck(L_157);
		Camera_set_targetTexture_m3925036117(L_157, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_158 = V_6;
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_159 = V_7;
		NullCheck(L_159);
		ByteU5BU5D_t3397334013* L_160 = Texture2D_EncodeToPNG_m2680110528(L_159, /*hidden argument*/NULL);
		V_8 = L_160;
		int32_t L_161 = __this->get_resWidth_2();
		int32_t L_162 = __this->get_resHeight_3();
		String_t* L_163 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_161, L_162, 4, /*hidden argument*/NULL);
		V_9 = L_163;
		String_t* L_164 = V_9;
		ByteU5BU5D_t3397334013* L_165 = V_8;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_164, L_165, /*hidden argument*/NULL);
		String_t* L_166 = V_9;
		String_t* L_167 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_166, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_167, /*hidden argument*/NULL);
		int32_t L_168 = __this->get_resWidth_2();
		int32_t L_169 = __this->get_resHeight_3();
		RenderTexture_t2666733923 * L_170 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4075605457(L_170, L_168, L_169, ((int32_t)24), /*hidden argument*/NULL);
		V_6 = L_170;
		Transform_t3275118058 * L_171 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_172 = Vector3_get_left_m2429378123(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_171);
		Transform_LookAt_m3314153180(L_171, L_172, /*hidden argument*/NULL);
		Camera_t189460977 * L_173 = V_5;
		RenderTexture_t2666733923 * L_174 = V_6;
		NullCheck(L_173);
		Camera_set_targetTexture_m3925036117(L_173, L_174, /*hidden argument*/NULL);
		int32_t L_175 = __this->get_resWidth_2();
		int32_t L_176 = __this->get_resHeight_3();
		Texture2D_t3542995729 * L_177 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_177, L_175, L_176, 3, (bool)0, /*hidden argument*/NULL);
		V_7 = L_177;
		Camera_t189460977 * L_178 = V_5;
		NullCheck(L_178);
		Camera_Render_m2021402646(L_178, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_179 = V_6;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_179, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_180 = V_7;
		int32_t L_181 = __this->get_resWidth_2();
		int32_t L_182 = __this->get_resHeight_3();
		Rect_t3681755626  L_183;
		memset(&L_183, 0, sizeof(L_183));
		Rect__ctor_m1220545469(&L_183, (0.0f), (0.0f), (((float)((float)L_181))), (((float)((float)L_182))), /*hidden argument*/NULL);
		NullCheck(L_180);
		Texture2D_ReadPixels_m1120832672(L_180, L_183, 0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_184 = V_5;
		NullCheck(L_184);
		Camera_set_targetTexture_m3925036117(L_184, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, (RenderTexture_t2666733923 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_185 = V_6;
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_185, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_186 = V_7;
		NullCheck(L_186);
		ByteU5BU5D_t3397334013* L_187 = Texture2D_EncodeToPNG_m2680110528(L_186, /*hidden argument*/NULL);
		V_8 = L_187;
		int32_t L_188 = __this->get_resWidth_2();
		int32_t L_189 = __this->get_resHeight_3();
		String_t* L_190 = Cam_ScreenShotName_m1636204419(NULL /*static, unused*/, L_188, L_189, 5, /*hidden argument*/NULL);
		V_9 = L_190;
		String_t* L_191 = V_9;
		ByteU5BU5D_t3397334013* L_192 = V_8;
		File_WriteAllBytes_m677793349(NULL /*static, unused*/, L_191, L_192, /*hidden argument*/NULL);
		String_t* L_193 = V_9;
		String_t* L_194 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2932563776, L_193, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_194, /*hidden argument*/NULL);
		__this->set_takeHiResShot_4((bool)0);
		__this->set_record_5((bool)0);
	}

IL_05ae:
	{
		return;
	}
}
// System.Void FixMesh::.ctor()
extern "C"  void FixMesh__ctor_m2345129751 (FixMesh_t1673343610 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FixMesh::Start()
extern Il2CppClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1170669043_MethodInfo_var;
extern const uint32_t FixMesh_Start_m481463467_MetadataUsageId;
extern "C"  void FixMesh_Start_m481463467 (FixMesh_t1673343610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixMesh_Start_m481463467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t1356156583 * V_0 = NULL;
	{
		MeshFilter_t3026937449 * L_0 = Component_GetComponent_TisMeshFilter_t3026937449_m1170669043(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1170669043_MethodInfo_var);
		NullCheck(L_0);
		Mesh_t1356156583 * L_1 = MeshFilter_get_mesh_m977520135(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Mesh_t1356156583 * L_2 = V_0;
		NullCheck(L_2);
		Vector3U5BU5D_t1172311765* L_3 = Mesh_get_vertices_m626989480(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		__this->set_newUV_2(((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		Mesh_t1356156583 * L_4 = V_0;
		FixMesh_TextCoord_m2805995387(__this, L_4, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_5 = V_0;
		Vector2U5BU5D_t686124026* L_6 = __this->get_newUV_2();
		NullCheck(L_5);
		Mesh_set_uv_m1497318906(L_5, L_6, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_7 = V_0;
		FixMesh_CorrectSeam_m3944530567(__this, L_7, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_8 = V_0;
		FixMesh_correctPole_m3924534843(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FixMesh::TextCoord(UnityEngine.Mesh)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t FixMesh_TextCoord_m2805995387_MetadataUsageId;
extern "C"  void FixMesh_TextCoord_m2805995387 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ____mesh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixMesh_TextCoord_m2805995387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = 0;
		goto IL_0079;
	}

IL_0008:
	{
		Mesh_t1356156583 * L_0 = ____mesh0;
		NullCheck(L_0);
		Vector3U5BU5D_t1172311765* L_1 = Mesh_get_vertices_m626989480(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		V_1 = (*(Vector3_t2243707580 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		Vector2U5BU5D_t686124026* L_3 = __this->get_newUV_2();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		float L_5 = (&V_1)->get_z_3();
		float L_6 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = atan2f(L_5, L_6);
		((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))->set_x_0(((float)((float)(0.5f)+(float)((float)((float)L_7/(float)(6.28318548f))))));
		Vector2U5BU5D_t686124026* L_8 = __this->get_newUV_2();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		float L_10 = (&V_1)->get_y_2();
		float L_11 = asinf(L_10);
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_y_1(((float)((float)(0.5f)-(float)((float)((float)L_11/(float)(3.14159274f))))));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_13 = V_0;
		Mesh_t1356156583 * L_14 = ____mesh0;
		NullCheck(L_14);
		Vector3U5BU5D_t1172311765* L_15 = Mesh_get_vertices_m626989480(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Int32[] FixMesh::DetectWrappedUVCoordinates(UnityEngine.Mesh)
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1465587600_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3453833174_MethodInfo_var;
extern const uint32_t FixMesh_DetectWrappedUVCoordinates_m1892403419_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* FixMesh_DetectWrappedUVCoordinates_m1892403419 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixMesh_DetectWrappedUVCoordinates_m1892403419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1440998580 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Int32U5BU5D_t3030399641* V_9 = NULL;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_0, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_00f3;
	}

IL_000e:
	{
		Mesh_t1356156583 * L_1 = ___mesh0;
		NullCheck(L_1);
		Int32U5BU5D_t3030399641* L_2 = Mesh_get_triangles_m3988715512(L_1, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		Mesh_t1356156583 * L_6 = ___mesh0;
		NullCheck(L_6);
		Int32U5BU5D_t3030399641* L_7 = Mesh_get_triangles_m3988715512(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_3 = L_10;
		Mesh_t1356156583 * L_11 = ___mesh0;
		NullCheck(L_11);
		Int32U5BU5D_t3030399641* L_12 = Mesh_get_triangles_m3988715512(L_11, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)((int32_t)L_13+(int32_t)2));
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		Mesh_t1356156583 * L_16 = ___mesh0;
		NullCheck(L_16);
		Vector2U5BU5D_t686124026* L_17 = Mesh_get_uv_m3811151337(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		NullCheck(L_17);
		float L_19 = ((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))->get_x_0();
		Mesh_t1356156583 * L_20 = ___mesh0;
		NullCheck(L_20);
		Vector2U5BU5D_t686124026* L_21 = Mesh_get_uv_m3811151337(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		float L_23 = ((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_y_1();
		Vector3__ctor_m2638739322((&V_5), L_19, L_23, (0.0f), /*hidden argument*/NULL);
		Mesh_t1356156583 * L_24 = ___mesh0;
		NullCheck(L_24);
		Vector2U5BU5D_t686124026* L_25 = Mesh_get_uv_m3811151337(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_3;
		NullCheck(L_25);
		float L_27 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_x_0();
		Mesh_t1356156583 * L_28 = ___mesh0;
		NullCheck(L_28);
		Vector2U5BU5D_t686124026* L_29 = Mesh_get_uv_m3811151337(L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_3;
		NullCheck(L_29);
		float L_31 = ((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_y_1();
		Vector3__ctor_m2638739322((&V_6), L_27, L_31, (0.0f), /*hidden argument*/NULL);
		Mesh_t1356156583 * L_32 = ___mesh0;
		NullCheck(L_32);
		Vector2U5BU5D_t686124026* L_33 = Mesh_get_uv_m3811151337(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_4;
		NullCheck(L_33);
		float L_35 = ((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_x_0();
		Mesh_t1356156583 * L_36 = ___mesh0;
		NullCheck(L_36);
		Vector2U5BU5D_t686124026* L_37 = Mesh_get_uv_m3811151337(L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_4;
		NullCheck(L_37);
		float L_39 = ((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38)))->get_y_1();
		Vector3__ctor_m2638739322((&V_7), L_35, L_39, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = V_6;
		Vector3_t2243707580  L_41 = V_5;
		Vector3_t2243707580  L_42 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Vector3_t2243707580  L_45 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Vector3_t2243707580  L_46 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_42, L_45, /*hidden argument*/NULL);
		V_8 = L_46;
		float L_47 = (&V_8)->get_z_3();
		if ((!(((float)L_47) < ((float)(0.0f)))))
		{
			goto IL_00ee;
		}
	}
	{
		List_1_t1440998580 * L_48 = V_0;
		int32_t L_49 = V_1;
		NullCheck(L_48);
		List_1_Add_m1465587600(L_48, L_49, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
	}

IL_00ee:
	{
		int32_t L_50 = V_1;
		V_1 = ((int32_t)((int32_t)L_50+(int32_t)3));
	}

IL_00f3:
	{
		int32_t L_51 = V_1;
		Mesh_t1356156583 * L_52 = ___mesh0;
		NullCheck(L_52);
		Int32U5BU5D_t3030399641* L_53 = Mesh_get_triangles_m3988715512(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		if ((((int32_t)L_51) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_53)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t1440998580 * L_54 = V_0;
		NullCheck(L_54);
		Int32U5BU5D_t3030399641* L_55 = List_1_ToArray_m3453833174(L_54, /*hidden argument*/List_1_ToArray_m3453833174_MethodInfo_var);
		V_9 = L_55;
		goto IL_010e;
	}

IL_010e:
	{
		Int32U5BU5D_t3030399641* L_56 = V_9;
		return L_56;
	}
}
// System.Void FixMesh::FixWrappedUV(System.Int32[],UnityEngine.Mesh)
extern Il2CppClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612828711_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1079703083_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1244050974_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m798345663_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m40859504_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2157787832_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2332787485_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1552520334_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1515754733_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1233026737_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2543904144_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4024240619_MethodInfo_var;
extern const uint32_t FixMesh_FixWrappedUV_m498452425_MetadataUsageId;
extern "C"  void FixMesh_FixWrappedUV_m498452425 (FixMesh_t1673343610 * __this, Int32U5BU5D_t3030399641* ___wrapped0, Mesh_t1356156583 * ___sphereMesh1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixMesh_FixWrappedUV_m498452425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1612828712 * V_0 = NULL;
	List_1_t1612828711 * V_1 = NULL;
	int32_t V_2 = 0;
	Dictionary_2_t1079703083 * V_3 = NULL;
	int32_t V_4 = 0;
	Int32U5BU5D_t3030399641* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t V_13 = 0;
	Vector2_t2243707579  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	Vector2_t2243707579  V_16;
	memset(&V_16, 0, sizeof(V_16));
	int32_t V_17 = 0;
	Vector2_t2243707579  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Int32U5BU5D_t3030399641* V_19 = NULL;
	{
		Mesh_t1356156583 * L_0 = ___sphereMesh1;
		NullCheck(L_0);
		Vector3U5BU5D_t1172311765* L_1 = Mesh_get_vertices_m626989480(L_0, /*hidden argument*/NULL);
		List_1_t1612828712 * L_2 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m1244050974(L_2, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1__ctor_m1244050974_MethodInfo_var);
		V_0 = L_2;
		Mesh_t1356156583 * L_3 = ___sphereMesh1;
		NullCheck(L_3);
		Vector2U5BU5D_t686124026* L_4 = Mesh_get_uv_m3811151337(L_3, /*hidden argument*/NULL);
		List_1_t1612828711 * L_5 = (List_1_t1612828711 *)il2cpp_codegen_object_new(List_1_t1612828711_il2cpp_TypeInfo_var);
		List_1__ctor_m798345663(L_5, (Il2CppObject*)(Il2CppObject*)L_4, /*hidden argument*/List_1__ctor_m798345663_MethodInfo_var);
		V_1 = L_5;
		List_1_t1612828712 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m40859504(L_6, /*hidden argument*/List_1_get_Count_m40859504_MethodInfo_var);
		V_2 = ((int32_t)((int32_t)L_7-(int32_t)1));
		Dictionary_2_t1079703083 * L_8 = (Dictionary_2_t1079703083 *)il2cpp_codegen_object_new(Dictionary_2_t1079703083_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2157787832(L_8, /*hidden argument*/Dictionary_2__ctor_m2157787832_MethodInfo_var);
		V_3 = L_8;
		Int32U5BU5D_t3030399641* L_9 = ___wrapped0;
		V_5 = L_9;
		V_6 = 0;
		goto IL_0243;
	}

IL_0034:
	{
		Int32U5BU5D_t3030399641* L_10 = V_5;
		int32_t L_11 = V_6;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		int32_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_4 = L_13;
		Mesh_t1356156583 * L_14 = ___sphereMesh1;
		NullCheck(L_14);
		Int32U5BU5D_t3030399641* L_15 = Mesh_get_triangles_m3988715512(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_4;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_7 = L_18;
		Mesh_t1356156583 * L_19 = ___sphereMesh1;
		NullCheck(L_19);
		Int32U5BU5D_t3030399641* L_20 = Mesh_get_triangles_m3988715512(L_19, /*hidden argument*/NULL);
		int32_t L_21 = V_4;
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)((int32_t)L_21+(int32_t)1));
		int32_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_8 = L_23;
		Mesh_t1356156583 * L_24 = ___sphereMesh1;
		NullCheck(L_24);
		Int32U5BU5D_t3030399641* L_25 = Mesh_get_triangles_m3988715512(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)((int32_t)L_26+(int32_t)2));
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_9 = L_28;
		Mesh_t1356156583 * L_29 = ___sphereMesh1;
		NullCheck(L_29);
		Vector3U5BU5D_t1172311765* L_30 = Mesh_get_vertices_m626989480(L_29, /*hidden argument*/NULL);
		int32_t L_31 = V_7;
		NullCheck(L_30);
		V_10 = (*(Vector3_t2243707580 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_31))));
		Mesh_t1356156583 * L_32 = ___sphereMesh1;
		NullCheck(L_32);
		Vector3U5BU5D_t1172311765* L_33 = Mesh_get_vertices_m626989480(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_8;
		NullCheck(L_33);
		V_11 = (*(Vector3_t2243707580 *)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34))));
		Mesh_t1356156583 * L_35 = ___sphereMesh1;
		NullCheck(L_35);
		Vector3U5BU5D_t1172311765* L_36 = Mesh_get_vertices_m626989480(L_35, /*hidden argument*/NULL);
		int32_t L_37 = V_9;
		NullCheck(L_36);
		V_12 = (*(Vector3_t2243707580 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37))));
		Mesh_t1356156583 * L_38 = ___sphereMesh1;
		NullCheck(L_38);
		Vector2U5BU5D_t686124026* L_39 = Mesh_get_uv_m3811151337(L_38, /*hidden argument*/NULL);
		int32_t L_40 = V_7;
		NullCheck(L_39);
		float L_41 = ((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))->get_x_0();
		if ((!(((float)L_41) < ((float)(0.25f)))))
		{
			goto IL_011b;
		}
	}
	{
		int32_t L_42 = V_7;
		V_13 = L_42;
		Dictionary_2_t1079703083 * L_43 = V_3;
		int32_t L_44 = V_7;
		NullCheck(L_43);
		bool L_45 = Dictionary_2_TryGetValue_m2332787485(L_43, L_44, (&V_13), /*hidden argument*/Dictionary_2_TryGetValue_m2332787485_MethodInfo_var);
		if (L_45)
		{
			goto IL_0116;
		}
	}
	{
		Mesh_t1356156583 * L_46 = ___sphereMesh1;
		NullCheck(L_46);
		Vector2U5BU5D_t686124026* L_47 = Mesh_get_uv_m3811151337(L_46, /*hidden argument*/NULL);
		int32_t L_48 = V_7;
		NullCheck(L_47);
		V_14 = (*(Vector2_t2243707579 *)((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector2_t2243707579 * L_49 = (&V_14);
		float L_50 = L_49->get_x_0();
		L_49->set_x_0(((float)((float)L_50+(float)(1.0f))));
		List_1_t1612828712 * L_51 = V_0;
		Vector3_t2243707580  L_52 = V_10;
		NullCheck(L_51);
		List_1_Add_m1552520334(L_51, L_52, /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828711 * L_53 = V_1;
		Vector2_t2243707579  L_54 = V_14;
		NullCheck(L_53);
		List_1_Add_m1515754733(L_53, L_54, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		int32_t L_55 = V_2;
		V_2 = ((int32_t)((int32_t)L_55+(int32_t)1));
		Dictionary_2_t1079703083 * L_56 = V_3;
		int32_t L_57 = V_7;
		int32_t L_58 = V_2;
		NullCheck(L_56);
		Dictionary_2_set_Item_m1233026737(L_56, L_57, L_58, /*hidden argument*/Dictionary_2_set_Item_m1233026737_MethodInfo_var);
		int32_t L_59 = V_2;
		V_13 = L_59;
	}

IL_0116:
	{
		int32_t L_60 = V_13;
		V_7 = L_60;
	}

IL_011b:
	{
		Mesh_t1356156583 * L_61 = ___sphereMesh1;
		NullCheck(L_61);
		Vector2U5BU5D_t686124026* L_62 = Mesh_get_uv_m3811151337(L_61, /*hidden argument*/NULL);
		int32_t L_63 = V_8;
		NullCheck(L_62);
		float L_64 = ((L_62)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_63)))->get_x_0();
		if ((!(((float)L_64) < ((float)(0.25f)))))
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_65 = V_8;
		V_15 = L_65;
		Dictionary_2_t1079703083 * L_66 = V_3;
		int32_t L_67 = V_8;
		NullCheck(L_66);
		bool L_68 = Dictionary_2_TryGetValue_m2332787485(L_66, L_67, (&V_15), /*hidden argument*/Dictionary_2_TryGetValue_m2332787485_MethodInfo_var);
		if (L_68)
		{
			goto IL_0194;
		}
	}
	{
		Mesh_t1356156583 * L_69 = ___sphereMesh1;
		NullCheck(L_69);
		Vector2U5BU5D_t686124026* L_70 = Mesh_get_uv_m3811151337(L_69, /*hidden argument*/NULL);
		int32_t L_71 = V_8;
		NullCheck(L_70);
		V_16 = (*(Vector2_t2243707579 *)((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_71))));
		Vector2_t2243707579 * L_72 = (&V_16);
		float L_73 = L_72->get_x_0();
		L_72->set_x_0(((float)((float)L_73+(float)(1.0f))));
		List_1_t1612828712 * L_74 = V_0;
		Vector3_t2243707580  L_75 = V_11;
		NullCheck(L_74);
		List_1_Add_m1552520334(L_74, L_75, /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828711 * L_76 = V_1;
		Vector2_t2243707579  L_77 = V_16;
		NullCheck(L_76);
		List_1_Add_m1515754733(L_76, L_77, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		int32_t L_78 = V_2;
		V_2 = ((int32_t)((int32_t)L_78+(int32_t)1));
		Dictionary_2_t1079703083 * L_79 = V_3;
		int32_t L_80 = V_8;
		int32_t L_81 = V_2;
		NullCheck(L_79);
		Dictionary_2_set_Item_m1233026737(L_79, L_80, L_81, /*hidden argument*/Dictionary_2_set_Item_m1233026737_MethodInfo_var);
		int32_t L_82 = V_2;
		V_15 = L_82;
	}

IL_0194:
	{
		int32_t L_83 = V_15;
		V_8 = L_83;
	}

IL_0199:
	{
		Mesh_t1356156583 * L_84 = ___sphereMesh1;
		NullCheck(L_84);
		Vector2U5BU5D_t686124026* L_85 = Mesh_get_uv_m3811151337(L_84, /*hidden argument*/NULL);
		int32_t L_86 = V_9;
		NullCheck(L_85);
		float L_87 = ((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_86)))->get_x_0();
		if ((!(((float)L_87) < ((float)(0.25f)))))
		{
			goto IL_0217;
		}
	}
	{
		int32_t L_88 = V_9;
		V_17 = L_88;
		Dictionary_2_t1079703083 * L_89 = V_3;
		int32_t L_90 = V_9;
		NullCheck(L_89);
		bool L_91 = Dictionary_2_TryGetValue_m2332787485(L_89, L_90, (&V_17), /*hidden argument*/Dictionary_2_TryGetValue_m2332787485_MethodInfo_var);
		if (L_91)
		{
			goto IL_0212;
		}
	}
	{
		Mesh_t1356156583 * L_92 = ___sphereMesh1;
		NullCheck(L_92);
		Vector2U5BU5D_t686124026* L_93 = Mesh_get_uv_m3811151337(L_92, /*hidden argument*/NULL);
		int32_t L_94 = V_9;
		NullCheck(L_93);
		V_18 = (*(Vector2_t2243707579 *)((L_93)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_94))));
		Vector2_t2243707579 * L_95 = (&V_18);
		float L_96 = L_95->get_x_0();
		L_95->set_x_0(((float)((float)L_96+(float)(1.0f))));
		List_1_t1612828712 * L_97 = V_0;
		Vector3_t2243707580  L_98 = V_12;
		NullCheck(L_97);
		List_1_Add_m1552520334(L_97, L_98, /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828711 * L_99 = V_1;
		Vector2_t2243707579  L_100 = V_18;
		NullCheck(L_99);
		List_1_Add_m1515754733(L_99, L_100, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		int32_t L_101 = V_2;
		V_2 = ((int32_t)((int32_t)L_101+(int32_t)1));
		Dictionary_2_t1079703083 * L_102 = V_3;
		int32_t L_103 = V_9;
		int32_t L_104 = V_2;
		NullCheck(L_102);
		Dictionary_2_set_Item_m1233026737(L_102, L_103, L_104, /*hidden argument*/Dictionary_2_set_Item_m1233026737_MethodInfo_var);
		int32_t L_105 = V_2;
		V_17 = L_105;
	}

IL_0212:
	{
		int32_t L_106 = V_17;
		V_9 = L_106;
	}

IL_0217:
	{
		Mesh_t1356156583 * L_107 = ___sphereMesh1;
		NullCheck(L_107);
		Int32U5BU5D_t3030399641* L_108 = Mesh_get_triangles_m3988715512(L_107, /*hidden argument*/NULL);
		int32_t L_109 = V_4;
		int32_t L_110 = V_7;
		NullCheck(L_108);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(L_109), (int32_t)L_110);
		Mesh_t1356156583 * L_111 = ___sphereMesh1;
		NullCheck(L_111);
		Int32U5BU5D_t3030399641* L_112 = Mesh_get_triangles_m3988715512(L_111, /*hidden argument*/NULL);
		int32_t L_113 = V_4;
		int32_t L_114 = V_8;
		NullCheck(L_112);
		(L_112)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_113+(int32_t)1))), (int32_t)L_114);
		Mesh_t1356156583 * L_115 = ___sphereMesh1;
		NullCheck(L_115);
		Int32U5BU5D_t3030399641* L_116 = Mesh_get_triangles_m3988715512(L_115, /*hidden argument*/NULL);
		int32_t L_117 = V_4;
		int32_t L_118 = V_9;
		NullCheck(L_116);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_117+(int32_t)2))), (int32_t)L_118);
		int32_t L_119 = V_6;
		V_6 = ((int32_t)((int32_t)L_119+(int32_t)1));
	}

IL_0243:
	{
		int32_t L_120 = V_6;
		Int32U5BU5D_t3030399641* L_121 = V_5;
		NullCheck(L_121);
		if ((((int32_t)L_120) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_121)->max_length)))))))
		{
			goto IL_0034;
		}
	}
	{
		Mesh_t1356156583 * L_122 = ___sphereMesh1;
		NullCheck(L_122);
		Int32U5BU5D_t3030399641* L_123 = Mesh_get_triangles_m3988715512(L_122, /*hidden argument*/NULL);
		V_19 = L_123;
		Mesh_t1356156583 * L_124 = ___sphereMesh1;
		NullCheck(L_124);
		Mesh_Clear_m231813403(L_124, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_125 = ___sphereMesh1;
		List_1_t1612828712 * L_126 = V_0;
		NullCheck(L_126);
		Vector3U5BU5D_t1172311765* L_127 = List_1_ToArray_m2543904144(L_126, /*hidden argument*/List_1_ToArray_m2543904144_MethodInfo_var);
		NullCheck(L_125);
		Mesh_set_vertices_m2936804213(L_125, L_127, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_128 = ___sphereMesh1;
		List_1_t1612828711 * L_129 = V_1;
		NullCheck(L_129);
		Vector2U5BU5D_t686124026* L_130 = List_1_ToArray_m4024240619(L_129, /*hidden argument*/List_1_ToArray_m4024240619_MethodInfo_var);
		NullCheck(L_128);
		Mesh_set_uv_m1497318906(L_128, L_130, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_131 = ___sphereMesh1;
		Int32U5BU5D_t3030399641* L_132 = V_19;
		NullCheck(L_131);
		Mesh_set_triangles_m3244966865(L_131, L_132, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_133 = ___sphereMesh1;
		NullCheck(L_133);
		Mesh_RecalculateNormals_m1034493793(L_133, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_134 = ___sphereMesh1;
		NullCheck(L_134);
		Mesh_RecalculateBounds_m3559909024(L_134, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FixMesh::CorrectSeam(UnityEngine.Mesh)
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612828711_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1079703083_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1244050974_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m798345663_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2157787832_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2338974797_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1308582362_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1465587600_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1552520334_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1515754733_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m40859504_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3036760224_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2543904144_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4024240619_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3453833174_MethodInfo_var;
extern const uint32_t FixMesh_CorrectSeam_m3944530567_MetadataUsageId;
extern "C"  void FixMesh_CorrectSeam_m3944530567 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixMesh_CorrectSeam_m3944530567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1440998580 * V_0 = NULL;
	List_1_t1612828712 * V_1 = NULL;
	List_1_t1612828711 * V_2 = NULL;
	Dictionary_2_t1079703083 * V_3 = NULL;
	bool V_4 = false;
	int32_t V_5 = 0;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector2_t2243707579  V_13;
	memset(&V_13, 0, sizeof(V_13));
	int32_t V_14 = 0;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_0, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		V_0 = L_0;
		Mesh_t1356156583 * L_1 = ___mesh0;
		NullCheck(L_1);
		Vector3U5BU5D_t1172311765* L_2 = Mesh_get_vertices_m626989480(L_1, /*hidden argument*/NULL);
		List_1_t1612828712 * L_3 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m1244050974(L_3, (Il2CppObject*)(Il2CppObject*)L_2, /*hidden argument*/List_1__ctor_m1244050974_MethodInfo_var);
		V_1 = L_3;
		Mesh_t1356156583 * L_4 = ___mesh0;
		NullCheck(L_4);
		Vector2U5BU5D_t686124026* L_5 = Mesh_get_uv_m3811151337(L_4, /*hidden argument*/NULL);
		List_1_t1612828711 * L_6 = (List_1_t1612828711 *)il2cpp_codegen_object_new(List_1_t1612828711_il2cpp_TypeInfo_var);
		List_1__ctor_m798345663(L_6, (Il2CppObject*)(Il2CppObject*)L_5, /*hidden argument*/List_1__ctor_m798345663_MethodInfo_var);
		V_2 = L_6;
		Dictionary_2_t1079703083 * L_7 = (Dictionary_2_t1079703083 *)il2cpp_codegen_object_new(Dictionary_2_t1079703083_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2157787832(L_7, /*hidden argument*/Dictionary_2__ctor_m2157787832_MethodInfo_var);
		V_3 = L_7;
		V_4 = (bool)0;
		Mesh_t1356156583 * L_8 = ___mesh0;
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = Mesh_get_triangles_m3988715512(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		V_5 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)3));
		goto IL_0261;
	}

IL_0039:
	{
		Mesh_t1356156583 * L_10 = ___mesh0;
		NullCheck(L_10);
		Vector2U5BU5D_t686124026* L_11 = Mesh_get_uv_m3811151337(L_10, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_12 = ___mesh0;
		NullCheck(L_12);
		Int32U5BU5D_t3030399641* L_13 = Mesh_get_triangles_m3988715512(L_12, /*hidden argument*/NULL);
		int32_t L_14 = V_5;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_11);
		float L_17 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->get_x_0();
		Mesh_t1356156583 * L_18 = ___mesh0;
		NullCheck(L_18);
		Vector2U5BU5D_t686124026* L_19 = Mesh_get_uv_m3811151337(L_18, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_20 = ___mesh0;
		NullCheck(L_20);
		Int32U5BU5D_t3030399641* L_21 = Mesh_get_triangles_m3988715512(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_5;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_19);
		float L_25 = ((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_y_1();
		Vector3__ctor_m2638739322((&V_6), L_17, L_25, (0.0f), /*hidden argument*/NULL);
		Mesh_t1356156583 * L_26 = ___mesh0;
		NullCheck(L_26);
		Vector2U5BU5D_t686124026* L_27 = Mesh_get_uv_m3811151337(L_26, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_28 = ___mesh0;
		NullCheck(L_28);
		Int32U5BU5D_t3030399641* L_29 = Mesh_get_triangles_m3988715512(L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_5;
		NullCheck(L_29);
		int32_t L_31 = ((int32_t)((int32_t)L_30+(int32_t)1));
		int32_t L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_27);
		float L_33 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))->get_x_0();
		Mesh_t1356156583 * L_34 = ___mesh0;
		NullCheck(L_34);
		Vector2U5BU5D_t686124026* L_35 = Mesh_get_uv_m3811151337(L_34, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_36 = ___mesh0;
		NullCheck(L_36);
		Int32U5BU5D_t3030399641* L_37 = Mesh_get_triangles_m3988715512(L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_5;
		NullCheck(L_37);
		int32_t L_39 = ((int32_t)((int32_t)L_38+(int32_t)1));
		int32_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_35);
		float L_41 = ((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))->get_y_1();
		Vector3__ctor_m2638739322((&V_7), L_33, L_41, (0.0f), /*hidden argument*/NULL);
		Mesh_t1356156583 * L_42 = ___mesh0;
		NullCheck(L_42);
		Vector2U5BU5D_t686124026* L_43 = Mesh_get_uv_m3811151337(L_42, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_44 = ___mesh0;
		NullCheck(L_44);
		Int32U5BU5D_t3030399641* L_45 = Mesh_get_triangles_m3988715512(L_44, /*hidden argument*/NULL);
		int32_t L_46 = V_5;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_43);
		float L_49 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48)))->get_x_0();
		Mesh_t1356156583 * L_50 = ___mesh0;
		NullCheck(L_50);
		Vector2U5BU5D_t686124026* L_51 = Mesh_get_uv_m3811151337(L_50, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_52 = ___mesh0;
		NullCheck(L_52);
		Int32U5BU5D_t3030399641* L_53 = Mesh_get_triangles_m3988715512(L_52, /*hidden argument*/NULL);
		int32_t L_54 = V_5;
		NullCheck(L_53);
		int32_t L_55 = ((int32_t)((int32_t)L_54+(int32_t)2));
		int32_t L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_51);
		float L_57 = ((L_51)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_56)))->get_y_1();
		Vector3__ctor_m2638739322((&V_8), L_49, L_57, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_58 = V_6;
		Vector3_t2243707580  L_59 = V_7;
		Vector3_t2243707580  L_60 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		Vector3_t2243707580  L_61 = V_8;
		Vector3_t2243707580  L_62 = V_7;
		Vector3_t2243707580  L_63 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		Vector3_t2243707580  L_64 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_60, L_63, /*hidden argument*/NULL);
		V_9 = L_64;
		bool L_65 = V_4;
		if (!L_65)
		{
			goto IL_012d;
		}
	}
	{
		float L_66 = (&V_9)->get_z_3();
		if ((((float)L_66) <= ((float)(0.0f))))
		{
			goto IL_0145;
		}
	}

IL_012d:
	{
		bool L_67 = V_4;
		if (L_67)
		{
			goto IL_0227;
		}
	}
	{
		float L_68 = (&V_9)->get_z_3();
		if ((!(((float)L_68) > ((float)(0.0f)))))
		{
			goto IL_0227;
		}
	}

IL_0145:
	{
		int32_t L_69 = V_5;
		V_10 = L_69;
		goto IL_0216;
	}

IL_014f:
	{
		Mesh_t1356156583 * L_70 = ___mesh0;
		NullCheck(L_70);
		Int32U5BU5D_t3030399641* L_71 = Mesh_get_triangles_m3988715512(L_70, /*hidden argument*/NULL);
		int32_t L_72 = V_10;
		NullCheck(L_71);
		int32_t L_73 = L_72;
		int32_t L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		V_11 = L_74;
		Mesh_t1356156583 * L_75 = ___mesh0;
		NullCheck(L_75);
		Vector3U5BU5D_t1172311765* L_76 = Mesh_get_vertices_m626989480(L_75, /*hidden argument*/NULL);
		int32_t L_77 = V_11;
		NullCheck(L_76);
		V_12 = (*(Vector3_t2243707580 *)((L_76)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_77))));
		Mesh_t1356156583 * L_78 = ___mesh0;
		NullCheck(L_78);
		Vector2U5BU5D_t686124026* L_79 = Mesh_get_uv_m3811151337(L_78, /*hidden argument*/NULL);
		int32_t L_80 = V_11;
		NullCheck(L_79);
		float L_81 = ((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_80)))->get_x_0();
		if ((!(((float)L_81) >= ((float)(0.9f)))))
		{
			goto IL_0207;
		}
	}
	{
		Dictionary_2_t1079703083 * L_82 = V_3;
		int32_t L_83 = V_11;
		NullCheck(L_82);
		bool L_84 = Dictionary_2_ContainsKey_m2338974797(L_82, L_83, /*hidden argument*/Dictionary_2_ContainsKey_m2338974797_MethodInfo_var);
		if (!L_84)
		{
			goto IL_01ac;
		}
	}
	{
		List_1_t1440998580 * L_85 = V_0;
		Dictionary_2_t1079703083 * L_86 = V_3;
		int32_t L_87 = V_11;
		NullCheck(L_86);
		int32_t L_88 = Dictionary_2_get_Item_m1308582362(L_86, L_87, /*hidden argument*/Dictionary_2_get_Item_m1308582362_MethodInfo_var);
		NullCheck(L_85);
		List_1_Add_m1465587600(L_85, L_88, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
		goto IL_0201;
	}

IL_01ac:
	{
		Mesh_t1356156583 * L_89 = ___mesh0;
		NullCheck(L_89);
		Vector2U5BU5D_t686124026* L_90 = Mesh_get_uv_m3811151337(L_89, /*hidden argument*/NULL);
		int32_t L_91 = V_11;
		NullCheck(L_90);
		V_13 = (*(Vector2_t2243707579 *)((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_91))));
		Vector2_t2243707579 * L_92 = (&V_13);
		float L_93 = L_92->get_x_0();
		L_92->set_x_0(((float)((float)L_93-(float)(1.0f))));
		List_1_t1612828712 * L_94 = V_1;
		Vector3_t2243707580  L_95 = V_12;
		NullCheck(L_94);
		List_1_Add_m1552520334(L_94, L_95, /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828711 * L_96 = V_2;
		Vector2_t2243707579  L_97 = V_13;
		NullCheck(L_96);
		List_1_Add_m1515754733(L_96, L_97, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		List_1_t1612828712 * L_98 = V_1;
		NullCheck(L_98);
		int32_t L_99 = List_1_get_Count_m40859504(L_98, /*hidden argument*/List_1_get_Count_m40859504_MethodInfo_var);
		V_14 = ((int32_t)((int32_t)L_99-(int32_t)1));
		Dictionary_2_t1079703083 * L_100 = V_3;
		int32_t L_101 = V_11;
		int32_t L_102 = V_14;
		NullCheck(L_100);
		Dictionary_2_Add_m3036760224(L_100, L_101, L_102, /*hidden argument*/Dictionary_2_Add_m3036760224_MethodInfo_var);
		List_1_t1440998580 * L_103 = V_0;
		int32_t L_104 = V_14;
		NullCheck(L_103);
		List_1_Add_m1465587600(L_103, L_104, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
	}

IL_0201:
	{
		goto IL_020f;
	}

IL_0207:
	{
		List_1_t1440998580 * L_105 = V_0;
		int32_t L_106 = V_11;
		NullCheck(L_105);
		List_1_Add_m1465587600(L_105, L_106, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
	}

IL_020f:
	{
		int32_t L_107 = V_10;
		V_10 = ((int32_t)((int32_t)L_107+(int32_t)1));
	}

IL_0216:
	{
		int32_t L_108 = V_10;
		int32_t L_109 = V_5;
		if ((((int32_t)L_108) < ((int32_t)((int32_t)((int32_t)L_109+(int32_t)3)))))
		{
			goto IL_014f;
		}
	}
	{
		goto IL_025a;
	}

IL_0227:
	{
		List_1_t1440998580 * L_110 = V_0;
		Mesh_t1356156583 * L_111 = ___mesh0;
		NullCheck(L_111);
		Int32U5BU5D_t3030399641* L_112 = Mesh_get_triangles_m3988715512(L_111, /*hidden argument*/NULL);
		int32_t L_113 = V_5;
		NullCheck(L_112);
		int32_t L_114 = L_113;
		int32_t L_115 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_114));
		NullCheck(L_110);
		List_1_Add_m1465587600(L_110, L_115, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
		List_1_t1440998580 * L_116 = V_0;
		Mesh_t1356156583 * L_117 = ___mesh0;
		NullCheck(L_117);
		Int32U5BU5D_t3030399641* L_118 = Mesh_get_triangles_m3988715512(L_117, /*hidden argument*/NULL);
		int32_t L_119 = V_5;
		NullCheck(L_118);
		int32_t L_120 = ((int32_t)((int32_t)L_119+(int32_t)1));
		int32_t L_121 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		NullCheck(L_116);
		List_1_Add_m1465587600(L_116, L_121, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
		List_1_t1440998580 * L_122 = V_0;
		Mesh_t1356156583 * L_123 = ___mesh0;
		NullCheck(L_123);
		Int32U5BU5D_t3030399641* L_124 = Mesh_get_triangles_m3988715512(L_123, /*hidden argument*/NULL);
		int32_t L_125 = V_5;
		NullCheck(L_124);
		int32_t L_126 = ((int32_t)((int32_t)L_125+(int32_t)2));
		int32_t L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		NullCheck(L_122);
		List_1_Add_m1465587600(L_122, L_127, /*hidden argument*/List_1_Add_m1465587600_MethodInfo_var);
	}

IL_025a:
	{
		int32_t L_128 = V_5;
		V_5 = ((int32_t)((int32_t)L_128-(int32_t)3));
	}

IL_0261:
	{
		int32_t L_129 = V_5;
		if ((((int32_t)L_129) >= ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		Mesh_t1356156583 * L_130 = ___mesh0;
		NullCheck(L_130);
		Mesh_Clear_m231813403(L_130, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_131 = ___mesh0;
		List_1_t1612828712 * L_132 = V_1;
		NullCheck(L_132);
		Vector3U5BU5D_t1172311765* L_133 = List_1_ToArray_m2543904144(L_132, /*hidden argument*/List_1_ToArray_m2543904144_MethodInfo_var);
		NullCheck(L_131);
		Mesh_set_vertices_m2936804213(L_131, L_133, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_134 = ___mesh0;
		List_1_t1612828711 * L_135 = V_2;
		NullCheck(L_135);
		Vector2U5BU5D_t686124026* L_136 = List_1_ToArray_m4024240619(L_135, /*hidden argument*/List_1_ToArray_m4024240619_MethodInfo_var);
		NullCheck(L_134);
		Mesh_set_uv_m1497318906(L_134, L_136, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_137 = ___mesh0;
		List_1_t1440998580 * L_138 = V_0;
		NullCheck(L_138);
		Int32U5BU5D_t3030399641* L_139 = List_1_ToArray_m3453833174(L_138, /*hidden argument*/List_1_ToArray_m3453833174_MethodInfo_var);
		NullCheck(L_137);
		Mesh_set_triangles_m3244966865(L_137, L_139, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FixMesh::correctPole(UnityEngine.Mesh)
extern Il2CppClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612828711_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1244050974_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m798345663_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2961219472_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m40859504_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1552520334_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4187614919_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1515754733_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m635251882_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2543904144_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4024240619_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3453833174_MethodInfo_var;
extern const uint32_t FixMesh_correctPole_m3924534843_MetadataUsageId;
extern "C"  void FixMesh_correctPole_m3924534843 (FixMesh_t1673343610 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixMesh_correctPole_m3924534843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1612828712 * V_0 = NULL;
	List_1_t1612828711 * V_1 = NULL;
	List_1_t1440998580 * V_2 = NULL;
	int32_t V_3 = 0;
	List_1_t1440998580 * V_4 = NULL;
	int32_t V_5 = 0;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector2_t2243707579  V_13;
	memset(&V_13, 0, sizeof(V_13));
	float V_14 = 0.0f;
	{
		Mesh_t1356156583 * L_0 = ___mesh0;
		NullCheck(L_0);
		Vector3U5BU5D_t1172311765* L_1 = Mesh_get_vertices_m626989480(L_0, /*hidden argument*/NULL);
		List_1_t1612828712 * L_2 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m1244050974(L_2, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1__ctor_m1244050974_MethodInfo_var);
		V_0 = L_2;
		Mesh_t1356156583 * L_3 = ___mesh0;
		NullCheck(L_3);
		Vector2U5BU5D_t686124026* L_4 = Mesh_get_uv_m3811151337(L_3, /*hidden argument*/NULL);
		List_1_t1612828711 * L_5 = (List_1_t1612828711 *)il2cpp_codegen_object_new(List_1_t1612828711_il2cpp_TypeInfo_var);
		List_1__ctor_m798345663(L_5, (Il2CppObject*)(Il2CppObject*)L_4, /*hidden argument*/List_1__ctor_m798345663_MethodInfo_var);
		V_1 = L_5;
		Mesh_t1356156583 * L_6 = ___mesh0;
		NullCheck(L_6);
		Int32U5BU5D_t3030399641* L_7 = Mesh_get_triangles_m3988715512(L_6, /*hidden argument*/NULL);
		List_1_t1440998580 * L_8 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m2961219472(L_8, (Il2CppObject*)(Il2CppObject*)L_7, /*hidden argument*/List_1__ctor_m2961219472_MethodInfo_var);
		V_2 = L_8;
		List_1_t1612828712 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m40859504(L_9, /*hidden argument*/List_1_get_Count_m40859504_MethodInfo_var);
		V_3 = ((int32_t)((int32_t)L_10-(int32_t)1));
		List_1_t1440998580 * L_11 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_11, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		V_4 = L_11;
		V_5 = 0;
		goto IL_0338;
	}

IL_003d:
	{
		Mesh_t1356156583 * L_12 = ___mesh0;
		NullCheck(L_12);
		Vector2U5BU5D_t686124026* L_13 = Mesh_get_uv_m3811151337(L_12, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_14 = ___mesh0;
		NullCheck(L_14);
		Int32U5BU5D_t3030399641* L_15 = Mesh_get_triangles_m3988715512(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_5;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_13);
		float L_19 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))->get_y_1();
		if ((((float)L_19) > ((float)(0.99f))))
		{
			goto IL_0084;
		}
	}
	{
		Mesh_t1356156583 * L_20 = ___mesh0;
		NullCheck(L_20);
		Vector2U5BU5D_t686124026* L_21 = Mesh_get_uv_m3811151337(L_20, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_22 = ___mesh0;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		int32_t L_24 = V_5;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		int32_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_21);
		float L_27 = ((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_y_1();
		if ((!(((float)L_27) < ((float)(0.01f)))))
		{
			goto IL_0136;
		}
	}

IL_0084:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
		List_1_t1612828712 * L_29 = V_0;
		Mesh_t1356156583 * L_30 = ___mesh0;
		NullCheck(L_30);
		Vector3U5BU5D_t1172311765* L_31 = Mesh_get_vertices_m626989480(L_30, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_32 = ___mesh0;
		NullCheck(L_32);
		Int32U5BU5D_t3030399641* L_33 = Mesh_get_triangles_m3988715512(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_5;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_31);
		NullCheck(L_29);
		List_1_Add_m1552520334(L_29, (*(Vector3_t2243707580 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36)))), /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828712 * L_37 = V_0;
		int32_t L_38 = V_3;
		NullCheck(L_37);
		Vector3_t2243707580  L_39 = List_1_get_Item_m4187614919(L_37, L_38, /*hidden argument*/List_1_get_Item_m4187614919_MethodInfo_var);
		V_6 = L_39;
		V_8 = (0.1f);
		Mesh_t1356156583 * L_40 = ___mesh0;
		NullCheck(L_40);
		Vector2U5BU5D_t686124026* L_41 = Mesh_get_uv_m3811151337(L_40, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_42 = ___mesh0;
		NullCheck(L_42);
		Int32U5BU5D_t3030399641* L_43 = Mesh_get_triangles_m3988715512(L_42, /*hidden argument*/NULL);
		int32_t L_44 = V_5;
		NullCheck(L_43);
		int32_t L_45 = ((int32_t)((int32_t)L_44+(int32_t)1));
		int32_t L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_41);
		float L_47 = ((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->get_x_0();
		Mesh_t1356156583 * L_48 = ___mesh0;
		NullCheck(L_48);
		Vector2U5BU5D_t686124026* L_49 = Mesh_get_uv_m3811151337(L_48, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_50 = ___mesh0;
		NullCheck(L_50);
		Int32U5BU5D_t3030399641* L_51 = Mesh_get_triangles_m3988715512(L_50, /*hidden argument*/NULL);
		int32_t L_52 = V_5;
		NullCheck(L_51);
		int32_t L_53 = ((int32_t)((int32_t)L_52+(int32_t)2));
		int32_t L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		NullCheck(L_49);
		float L_55 = ((L_49)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_54)))->get_x_0();
		float L_56 = V_8;
		(&V_7)->set_x_0(((float)((float)((float)((float)((float)((float)L_47+(float)L_55))*(float)(0.5f)))+(float)L_56)));
		Mesh_t1356156583 * L_57 = ___mesh0;
		NullCheck(L_57);
		Vector2U5BU5D_t686124026* L_58 = Mesh_get_uv_m3811151337(L_57, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_59 = ___mesh0;
		NullCheck(L_59);
		Int32U5BU5D_t3030399641* L_60 = Mesh_get_triangles_m3988715512(L_59, /*hidden argument*/NULL);
		int32_t L_61 = V_5;
		NullCheck(L_60);
		int32_t L_62 = L_61;
		int32_t L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		NullCheck(L_58);
		float L_64 = ((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_63)))->get_y_1();
		(&V_7)->set_y_1(L_64);
		List_1_t1612828711 * L_65 = V_1;
		Vector2_t2243707579  L_66 = V_7;
		NullCheck(L_65);
		List_1_Add_m1515754733(L_65, L_66, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		List_1_t1440998580 * L_67 = V_2;
		int32_t L_68 = V_5;
		int32_t L_69 = V_3;
		NullCheck(L_67);
		List_1_set_Item_m635251882(L_67, L_68, L_69, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
		goto IL_0331;
	}

IL_0136:
	{
		Mesh_t1356156583 * L_70 = ___mesh0;
		NullCheck(L_70);
		Vector2U5BU5D_t686124026* L_71 = Mesh_get_uv_m3811151337(L_70, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_72 = ___mesh0;
		NullCheck(L_72);
		Int32U5BU5D_t3030399641* L_73 = Mesh_get_triangles_m3988715512(L_72, /*hidden argument*/NULL);
		int32_t L_74 = V_5;
		NullCheck(L_73);
		int32_t L_75 = ((int32_t)((int32_t)L_74+(int32_t)1));
		int32_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		NullCheck(L_71);
		float L_77 = ((L_71)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_76)))->get_y_1();
		if ((((float)L_77) > ((float)(0.99f))))
		{
			goto IL_0180;
		}
	}
	{
		Mesh_t1356156583 * L_78 = ___mesh0;
		NullCheck(L_78);
		Vector2U5BU5D_t686124026* L_79 = Mesh_get_uv_m3811151337(L_78, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_80 = ___mesh0;
		NullCheck(L_80);
		Int32U5BU5D_t3030399641* L_81 = Mesh_get_triangles_m3988715512(L_80, /*hidden argument*/NULL);
		int32_t L_82 = V_5;
		NullCheck(L_81);
		int32_t L_83 = ((int32_t)((int32_t)L_82+(int32_t)1));
		int32_t L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		NullCheck(L_79);
		float L_85 = ((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_84)))->get_y_1();
		if ((!(((float)L_85) < ((float)(0.01f)))))
		{
			goto IL_0236;
		}
	}

IL_0180:
	{
		int32_t L_86 = V_3;
		V_3 = ((int32_t)((int32_t)L_86+(int32_t)1));
		List_1_t1612828712 * L_87 = V_0;
		Mesh_t1356156583 * L_88 = ___mesh0;
		NullCheck(L_88);
		Vector3U5BU5D_t1172311765* L_89 = Mesh_get_vertices_m626989480(L_88, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_90 = ___mesh0;
		NullCheck(L_90);
		Int32U5BU5D_t3030399641* L_91 = Mesh_get_triangles_m3988715512(L_90, /*hidden argument*/NULL);
		int32_t L_92 = V_5;
		NullCheck(L_91);
		int32_t L_93 = ((int32_t)((int32_t)L_92+(int32_t)1));
		int32_t L_94 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		NullCheck(L_89);
		NullCheck(L_87);
		List_1_Add_m1552520334(L_87, (*(Vector3_t2243707580 *)((L_89)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_94)))), /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828712 * L_95 = V_0;
		int32_t L_96 = V_3;
		NullCheck(L_95);
		Vector3_t2243707580  L_97 = List_1_get_Item_m4187614919(L_95, L_96, /*hidden argument*/List_1_get_Item_m4187614919_MethodInfo_var);
		V_9 = L_97;
		V_11 = (0.1f);
		Mesh_t1356156583 * L_98 = ___mesh0;
		NullCheck(L_98);
		Vector2U5BU5D_t686124026* L_99 = Mesh_get_uv_m3811151337(L_98, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_100 = ___mesh0;
		NullCheck(L_100);
		Int32U5BU5D_t3030399641* L_101 = Mesh_get_triangles_m3988715512(L_100, /*hidden argument*/NULL);
		int32_t L_102 = V_5;
		NullCheck(L_101);
		int32_t L_103 = L_102;
		int32_t L_104 = (L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		NullCheck(L_99);
		float L_105 = ((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_104)))->get_x_0();
		Mesh_t1356156583 * L_106 = ___mesh0;
		NullCheck(L_106);
		Vector2U5BU5D_t686124026* L_107 = Mesh_get_uv_m3811151337(L_106, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_108 = ___mesh0;
		NullCheck(L_108);
		Int32U5BU5D_t3030399641* L_109 = Mesh_get_triangles_m3988715512(L_108, /*hidden argument*/NULL);
		int32_t L_110 = V_5;
		NullCheck(L_109);
		int32_t L_111 = ((int32_t)((int32_t)L_110+(int32_t)2));
		int32_t L_112 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		NullCheck(L_107);
		float L_113 = ((L_107)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_112)))->get_x_0();
		float L_114 = V_11;
		(&V_10)->set_x_0(((float)((float)((float)((float)((float)((float)L_105+(float)L_113))*(float)(0.5f)))+(float)L_114)));
		Mesh_t1356156583 * L_115 = ___mesh0;
		NullCheck(L_115);
		Vector2U5BU5D_t686124026* L_116 = Mesh_get_uv_m3811151337(L_115, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_117 = ___mesh0;
		NullCheck(L_117);
		Int32U5BU5D_t3030399641* L_118 = Mesh_get_triangles_m3988715512(L_117, /*hidden argument*/NULL);
		int32_t L_119 = V_5;
		NullCheck(L_118);
		int32_t L_120 = ((int32_t)((int32_t)L_119+(int32_t)1));
		int32_t L_121 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		NullCheck(L_116);
		float L_122 = ((L_116)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_121)))->get_y_1();
		(&V_10)->set_y_1(L_122);
		List_1_t1612828711 * L_123 = V_1;
		Vector2_t2243707579  L_124 = V_10;
		NullCheck(L_123);
		List_1_Add_m1515754733(L_123, L_124, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		List_1_t1440998580 * L_125 = V_2;
		int32_t L_126 = V_5;
		int32_t L_127 = V_3;
		NullCheck(L_125);
		List_1_set_Item_m635251882(L_125, ((int32_t)((int32_t)L_126+(int32_t)1)), L_127, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
		goto IL_0331;
	}

IL_0236:
	{
		Mesh_t1356156583 * L_128 = ___mesh0;
		NullCheck(L_128);
		Vector2U5BU5D_t686124026* L_129 = Mesh_get_uv_m3811151337(L_128, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_130 = ___mesh0;
		NullCheck(L_130);
		Int32U5BU5D_t3030399641* L_131 = Mesh_get_triangles_m3988715512(L_130, /*hidden argument*/NULL);
		int32_t L_132 = V_5;
		NullCheck(L_131);
		int32_t L_133 = ((int32_t)((int32_t)L_132+(int32_t)2));
		int32_t L_134 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		NullCheck(L_129);
		float L_135 = ((L_129)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_134)))->get_y_1();
		if ((((float)L_135) > ((float)(0.99f))))
		{
			goto IL_0280;
		}
	}
	{
		Mesh_t1356156583 * L_136 = ___mesh0;
		NullCheck(L_136);
		Vector2U5BU5D_t686124026* L_137 = Mesh_get_uv_m3811151337(L_136, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_138 = ___mesh0;
		NullCheck(L_138);
		Int32U5BU5D_t3030399641* L_139 = Mesh_get_triangles_m3988715512(L_138, /*hidden argument*/NULL);
		int32_t L_140 = V_5;
		NullCheck(L_139);
		int32_t L_141 = ((int32_t)((int32_t)L_140+(int32_t)2));
		int32_t L_142 = (L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		NullCheck(L_137);
		float L_143 = ((L_137)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_142)))->get_y_1();
		if ((!(((float)L_143) < ((float)(0.01f)))))
		{
			goto IL_0331;
		}
	}

IL_0280:
	{
		int32_t L_144 = V_3;
		V_3 = ((int32_t)((int32_t)L_144+(int32_t)1));
		List_1_t1612828712 * L_145 = V_0;
		Mesh_t1356156583 * L_146 = ___mesh0;
		NullCheck(L_146);
		Vector3U5BU5D_t1172311765* L_147 = Mesh_get_vertices_m626989480(L_146, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_148 = ___mesh0;
		NullCheck(L_148);
		Int32U5BU5D_t3030399641* L_149 = Mesh_get_triangles_m3988715512(L_148, /*hidden argument*/NULL);
		int32_t L_150 = V_5;
		NullCheck(L_149);
		int32_t L_151 = ((int32_t)((int32_t)L_150+(int32_t)2));
		int32_t L_152 = (L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151));
		NullCheck(L_147);
		NullCheck(L_145);
		List_1_Add_m1552520334(L_145, (*(Vector3_t2243707580 *)((L_147)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_152)))), /*hidden argument*/List_1_Add_m1552520334_MethodInfo_var);
		List_1_t1612828712 * L_153 = V_0;
		int32_t L_154 = V_3;
		NullCheck(L_153);
		Vector3_t2243707580  L_155 = List_1_get_Item_m4187614919(L_153, L_154, /*hidden argument*/List_1_get_Item_m4187614919_MethodInfo_var);
		V_12 = L_155;
		V_14 = (0.1f);
		Mesh_t1356156583 * L_156 = ___mesh0;
		NullCheck(L_156);
		Vector2U5BU5D_t686124026* L_157 = Mesh_get_uv_m3811151337(L_156, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_158 = ___mesh0;
		NullCheck(L_158);
		Int32U5BU5D_t3030399641* L_159 = Mesh_get_triangles_m3988715512(L_158, /*hidden argument*/NULL);
		int32_t L_160 = V_5;
		NullCheck(L_159);
		int32_t L_161 = L_160;
		int32_t L_162 = (L_159)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		NullCheck(L_157);
		float L_163 = ((L_157)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_162)))->get_x_0();
		Mesh_t1356156583 * L_164 = ___mesh0;
		NullCheck(L_164);
		Vector2U5BU5D_t686124026* L_165 = Mesh_get_uv_m3811151337(L_164, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_166 = ___mesh0;
		NullCheck(L_166);
		Int32U5BU5D_t3030399641* L_167 = Mesh_get_triangles_m3988715512(L_166, /*hidden argument*/NULL);
		int32_t L_168 = V_5;
		NullCheck(L_167);
		int32_t L_169 = ((int32_t)((int32_t)L_168+(int32_t)1));
		int32_t L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		NullCheck(L_165);
		float L_171 = ((L_165)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_170)))->get_x_0();
		float L_172 = V_14;
		(&V_13)->set_x_0(((float)((float)((float)((float)((float)((float)L_163+(float)L_171))*(float)(0.5f)))+(float)L_172)));
		Mesh_t1356156583 * L_173 = ___mesh0;
		NullCheck(L_173);
		Vector2U5BU5D_t686124026* L_174 = Mesh_get_uv_m3811151337(L_173, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_175 = ___mesh0;
		NullCheck(L_175);
		Int32U5BU5D_t3030399641* L_176 = Mesh_get_triangles_m3988715512(L_175, /*hidden argument*/NULL);
		int32_t L_177 = V_5;
		NullCheck(L_176);
		int32_t L_178 = ((int32_t)((int32_t)L_177+(int32_t)2));
		int32_t L_179 = (L_176)->GetAt(static_cast<il2cpp_array_size_t>(L_178));
		NullCheck(L_174);
		float L_180 = ((L_174)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_179)))->get_y_1();
		(&V_13)->set_y_1(L_180);
		List_1_t1612828711 * L_181 = V_1;
		Vector2_t2243707579  L_182 = V_13;
		NullCheck(L_181);
		List_1_Add_m1515754733(L_181, L_182, /*hidden argument*/List_1_Add_m1515754733_MethodInfo_var);
		List_1_t1440998580 * L_183 = V_2;
		int32_t L_184 = V_5;
		int32_t L_185 = V_3;
		NullCheck(L_183);
		List_1_set_Item_m635251882(L_183, ((int32_t)((int32_t)L_184+(int32_t)2)), L_185, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
	}

IL_0331:
	{
		int32_t L_186 = V_5;
		V_5 = ((int32_t)((int32_t)L_186+(int32_t)3));
	}

IL_0338:
	{
		int32_t L_187 = V_5;
		Mesh_t1356156583 * L_188 = ___mesh0;
		NullCheck(L_188);
		Int32U5BU5D_t3030399641* L_189 = Mesh_get_triangles_m3988715512(L_188, /*hidden argument*/NULL);
		NullCheck(L_189);
		if ((((int32_t)L_187) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_189)->max_length)))))))
		{
			goto IL_003d;
		}
	}
	{
		Mesh_t1356156583 * L_190 = ___mesh0;
		NullCheck(L_190);
		Mesh_Clear_m231813403(L_190, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_191 = ___mesh0;
		List_1_t1612828712 * L_192 = V_0;
		NullCheck(L_192);
		Vector3U5BU5D_t1172311765* L_193 = List_1_ToArray_m2543904144(L_192, /*hidden argument*/List_1_ToArray_m2543904144_MethodInfo_var);
		NullCheck(L_191);
		Mesh_set_vertices_m2936804213(L_191, L_193, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_194 = ___mesh0;
		List_1_t1612828711 * L_195 = V_1;
		NullCheck(L_195);
		Vector2U5BU5D_t686124026* L_196 = List_1_ToArray_m4024240619(L_195, /*hidden argument*/List_1_ToArray_m4024240619_MethodInfo_var);
		NullCheck(L_194);
		Mesh_set_uv_m1497318906(L_194, L_196, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_197 = ___mesh0;
		List_1_t1440998580 * L_198 = V_2;
		NullCheck(L_198);
		Int32U5BU5D_t3030399641* L_199 = List_1_ToArray_m3453833174(L_198, /*hidden argument*/List_1_ToArray_m3453833174_MethodInfo_var);
		NullCheck(L_197);
		Mesh_set_triangles_m3244966865(L_197, L_199, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MyMove::.ctor()
extern "C"  void MyMove__ctor_m807702674 (MyMove_t2796795019 * __this, const MethodInfo* method)
{
	{
		__this->set_horizontalSpeed_2((2.0f));
		__this->set_verticalSpeed_3((2.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MyMove::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern const uint32_t MyMove_Update_m3933786771_MetadataUsageId;
extern "C"  void MyMove_Update_m3933786771 (MyMove_t2796795019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MyMove_Update_m3933786771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_horizontalSpeed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = __this->get_verticalSpeed_3();
		float L_3 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)L_3));
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_5 = V_1;
		float L_6 = V_0;
		NullCheck(L_4);
		Transform_Rotate_m4255273365(L_4, L_5, L_6, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCam::.ctor()
extern "C"  void TouchCam__ctor_m1345353997 (TouchCam_t4105149786 * __this, const MethodInfo* method)
{
	{
		__this->set_xAngle_4((0.0f));
		__this->set_yAngle_5((0.0f));
		__this->set_xAngTemp_6((0.0f));
		__this->set_yAngTemp_7((0.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCam::Start()
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t TouchCam_Start_m1317043845_MetadataUsageId;
extern "C"  void TouchCam_Start_m1317043845 (TouchCam_t4105149786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCam_Start_m1317043845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_firstpoint_2(L_0);
		Vector3_t2243707580  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m2638739322(&L_1, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_secondpoint_3(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		__this->set_playRot_8(L_2);
		__this->set_xAngle_4((0.0f));
		__this->set_yAngle_5((0.0f));
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_yAngle_5();
		float L_5 = __this->get_xAngle_4();
		Quaternion_t4030073918  L_6 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_4, L_5, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m3411284563(L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCam::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t TouchCam_Update_m1459278428_MetadataUsageId;
extern "C"  void TouchCam_Update_m1459278428 (TouchCam_t4105149786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCam_Update_m1459278428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t407273883  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_01e4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_1 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_3 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector2_t2243707579  L_4 = Touch_get_position_m2079703643((&V_1), /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_firstpoint_2(L_5);
		float L_6 = __this->get_xAngle_4();
		__this->set_xAngTemp_6(L_6);
		float L_7 = __this->get_yAngle_5();
		__this->set_yAngTemp_7(L_7);
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_8 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = Touch_get_phase_m196706494((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_01e3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_10 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_10;
		Vector2_t2243707579  L_11 = Touch_get_position_m2079703643((&V_3), /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->set_secondpoint_3(L_12);
		float L_13 = __this->get_yAngTemp_7();
		Vector3_t2243707580 * L_14 = __this->get_address_of_secondpoint_3();
		float L_15 = L_14->get_y_2();
		Vector3_t2243707580 * L_16 = __this->get_address_of_firstpoint_2();
		float L_17 = L_16->get_y_2();
		int32_t L_18 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_yAngle_5(((float)((float)L_13-(float)((float)((float)((float)((float)((float)((float)L_15-(float)L_17))*(float)(90.0f)))/(float)(((float)((float)L_18))))))));
		float L_19 = __this->get_yAngle_5();
		if ((!(((float)L_19) < ((float)(0.0f)))))
		{
			goto IL_00d4;
		}
	}
	{
		float L_20 = __this->get_yAngle_5();
		__this->set_yAngle_5(((float)((float)L_20+(float)(360.0f))));
	}

IL_00d4:
	{
		float L_21 = __this->get_yAngle_5();
		if ((!(((float)L_21) > ((float)(360.0f)))))
		{
			goto IL_00f6;
		}
	}
	{
		float L_22 = __this->get_yAngle_5();
		__this->set_yAngle_5(((float)((float)L_22-(float)(360.0f))));
	}

IL_00f6:
	{
		float L_23 = __this->get_yAngle_5();
		if ((!(((float)L_23) > ((float)(90.0f)))))
		{
			goto IL_014c;
		}
	}
	{
		float L_24 = __this->get_yAngle_5();
		if ((!(((float)L_24) < ((float)(270.0f)))))
		{
			goto IL_014c;
		}
	}
	{
		float L_25 = __this->get_xAngTemp_6();
		Vector3_t2243707580 * L_26 = __this->get_address_of_secondpoint_3();
		float L_27 = L_26->get_x_1();
		Vector3_t2243707580 * L_28 = __this->get_address_of_firstpoint_2();
		float L_29 = L_28->get_x_1();
		int32_t L_30 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_xAngle_4(((float)((float)L_25-(float)((float)((float)((float)((float)((float)((float)L_27-(float)L_29))*(float)(180.0f)))/(float)(((float)((float)L_30))))))));
		goto IL_017d;
	}

IL_014c:
	{
		float L_31 = __this->get_xAngTemp_6();
		Vector3_t2243707580 * L_32 = __this->get_address_of_secondpoint_3();
		float L_33 = L_32->get_x_1();
		Vector3_t2243707580 * L_34 = __this->get_address_of_firstpoint_2();
		float L_35 = L_34->get_x_1();
		int32_t L_36 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_xAngle_4(((float)((float)L_31+(float)((float)((float)((float)((float)((float)((float)L_33-(float)L_35))*(float)(180.0f)))/(float)(((float)((float)L_36))))))));
	}

IL_017d:
	{
		float L_37 = __this->get_xAngle_4();
		if ((!(((float)L_37) < ((float)(0.0f)))))
		{
			goto IL_019f;
		}
	}
	{
		float L_38 = __this->get_xAngle_4();
		__this->set_xAngle_4(((float)((float)L_38+(float)(360.0f))));
	}

IL_019f:
	{
		float L_39 = __this->get_xAngle_4();
		if ((!(((float)L_39) > ((float)(360.0f)))))
		{
			goto IL_01c1;
		}
	}
	{
		float L_40 = __this->get_xAngle_4();
		__this->set_xAngle_4(((float)((float)L_40-(float)(360.0f))));
	}

IL_01c1:
	{
		Transform_t3275118058 * L_41 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_42 = __this->get_yAngle_5();
		float L_43 = __this->get_xAngle_4();
		Quaternion_t4030073918  L_44 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_42, L_43, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_rotation_m3411284563(L_41, L_44, /*hidden argument*/NULL);
	}

IL_01e3:
	{
	}

IL_01e4:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
