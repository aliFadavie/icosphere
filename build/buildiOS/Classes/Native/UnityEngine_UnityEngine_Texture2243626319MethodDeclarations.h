﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m4198984292 (Texture_t2243626319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_width(System.Int32)
extern "C"  void Texture_set_width_m3075240330 (Texture_t2243626319 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_height(System.Int32)
extern "C"  void Texture_set_height_m1406712949 (Texture_t2243626319 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
