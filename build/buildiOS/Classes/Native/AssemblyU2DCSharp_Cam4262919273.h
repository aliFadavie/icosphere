﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cam
struct  Cam_t4262919273  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Cam::resWidth
	int32_t ___resWidth_2;
	// System.Int32 Cam::resHeight
	int32_t ___resHeight_3;
	// System.Boolean Cam::takeHiResShot
	bool ___takeHiResShot_4;
	// System.Boolean Cam::record
	bool ___record_5;

public:
	inline static int32_t get_offset_of_resWidth_2() { return static_cast<int32_t>(offsetof(Cam_t4262919273, ___resWidth_2)); }
	inline int32_t get_resWidth_2() const { return ___resWidth_2; }
	inline int32_t* get_address_of_resWidth_2() { return &___resWidth_2; }
	inline void set_resWidth_2(int32_t value)
	{
		___resWidth_2 = value;
	}

	inline static int32_t get_offset_of_resHeight_3() { return static_cast<int32_t>(offsetof(Cam_t4262919273, ___resHeight_3)); }
	inline int32_t get_resHeight_3() const { return ___resHeight_3; }
	inline int32_t* get_address_of_resHeight_3() { return &___resHeight_3; }
	inline void set_resHeight_3(int32_t value)
	{
		___resHeight_3 = value;
	}

	inline static int32_t get_offset_of_takeHiResShot_4() { return static_cast<int32_t>(offsetof(Cam_t4262919273, ___takeHiResShot_4)); }
	inline bool get_takeHiResShot_4() const { return ___takeHiResShot_4; }
	inline bool* get_address_of_takeHiResShot_4() { return &___takeHiResShot_4; }
	inline void set_takeHiResShot_4(bool value)
	{
		___takeHiResShot_4 = value;
	}

	inline static int32_t get_offset_of_record_5() { return static_cast<int32_t>(offsetof(Cam_t4262919273, ___record_5)); }
	inline bool get_record_5() const { return ___record_5; }
	inline bool* get_address_of_record_5() { return &___record_5; }
	inline void set_record_5(bool value)
	{
		___record_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
