﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// UnityEngine.AnimationEvent
struct AnimationEvent_t2428323300;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.AnimationState
struct AnimationState_t1303741697;
// UnityEngine.Application/LogCallback
struct LogCallback_t1867914413;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t1557026495;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1038783543;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2674559435;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t2717914595;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3007145346;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t421863554;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t3743753033;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// UnityEngine.BitStream
struct BitStream_t1979465639;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Component[]
struct ComponentU5BU5D_t4136971630;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3188497603;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.CullingGroup
struct CullingGroup_t1091689465;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t2480912210;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// System.Exception
struct Exception_t1927440687;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t865810509;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Display
struct Display_t3666191348;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t3423469815;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t4810721;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t3793436469;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t828812576;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.Director.Playable>
struct IEnumerable_1_t3959672593;
// UnityEngine.Experimental.Director.CustomAnimationPlayable
struct CustomAnimationPlayable_t3423099547;
// UnityEngine.Experimental.Director.Playable[]
struct PlayableU5BU5D_t4034110853;
// UnityEngine.Experimental.Director.ScriptPlayable
struct ScriptPlayable_t4067966717;
// System.Array
struct Il2CppArray;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// UnityEngine.GUILayer
struct GUILayer_t3254902478;
// UnityEngine.GUIElement
struct GUIElement_t3381083099;
// UnityEngine.HostData
struct HostData_t3480691970;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t1027170048;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t665825653;
// UnityEngine.iOS.LocalNotification
struct LocalNotification_t317971878;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.iOS.RemoteNotification
struct RemoteNotification_t2254252895;
// UnityEngine.Logger
struct Logger_t3328995178;
// UnityEngine.ILogHandler
struct ILogHandler_t264057413;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.NetworkView
struct NetworkView_t172525251;
// UnityEngine.PreferBinarySerialization
struct PreferBinarySerialization_t2472773525;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t3033456180;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_U3CModuleU3E3783534214.h"
#include "UnityEngine_U3CModuleU3E3783534214MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_System_Collections_Generic_Stack_1_gen2391531380MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2391531380.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672924358MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672924358.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder2717914595MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder2717914595.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BitStream1979465639.h"
#include "UnityEngine_UnityEngine_BitStream1979465639MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Int164041245914.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace627621177.h"
#include "UnityEngine_UnityEngine_ColorSpace627621177MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3188497603.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263.h"
#include "mscorlib_System_Exception1927440687.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Logger3328995178MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display3666191348.h"
#include "UnityEngine_UnityEngine_Display3666191348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721MethodDeclarations.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1598685972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1598685972.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3162557601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3162557601.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play2968292729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Cust3423099547.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Cust3423099547MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri4067966717MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3250302433.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2947922465.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2947922465MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Gener788733994.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Gener788733994MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3036666680MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3036666680.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play2968292729.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3250302433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri4067966717.h"
#include "UnityEngine_UnityEngine_Gradient3600583008.h"
#include "UnityEngine_UnityEngine_Gradient3600583008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gyroscope1705362817.h"
#include "UnityEngine_UnityEngine_Gyroscope1705362817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HostData3480691970.h"
#include "UnityEngine_UnityEngine_HostData3480691970MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanLimit250797648.h"
#include "UnityEngine_UnityEngine_HumanLimit250797648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier259698391.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier259698391MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit4134400622.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit4134400622MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification317971878.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification317971878MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification2254252895.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification2254252895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType1559732862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView172525251.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView172525251MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString276356480MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2469606224MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"

// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  Il2CppObject * AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method);
#define AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473(__this /* static, unused */, ___klass0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_gshared)(__this /* static, unused */, ___klass0, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<UnityEngine.DefaultExecutionOrder>(System.Type)
#define AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2717914595_m3755678983(__this /* static, unused */, ___klass0, method) ((  DefaultExecutionOrder_t2717914595 * (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_gshared)(__this /* static, unused */, ___klass0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537(__this, ___channel0, method) ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared)(__this, ___channel0, method)
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector3_t2243707580_m3518652704_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, Vector3U5BU5D_t1172311765* ___values1, const MethodInfo* method);
#define Mesh_SetArrayForChannel_TisVector3_t2243707580_m3518652704(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t1356156583 *, int32_t, Vector3U5BU5D_t1172311765*, const MethodInfo*))Mesh_SetArrayForChannel_TisVector3_t2243707580_m3518652704_gshared)(__this, ___channel0, ___values1, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716(__this, ___channel0, method) ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared)(__this, ___channel0, method)
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector2_t2243707579_m1051036475_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, Vector2U5BU5D_t686124026* ___values1, const MethodInfo* method);
#define Mesh_SetArrayForChannel_TisVector2_t2243707579_m1051036475(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t1356156583 *, int32_t, Vector2U5BU5D_t686124026*, const MethodInfo*))Mesh_SetArrayForChannel_TisVector2_t2243707579_m1051036475_gshared)(__this, ___channel0, ___values1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2814448007 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* ___keys0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t449065829* L_0 = ___keys0;
		AnimationCurve_Init_m1486386337(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m3707994114 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m1486386337(__this, (KeyframeU5BU5D_t449065829*)(KeyframeU5BU5D_t449065829*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m2190142678 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m2190142678_ftn) (AnimationCurve_t3306541151 *);
	static AnimationCurve_Cleanup_m2190142678_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m2190142678_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m3393741894 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m2190142678(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m1486386337 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m1486386337_ftn) (AnimationCurve_t3306541151 *, KeyframeU5BU5D_t449065829*);
	static AnimationCurve_Init_m1486386337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m1486386337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(const AnimationCurve_t3306541151_marshaled_pinvoke& marshaled, AnimationCurve_t3306541151& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(AnimationCurve_t3306541151_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_com(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3306541151_marshal_com_back(const AnimationCurve_t3306541151_marshaled_com& marshaled, AnimationCurve_t3306541151& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_com_cleanup(AnimationCurve_t3306541151_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t AnimationEvent__ctor_m3458990599_MetadataUsageId;
extern "C"  void AnimationEvent__ctor_m3458990599 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent__ctor_m3458990599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_m_Time_0((0.0f));
		__this->set_m_FunctionName_1(_stringLiteral371857150);
		__this->set_m_StringParameter_2(_stringLiteral371857150);
		__this->set_m_ObjectReferenceParameter_3((Object_t1021602117 *)NULL);
		__this->set_m_FloatParameter_4((0.0f));
		__this->set_m_IntParameter_5(0);
		__this->set_m_MessageOptions_6(0);
		__this->set_m_Source_7(0);
		__this->set_m_StateSender_8((AnimationState_t1303741697 *)NULL);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C"  String_t* AnimationEvent_get_data_m1944226119 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C"  void AnimationEvent_set_data_m2305843164 (AnimationEvent_t2428323300 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C"  String_t* AnimationEvent_get_stringParameter_m3994883119 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C"  void AnimationEvent_set_stringParameter_m3956270534 (AnimationEvent_t2428323300 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C"  float AnimationEvent_get_floatParameter_m2695208933 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatParameter_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C"  void AnimationEvent_set_floatParameter_m4139544688 (AnimationEvent_t2428323300 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_FloatParameter_4(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C"  int32_t AnimationEvent_get_intParameter_m903783586 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntParameter_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C"  void AnimationEvent_set_intParameter_m1174062029 (AnimationEvent_t2428323300 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_IntParameter_5(L_0);
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C"  Object_t1021602117 * AnimationEvent_get_objectReferenceParameter_m3565420672 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = __this->get_m_ObjectReferenceParameter_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t1021602117 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C"  void AnimationEvent_set_objectReferenceParameter_m3171652515 (AnimationEvent_t2428323300 * __this, Object_t1021602117 * ___value0, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = ___value0;
		__this->set_m_ObjectReferenceParameter_3(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C"  String_t* AnimationEvent_get_functionName_m4178006856 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_FunctionName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C"  void AnimationEvent_set_functionName_m1910707421 (AnimationEvent_t2428323300 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_FunctionName_1(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C"  float AnimationEvent_get_time_m2837507241 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Time_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C"  void AnimationEvent_set_time_m2162176572 (AnimationEvent_t2428323300 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C"  int32_t AnimationEvent_get_messageOptions_m3547411650 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_MessageOptions_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C"  void AnimationEvent_set_messageOptions_m3825202925 (AnimationEvent_t2428323300 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_MessageOptions_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C"  bool AnimationEvent_get_isFiredByLegacy_m2585487710 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Source_7();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C"  bool AnimationEvent_get_isFiredByAnimator_m3399078288 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Source_7();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2898685537;
extern const uint32_t AnimationEvent_get_animationState_m1069191380_MetadataUsageId;
extern "C"  AnimationState_t1303741697 * AnimationEvent_get_animationState_m1069191380 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animationState_m1069191380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimationState_t1303741697 * V_0 = NULL;
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m2585487710(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2898685537, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AnimationState_t1303741697 * L_1 = __this->get_m_StateSender_8();
		V_0 = L_1;
		goto IL_0022;
	}

IL_0022:
	{
		AnimationState_t1303741697 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral619768799;
extern const uint32_t AnimationEvent_get_animatorStateInfo_m4255437518_MetadataUsageId;
extern "C"  AnimatorStateInfo_t2577870592  AnimationEvent_get_animatorStateInfo_m4255437518 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorStateInfo_m4255437518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_t2577870592  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m3399078288(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral619768799, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AnimatorStateInfo_t2577870592  L_1 = __this->get_m_AnimatorStateInfo_9();
		V_0 = L_1;
		goto IL_0022;
	}

IL_0022:
	{
		AnimatorStateInfo_t2577870592  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1382485084;
extern const uint32_t AnimationEvent_get_animatorClipInfo_m615328956_MetadataUsageId;
extern "C"  AnimatorClipInfo_t3905751349  AnimationEvent_get_animatorClipInfo_m615328956 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorClipInfo_m615328956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorClipInfo_t3905751349  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m3399078288(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1382485084, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AnimatorClipInfo_t3905751349  L_1 = __this->get_m_AnimatorClipInfo_10();
		V_0 = L_1;
		goto IL_0022;
	}

IL_0022:
	{
		AnimatorClipInfo_t3905751349  L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C"  int32_t AnimationEvent_GetHash_m2850593057 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m4178006856(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m2837507241(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m3102305584((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		V_2 = L_5;
		goto IL_0031;
	}

IL_0031:
	{
		int32_t L_6 = V_2;
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke(const AnimationEvent_t2428323300& unmarshaled, AnimationEvent_t2428323300_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_back(const AnimationEvent_t2428323300_marshaled_pinvoke& marshaled, AnimationEvent_t2428323300& unmarshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_cleanup(AnimationEvent_t2428323300_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_com(const AnimationEvent_t2428323300& unmarshaled, AnimationEvent_t2428323300_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
extern "C" void AnimationEvent_t2428323300_marshal_com_back(const AnimationEvent_t2428323300_marshaled_com& marshaled, AnimationEvent_t2428323300& unmarshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_com_cleanup(AnimationEvent_t2428323300_marshaled_com& marshaled)
{
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m3313850714 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m3313850714_ftn) (String_t*);
	static Animator_StringToHash_m3313850714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m3313850714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m4069203550 (AnimatorStateInfo_t2577870592 * __this, String_t* ___name0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_m_FullPath_2();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_m_Name_0();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_m_Path_1();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		V_1 = (bool)G_B4_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
extern "C"  bool AnimatorStateInfo_IsName_m4069203550_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_IsName_m4069203550(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m3941998936 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FullPath_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m3941998936_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_fullPathHash_m3941998936(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m1703033713 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Path_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m1703033713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_nameHash_m1703033713(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m3915898263 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Name_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m3915898263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_shortNameHash_m3915898263(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C"  float AnimatorStateInfo_get_normalizedTime_m1330221276 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_normalizedTime_m1330221276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_normalizedTime_m1330221276(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C"  float AnimatorStateInfo_get_length_m3151009408 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Length_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_length_m3151009408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_length_m3151009408(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C"  float AnimatorStateInfo_get_speed_m3265333371 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Speed_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_speed_m3265333371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_speed_m3265333371(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m3451382250 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_SpeedMultiplier_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m3451382250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_speedMultiplier_m3451382250(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m1559842952 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Tag_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m1559842952_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_tagHash_m1559842952(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C"  bool AnimatorStateInfo_IsTag_m1529621725 (AnimatorStateInfo_t2577870592 * __this, String_t* ___tag0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___tag0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Tag_7();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorStateInfo_IsTag_m1529621725_AdjustorThunk (Il2CppObject * __this, String_t* ___tag0, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_IsTag_m1529621725(_thisAdjusted, ___tag0, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C"  bool AnimatorStateInfo_get_loop_m765573376 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Loop_8();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorStateInfo_get_loop_m765573376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_loop_m765573376(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsName_m2561957738 (AnimatorTransitionInfo_t2410896200 * __this, String_t* ___name0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Name_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___name0;
		int32_t L_4 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_FullPath_0();
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 1;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsName_m2561957738_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_IsName_m2561957738(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsUserName_m3771933881 (AnimatorTransitionInfo_t2410896200 * __this, String_t* ___name0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_UserName_1();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsUserName_m3771933881_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_IsUserName_m3771933881(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2689544352 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FullPath_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2689544352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_fullPathHash_m2689544352(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m460043277 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Name_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m460043277_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_nameHash_m460043277(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m971244190 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_UserName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m971244190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_userNameHash_m971244190(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m2741999844 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m2741999844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_normalizedTime_m2741999844(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C"  bool AnimatorTransitionInfo_get_anyState_m3474272231 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_AnyState_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_anyState_m3474272231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_anyState_m3474272231(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C"  bool AnimatorTransitionInfo_get_entry_m1140118520 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		V_0 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_entry_m1140118520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_entry_m1140118520(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C"  bool AnimatorTransitionInfo_get_exit_m3018502934 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		V_0 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_exit_m3018502934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_exit_m3018502934(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke(const AnimatorTransitionInfo_t2410896200& unmarshaled, AnimatorTransitionInfo_t2410896200_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = static_cast<int32_t>(unmarshaled.get_m_AnyState_4());
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back(const AnimatorTransitionInfo_t2410896200_marshaled_pinvoke& marshaled, AnimatorTransitionInfo_t2410896200& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = static_cast<bool>(marshaled.___m_AnyState_4);
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup(AnimatorTransitionInfo_t2410896200_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_com(const AnimatorTransitionInfo_t2410896200& unmarshaled, AnimatorTransitionInfo_t2410896200_marshaled_com& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = static_cast<int32_t>(unmarshaled.get_m_AnyState_4());
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_com_back(const AnimatorTransitionInfo_t2410896200_marshaled_com& marshaled, AnimatorTransitionInfo_t2410896200& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = static_cast<bool>(marshaled.___m_AnyState_4);
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_com_cleanup(AnimatorTransitionInfo_t2410896200_marshaled_com& marshaled)
{
}
// System.String UnityEngine.Application::get_dataPath()
extern "C"  String_t* Application_get_dataPath_m371940330 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_dataPath_m371940330_ftn) ();
	static Application_get_dataPath_m371940330_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_dataPath_m371940330_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_dataPath()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern Il2CppClass* Application_t354826772_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLogCallback_m3408386792_MetadataUsageId;
extern "C"  void Application_CallLogCallback_m3408386792 (Il2CppObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m3408386792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LogCallback_t1867914413 * V_0 = NULL;
	LogCallback_t1867914413 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		LogCallback_t1867914413 * L_1 = ((Application_t354826772_StaticFields*)Application_t354826772_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandler_0();
		V_0 = L_1;
		LogCallback_t1867914413 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		LogCallback_t1867914413 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m3921378796(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001d:
	{
	}

IL_001e:
	{
		LogCallback_t1867914413 * L_7 = ((Application_t354826772_StaticFields*)Application_t354826772_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandlerThreaded_1();
		V_1 = L_7;
		LogCallback_t1867914413 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		LogCallback_t1867914413 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m3921378796(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m898185969 (LogCallback_t1867914413 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m3921378796 (LogCallback_t1867914413 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m3921378796((LogCallback_t1867914413 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t1867914413 (LogCallback_t1867914413 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern Il2CppClass* LogType_t1559732862_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m2485641857_MetadataUsageId;
extern "C"  Il2CppObject * LogCallback_BeginInvoke_m2485641857 (LogCallback_t1867914413 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m2485641857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t1559732862_il2cpp_TypeInfo_var, &___type2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m1910375855 (LogCallback_t1867914413 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C"  void AssemblyIsEditorAssembly__ctor_m2639706522 (AssemblyIsEditorAssembly_t1557026495 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C"  void AssetBundleCreateRequest__ctor_m2967025878 (AssetBundleCreateRequest_t1038783543 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C"  AssetBundle_t2054978754 * AssetBundleCreateRequest_get_assetBundle_m2931245245 (AssetBundleCreateRequest_t1038783543 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t2054978754 * (*AssetBundleCreateRequest_get_assetBundle_m2931245245_ftn) (AssetBundleCreateRequest_t1038783543 *);
	static AssetBundleCreateRequest_get_assetBundle_m2931245245_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m2931245245_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C"  void AssetBundleCreateRequest_DisableCompatibilityChecks_m3752941663 (AssetBundleCreateRequest_t1038783543 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m3752941663_ftn) (AssetBundleCreateRequest_t1038783543 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m3752941663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m3752941663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C"  void AssetBundleRequest__ctor_m2870261062 (AssetBundleRequest_t2674559435 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C"  Object_t1021602117 * AssetBundleRequest_get_asset_m624603186 (AssetBundleRequest_t2674559435 * __this, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*AssetBundleRequest_get_asset_m624603186_ftn) (AssetBundleRequest_t2674559435 *);
	static AssetBundleRequest_get_asset_m624603186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_asset_m624603186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_asset()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C"  ObjectU5BU5D_t4217747464* AssetBundleRequest_get_allAssets_m3314852268 (AssetBundleRequest_t2674559435 * __this, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*AssetBundleRequest_get_allAssets_m3314852268_ftn) (AssetBundleRequest_t2674559435 *);
	static AssetBundleRequest_get_allAssets_m3314852268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_allAssets_m3314852268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_allAssets()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke(const AssetBundleRequest_t2674559435& unmarshaled, AssetBundleRequest_t2674559435_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_back(const AssetBundleRequest_t2674559435_marshaled_pinvoke& marshaled, AssetBundleRequest_t2674559435& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup(AssetBundleRequest_t2674559435_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_com(const AssetBundleRequest_t2674559435& unmarshaled, AssetBundleRequest_t2674559435_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t2674559435_marshal_com_back(const AssetBundleRequest_t2674559435_marshaled_com& marshaled, AssetBundleRequest_t2674559435& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_com_cleanup(AssetBundleRequest_t2674559435_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2914860946 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m3312061823 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m3312061823_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_InternalDestroy_m3312061823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m3312061823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m67522446 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m3312061823(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern "C"  bool AsyncOperation_get_isDone_m1085614149 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_isDone_m1085614149_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_isDone_m1085614149_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_isDone_m1085614149_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.AsyncOperation::get_progress()
extern "C"  float AsyncOperation_get_progress_m478775228 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef float (*AsyncOperation_get_progress_m478775228_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_progress_m478775228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_progress_m478775228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_progress()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AsyncOperation::get_priority()
extern "C"  int32_t AsyncOperation_get_priority_m966065939 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef int32_t (*AsyncOperation_get_priority_m966065939_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_priority_m966065939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_priority_m966065939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_priority()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
extern "C"  void AsyncOperation_set_priority_m3581020444 (AsyncOperation_t3814632279 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_priority_m3581020444_ftn) (AsyncOperation_t3814632279 *, int32_t);
	static AsyncOperation_set_priority_m3581020444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_priority_m3581020444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_priority(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
extern "C"  bool AsyncOperation_get_allowSceneActivation_m2825451182 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_allowSceneActivation_m2825451182_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_allowSceneActivation_m2825451182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_allowSceneActivation_m2825451182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_allowSceneActivation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
extern "C"  void AsyncOperation_set_allowSceneActivation_m3988498951 (AsyncOperation_t3814632279 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_allowSceneActivation_m3988498951_ftn) (AsyncOperation_t3814632279 *, bool);
	static AsyncOperation_set_allowSceneActivation_m3988498951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_allowSceneActivation_m3988498951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke(const AsyncOperation_t3814632279& unmarshaled, AsyncOperation_t3814632279_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_back(const AsyncOperation_t3814632279_marshaled_pinvoke& marshaled, AsyncOperation_t3814632279& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_cleanup(AsyncOperation_t3814632279_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_com(const AsyncOperation_t3814632279& unmarshaled, AsyncOperation_t3814632279_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t3814632279_marshal_com_back(const AsyncOperation_t3814632279_marshaled_com& marshaled, AsyncOperation_t3814632279& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_com_cleanup(AsyncOperation_t3814632279_marshaled_com& marshaled)
{
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t1158329972_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t2656950_0_0_0_var;
extern Il2CppClass* Stack_1_t2391531380_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m1240888107_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m1219431354_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m685736912_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m1292854535_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m685343645_MetadataUsageId;
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m685343645 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m685343645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t2391531380 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	{
		Stack_1_t2391531380 * L_0 = (Stack_1_t2391531380 *)il2cpp_codegen_object_new(Stack_1_t2391531380_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1240888107(L_0, /*hidden argument*/Stack_1__ctor_m1240888107_MethodInfo_var);
		V_0 = L_0;
		goto IL_001d;
	}

IL_000c:
	{
		Stack_1_t2391531380 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m1219431354(L_1, L_2, /*hidden argument*/Stack_1_Push_m1219431354_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001d:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_000c;
		}
	}

IL_0033:
	{
		V_1 = (Type_t *)NULL;
		goto IL_0067;
	}

IL_003a:
	{
		Stack_1_t2391531380 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m685736912(L_8, /*hidden argument*/Stack_1_Pop_m685736912_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t2656950_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t3614634134* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t3614634134* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_15 = V_1;
		V_4 = L_15;
		goto IL_007b;
	}

IL_0066:
	{
	}

IL_0067:
	{
		Stack_1_t2391531380 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m1292854535(L_16, /*hidden argument*/Stack_1_get_Count_m1292854535_MethodInfo_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		V_4 = (Type_t *)NULL;
		goto IL_007b;
	}

IL_007b:
	{
		Type_t * L_18 = V_4;
		return L_18;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t864575032_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t1158329972_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t672924358_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4047179467_MethodInfo_var;
extern const MethodInfo* List_1_Add_m176071399_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1070939693_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m120894667_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* AttributeHelperEngine_GetRequiredComponents_m120894667 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m120894667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t672924358 * V_0 = NULL;
	RequireComponentU5BU5D_t2214070761* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t864575032 * V_3 = NULL;
	RequireComponentU5BU5D_t2214070761* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t1664964607* V_6 = NULL;
	TypeU5BU5D_t1664964607* V_7 = NULL;
	{
		V_0 = (List_1_t672924358 *)NULL;
		goto IL_00ef;
	}

IL_0008:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t864575032_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t3614634134* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t2214070761*)Castclass(L_2, RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t2214070761* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00e0;
	}

IL_0033:
	{
		RequireComponentU5BU5D_t2214070761* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RequireComponent_t864575032 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t672924358 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		RequireComponentU5BU5D_t2214070761* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13))))
		{
			goto IL_0086;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_14 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t864575032 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1664964607* L_17 = L_14;
		RequireComponent_t864575032 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t1664964607* L_20 = L_17;
		RequireComponent_t864575032 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t1664964607* L_23 = V_6;
		V_7 = L_23;
		goto IL_0120;
	}

IL_0086:
	{
		List_1_t672924358 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t672924358 * L_25 = (List_1_t672924358 *)il2cpp_codegen_object_new(List_1_t672924358_il2cpp_TypeInfo_var);
		List_1__ctor_m4047179467(L_25, /*hidden argument*/List_1__ctor_m4047179467_MethodInfo_var);
		V_0 = L_25;
	}

IL_0093:
	{
		RequireComponent_t864575032 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t672924358 * L_28 = V_0;
		RequireComponent_t864575032 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m176071399(L_28, L_30, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
	}

IL_00aa:
	{
		RequireComponent_t864575032 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		List_1_t672924358 * L_33 = V_0;
		RequireComponent_t864575032 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m176071399(L_33, L_35, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
	}

IL_00c1:
	{
		RequireComponent_t864575032 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		List_1_t672924358 * L_38 = V_0;
		RequireComponent_t864575032 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m176071399(L_38, L_40, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
	}

IL_00d8:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t2214070761* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00ef:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_46) == ((Il2CppObject*)(Type_t *)L_47))))
		{
			goto IL_0008;
		}
	}

IL_0105:
	{
		List_1_t672924358 * L_48 = V_0;
		if (L_48)
		{
			goto IL_0113;
		}
	}
	{
		V_7 = (TypeU5BU5D_t1664964607*)NULL;
		goto IL_0120;
	}

IL_0113:
	{
		List_1_t672924358 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t1664964607* L_50 = List_1_ToArray_m1070939693(L_49, /*hidden argument*/List_1_ToArray_m1070939693_MethodInfo_var);
		V_7 = L_50;
		goto IL_0120;
	}

IL_0120:
	{
		TypeU5BU5D_t1664964607* L_51 = V_7;
		return L_51;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t3043633143_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t1158329972_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m2980171478_MetadataUsageId;
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m2980171478 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m2980171478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		goto IL_0033;
	}

IL_0006:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t3043633143_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t3614634134* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0050;
	}

IL_002a:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_0033:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0006;
		}
	}

IL_0049:
	{
		V_2 = (bool)0;
		goto IL_0050;
	}

IL_0050:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern Il2CppClass* AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var;
extern const MethodInfo* AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2717914595_m3755678983_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m451063166_MetadataUsageId;
extern "C"  int32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m451063166 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetDefaultExecutionOrderFor_m451063166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultExecutionOrder_t2717914595 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var);
		DefaultExecutionOrder_t2717914595 * L_1 = AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2717914595_m3755678983(NULL /*static, unused*/, L_0, /*hidden argument*/AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t2717914595_m3755678983_MethodInfo_var);
		V_0 = L_1;
		DefaultExecutionOrder_t2717914595 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_0015:
	{
		DefaultExecutionOrder_t2717914595 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = DefaultExecutionOrder_get_order_m1561221759(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern Il2CppClass* DisallowMultipleComponentU5BU5D_t674354611_il2cpp_TypeInfo_var;
extern Il2CppClass* AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteInEditModeU5BU5D_t1783660110_il2cpp_TypeInfo_var;
extern Il2CppClass* RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m1775592582_MetadataUsageId;
extern "C"  void AttributeHelperEngine__cctor_m1775592582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m1775592582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AttributeHelperEngine_t958797062_StaticFields*)AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var->static_fields)->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t674354611*)SZArrayNew(DisallowMultipleComponentU5BU5D_t674354611_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t958797062_StaticFields*)AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var->static_fields)->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t1783660110*)SZArrayNew(ExecuteInEditModeU5BU5D_t1783660110_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t958797062_StaticFields*)AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var->static_fields)->set__requireComponentArray_2(((RequireComponentU5BU5D_t2214070761*)SZArrayNew(RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C"  void AudioClip_InvokePCMReaderCallback_Internal_m1966286598 (AudioClip_t1932558630 * __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method)
{
	{
		PCMReaderCallback_t3007145346 * L_0 = __this->get_m_PCMReaderCallback_2();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PCMReaderCallback_t3007145346 * L_1 = __this->get_m_PCMReaderCallback_2();
		SingleU5BU5D_t577127397* L_2 = ___data0;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m3610389815(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C"  void AudioClip_InvokePCMSetPositionCallback_Internal_m2304858844 (AudioClip_t1932558630 * __this, int32_t ___position0, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t421863554 * L_0 = __this->get_m_PCMSetPositionCallback_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PCMSetPositionCallback_t421863554 * L_1 = __this->get_m_PCMSetPositionCallback_3();
		int32_t L_2 = ___position0;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m651987035(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMReaderCallback__ctor_m4217492708 (PCMReaderCallback_t3007145346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m3610389815 (PCMReaderCallback_t3007145346 * __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMReaderCallback_Invoke_m3610389815((PCMReaderCallback_t3007145346 *)__this->get_prev_9(),___data0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 (PCMReaderCallback_t3007145346 * __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(float*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___data0' to native representation
	float* ____data0_marshaled = NULL;
	if (___data0 != NULL)
	{
		____data0_marshaled = reinterpret_cast<float*>((___data0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(____data0_marshaled);

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PCMReaderCallback_BeginInvoke_m3119095492 (PCMReaderCallback_t3007145346 * __this, SingleU5BU5D_t577127397* ___data0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMReaderCallback_EndInvoke_m909664362 (PCMReaderCallback_t3007145346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMSetPositionCallback__ctor_m232778710 (PCMSetPositionCallback_t421863554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m651987035 (PCMSetPositionCallback_t421863554 * __this, int32_t ___position0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMSetPositionCallback_Invoke_m651987035((PCMSetPositionCallback_t421863554 *)__this->get_prev_9(),___position0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 (PCMSetPositionCallback_t421863554 * __this, int32_t ___position0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___position0);

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t PCMSetPositionCallback_BeginInvoke_m2507142524_MetadataUsageId;
extern "C"  Il2CppObject * PCMSetPositionCallback_BeginInvoke_m2507142524 (PCMSetPositionCallback_t421863554 * __this, int32_t ___position0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PCMSetPositionCallback_BeginInvoke_m2507142524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___position0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMSetPositionCallback_EndInvoke_m4290184144 (PCMSetPositionCallback_t421863554 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern Il2CppClass* AudioSettings_t3144015719_il2cpp_TypeInfo_var;
extern const uint32_t AudioSettings_InvokeOnAudioConfigurationChanged_m3225073778_MetadataUsageId;
extern "C"  void AudioSettings_InvokeOnAudioConfigurationChanged_m3225073778 (Il2CppObject * __this /* static, unused */, bool ___deviceWasChanged0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioConfigurationChanged_m3225073778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioConfigurationChangeHandler_t3743753033 * L_0 = ((AudioSettings_t3144015719_StaticFields*)AudioSettings_t3144015719_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioConfigurationChangeHandler_t3743753033 * L_1 = ((AudioSettings_t3144015719_StaticFields*)AudioSettings_t3144015719_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		bool L_2 = ___deviceWasChanged0;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m635374412(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AudioConfigurationChangeHandler__ctor_m114228029 (AudioConfigurationChangeHandler_t3743753033 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m635374412 (AudioConfigurationChangeHandler_t3743753033 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m635374412((AudioConfigurationChangeHandler_t3743753033 *)__this->get_prev_9(),___deviceWasChanged0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 (AudioConfigurationChangeHandler_t3743753033 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___deviceWasChanged0));

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t AudioConfigurationChangeHandler_BeginInvoke_m2418670597_MetadataUsageId;
extern "C"  Il2CppObject * AudioConfigurationChangeHandler_BeginInvoke_m2418670597 (AudioConfigurationChangeHandler_t3743753033 * __this, bool ___deviceWasChanged0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioConfigurationChangeHandler_BeginInvoke_m2418670597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___deviceWasChanged0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AudioConfigurationChangeHandler_EndInvoke_m2237100787 (AudioConfigurationChangeHandler_t3743753033 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2699265412 (Behaviour_t955675639 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m205306948(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::.ctor()
extern "C"  void BitStream__ctor_m3947899936 (BitStream_t1979465639 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serializeb(System.Int32&)
extern "C"  void BitStream_Serializeb_m2728727597 (BitStream_t1979465639 * __this, int32_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializeb_m2728727597_ftn) (BitStream_t1979465639 *, int32_t*);
	static BitStream_Serializeb_m2728727597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializeb_m2728727597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializeb(System.Int32&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializec(System.Char&)
extern "C"  void BitStream_Serializec_m2418807390 (BitStream_t1979465639 * __this, Il2CppChar* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializec_m2418807390_ftn) (BitStream_t1979465639 *, Il2CppChar*);
	static BitStream_Serializec_m2418807390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializec_m2418807390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializec(System.Char&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializes(System.Int16&)
extern "C"  void BitStream_Serializes_m1379945554 (BitStream_t1979465639 * __this, int16_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializes_m1379945554_ftn) (BitStream_t1979465639 *, int16_t*);
	static BitStream_Serializes_m1379945554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializes_m1379945554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializes(System.Int16&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializei(System.Int32&)
extern "C"  void BitStream_Serializei_m1463666006 (BitStream_t1979465639 * __this, int32_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializei_m1463666006_ftn) (BitStream_t1979465639 *, int32_t*);
	static BitStream_Serializei_m1463666006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializei_m1463666006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializei(System.Int32&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializef(System.Single&,System.Single)
extern "C"  void BitStream_Serializef_m745934178 (BitStream_t1979465639 * __this, float* ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	typedef void (*BitStream_Serializef_m745934178_ftn) (BitStream_t1979465639 *, float*, float);
	static BitStream_Serializef_m745934178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializef_m745934178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializef(System.Single&,System.Single)");
	_il2cpp_icall_func(__this, ___value0, ___maximumDelta1);
}
// System.Void UnityEngine.BitStream::Serializeq(UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_Serializeq_m4825889 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	{
		Quaternion_t4030073918 * L_0 = ___value0;
		float L_1 = ___maximumDelta1;
		BitStream_INTERNAL_CALL_Serializeq_m2945252026(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_INTERNAL_CALL_Serializeq_m2945252026 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___self0, Quaternion_t4030073918 * ___value1, float ___maximumDelta2, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializeq_m2945252026_ftn) (BitStream_t1979465639 *, Quaternion_t4030073918 *, float);
	static BitStream_INTERNAL_CALL_Serializeq_m2945252026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializeq_m2945252026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)");
	_il2cpp_icall_func(___self0, ___value1, ___maximumDelta2);
}
// System.Void UnityEngine.BitStream::Serializev(UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_Serializev_m2025246298 (BitStream_t1979465639 * __this, Vector3_t2243707580 * ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = ___value0;
		float L_1 = ___maximumDelta1;
		BitStream_INTERNAL_CALL_Serializev_m928884041(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_INTERNAL_CALL_Serializev_m928884041 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___self0, Vector3_t2243707580 * ___value1, float ___maximumDelta2, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializev_m928884041_ftn) (BitStream_t1979465639 *, Vector3_t2243707580 *, float);
	static BitStream_INTERNAL_CALL_Serializev_m928884041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializev_m928884041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self0, ___value1, ___maximumDelta2);
}
// System.Void UnityEngine.BitStream::Serializen(UnityEngine.NetworkViewID&)
extern "C"  void BitStream_Serializen_m1340978949 (BitStream_t1979465639 * __this, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	{
		NetworkViewID_t3942988548 * L_0 = ___viewID0;
		BitStream_INTERNAL_CALL_Serializen_m1742444286(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)
extern "C"  void BitStream_INTERNAL_CALL_Serializen_m1742444286 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___self0, NetworkViewID_t3942988548 * ___viewID1, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializen_m1742444286_ftn) (BitStream_t1979465639 *, NetworkViewID_t3942988548 *);
	static BitStream_INTERNAL_CALL_Serializen_m1742444286_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializen_m1742444286_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___self0, ___viewID1);
}
// System.Void UnityEngine.BitStream::Serialize(System.Boolean&)
extern "C"  void BitStream_Serialize_m390089021 (BitStream_t1979465639 * __this, bool* ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	bool* G_B5_0 = NULL;
	bool* G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	bool* G_B6_1 = NULL;
	{
		bool* L_0 = ___value0;
		if (!(*((int8_t*)L_0)))
		{
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		BitStream_Serializeb_m2728727597(__this, (&V_0), /*hidden argument*/NULL);
		bool* L_1 = ___value0;
		int32_t L_2 = V_0;
		G_B4_0 = L_1;
		if (L_2)
		{
			G_B5_0 = L_1;
			goto IL_0025;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		goto IL_0026;
	}

IL_0025:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0026:
	{
		*((int8_t*)(G_B6_1)) = (int8_t)G_B6_0;
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Char&)
extern "C"  void BitStream_Serialize_m2742111985 (BitStream_t1979465639 * __this, Il2CppChar* ___value0, const MethodInfo* method)
{
	{
		Il2CppChar* L_0 = ___value0;
		BitStream_Serializec_m2418807390(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Int16&)
extern "C"  void BitStream_Serialize_m1625036513 (BitStream_t1979465639 * __this, int16_t* ___value0, const MethodInfo* method)
{
	{
		int16_t* L_0 = ___value0;
		BitStream_Serializes_m1379945554(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Int32&)
extern "C"  void BitStream_Serialize_m3312079847 (BitStream_t1979465639 * __this, int32_t* ___value0, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___value0;
		BitStream_Serializei_m1463666006(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Single&)
extern "C"  void BitStream_Serialize_m2064522995 (BitStream_t1979465639 * __this, float* ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		float* L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m622035106(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Single&,System.Single)
extern "C"  void BitStream_Serialize_m622035106 (BitStream_t1979465639 * __this, float* ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		float* L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializef_m745934178(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&)
extern "C"  void BitStream_Serialize_m2762694425 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		Quaternion_t4030073918 * L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m3564943814(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_Serialize_m3564943814 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		Quaternion_t4030073918 * L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializeq_m4825889(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&)
extern "C"  void BitStream_Serialize_m753399111 (BitStream_t1979465639 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		Vector3_t2243707580 * L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m1992343662(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_Serialize_m1992343662 (BitStream_t1979465639 * __this, Vector3_t2243707580 * ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializev_m2025246298(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.NetworkPlayer&)
extern "C"  void BitStream_Serialize_m2844992970 (BitStream_t1979465639 * __this, NetworkPlayer_t1243528291 * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		NetworkPlayer_t1243528291 * L_0 = ___value0;
		int32_t L_1 = L_0->get_index_0();
		V_0 = L_1;
		BitStream_Serializei_m1463666006(__this, (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t1243528291 * L_2 = ___value0;
		int32_t L_3 = V_0;
		L_2->set_index_0(L_3);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.NetworkViewID&)
extern "C"  void BitStream_Serialize_m2826028759 (BitStream_t1979465639 * __this, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	{
		NetworkViewID_t3942988548 * L_0 = ___viewID0;
		BitStream_Serializen_m1340978949(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.BitStream::get_isReading()
extern "C"  bool BitStream_get_isReading_m1625418235 (BitStream_t1979465639 * __this, const MethodInfo* method)
{
	typedef bool (*BitStream_get_isReading_m1625418235_ftn) (BitStream_t1979465639 *);
	static BitStream_get_isReading_m1625418235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_get_isReading_m1625418235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::get_isReading()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.BitStream::get_isWriting()
extern "C"  bool BitStream_get_isWriting_m3519823449 (BitStream_t1979465639 * __this, const MethodInfo* method)
{
	typedef bool (*BitStream_get_isWriting_m3519823449_ftn) (BitStream_t1979465639 *);
	static BitStream_get_isWriting_m3519823449_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_get_isWriting_m3519823449_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::get_isWriting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.BitStream::Serialize(System.String&)
extern "C"  void BitStream_Serialize_m1778986188 (BitStream_t1979465639 * __this, String_t** ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serialize_m1778986188_ftn) (BitStream_t1979465639 *, String_t**);
	static BitStream_Serialize_m1778986188_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serialize_m1778986188_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serialize(System.String&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m3536967407 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m3536967407_ftn) (Camera_t189460977 *);
	static Camera_get_nearClipPlane_m3536967407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m3536967407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m3137713566 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m3137713566_ftn) (Camera_t189460977 *);
	static Camera_get_farClipPlane_m3137713566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m3137713566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m73686965 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m73686965_ftn) (Camera_t189460977 *);
	static Camera_get_cullingMask_m73686965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m73686965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m4241372419 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m4241372419_ftn) (Camera_t189460977 *);
	static Camera_get_eventMask_m4241372419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m4241372419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t3681755626  Camera_get_pixelRect_m2084185953 (Camera_t189460977 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_pixelRect_m1785951490(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3681755626  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m1785951490 (Camera_t189460977 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m1785951490_ftn) (Camera_t189460977 *, Rect_t3681755626 *);
	static Camera_INTERNAL_get_pixelRect_m1785951490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m1785951490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2666733923 * Camera_get_targetTexture_m705925974 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t2666733923 * (*Camera_get_targetTexture_m705925974_ftn) (Camera_t189460977 *);
	static Camera_get_targetTexture_m705925974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m705925974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C"  void Camera_set_targetTexture_m3925036117 (Camera_t189460977 * __this, RenderTexture_t2666733923 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_targetTexture_m3925036117_ftn) (Camera_t189460977 *, RenderTexture_t2666733923 *);
	static Camera_set_targetTexture_m3925036117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_targetTexture_m3925036117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m1743144302 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m1743144302_ftn) (Camera_t189460977 *);
	static Camera_get_clearFlags_m1743144302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m1743144302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t2469606224  Camera_ScreenPointToRay_m614889538 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t2469606224  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t2469606224  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Ray_t2469606224  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Vector3_t2243707580 * ___position1, Ray_t2469606224 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646_ftn) (Camera_t189460977 *, Vector3_t2243707580 *, Ray_t2469606224 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m989474043 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m989474043_ftn) ();
	static Camera_get_allCamerasCount_m989474043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m989474043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m2922515227 (Il2CppObject * __this /* static, unused */, CameraU5BU5D_t3079764780* ___cameras0, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m2922515227_ftn) (CameraU5BU5D_t3079764780*);
	static Camera_GetAllCameras_m2922515227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m2922515227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras0);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m1679634170_MetadataUsageId;
extern "C"  void Camera_FireOnPreCull_m1679634170 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m1679634170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t834278767 * L_0 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		Camera_t189460977 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3079065225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreRender_m24116662_MetadataUsageId;
extern "C"  void Camera_FireOnPreRender_m24116662 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m24116662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t834278767 * L_0 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		Camera_t189460977 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3079065225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPostRender_m94860165_MetadataUsageId;
extern "C"  void Camera_FireOnPostRender_m94860165 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m94860165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t834278767 * L_0 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		Camera_t189460977 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3079065225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::Render()
extern "C"  void Camera_Render_m2021402646 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef void (*Camera_Render_m2021402646_ftn) (Camera_t189460977 *);
	static Camera_Render_m2021402646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_Render_m2021402646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::Render()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * Camera_RaycastTry_m3412198936 (Camera_t189460977 * __this, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	{
		V_0 = 0;
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		int32_t L_2 = V_0;
		GameObject_t1756533147 * L_3 = Camera_INTERNAL_CALL_RaycastTry_m2295752796(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		GameObject_t1756533147 * L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  GameObject_t1756533147 * Camera_INTERNAL_CALL_RaycastTry_m2295752796 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Ray_t2469606224 * ___ray1, float ___distance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*Camera_INTERNAL_CALL_RaycastTry_m2295752796_ftn) (Camera_t189460977 *, Ray_t2469606224 *, float, int32_t, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m2295752796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m2295752796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3, ___queryTriggerInteraction4);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * Camera_RaycastTry2D_m755036866 (Camera_t189460977 * __this, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	GameObject_t1756533147 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t1756533147 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m1020711785(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t1756533147 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * Camera_INTERNAL_CALL_RaycastTry2D_m1020711785 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Ray_t2469606224 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*Camera_INTERNAL_CALL_RaycastTry2D_m1020711785_ftn) (Camera_t189460977 *, Ray_t2469606224 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m1020711785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m1020711785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m2929748071 (CameraCallback_t834278767 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m3079065225 (CameraCallback_t834278767 * __this, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m3079065225((CameraCallback_t834278767 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Camera_t189460977 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t189460977 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CameraCallback_BeginInvoke_m144217562 (CameraCallback_t834278767 * __this, Camera_t189460977 * ___cam0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m2103705933 (CameraCallback_t834278767 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Component::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m205306948_MetadataUsageId;
extern "C"  void Component__ctor_m205306948 (Component_t3819376471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m205306948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Component_get_transform_m2697483695_ftn) (Component_t3819376471 *);
	static Component_get_transform_m2697483695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m2697483695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*Component_get_gameObject_m3105766835_ftn) (Component_t3819376471 *);
	static Component_get_gameObject_m3105766835_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m3105766835_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponent_m4225719715 (Component_t3819376471 * __this, Type_t * ___type0, const MethodInfo* method)
{
	Component_t3819376471 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m306258075(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t3819376471 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m2700814707 (Component_t3819376471 * __this, Type_t * ___type0, IntPtr_t ___oneFurtherThanResultValue1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m2700814707_ftn) (Component_t3819376471 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m2700814707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m2700814707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
extern "C"  Component_t3819376471 * Component_GetComponent_m2473832642 (Component_t3819376471 * __this, String_t* ___type0, const MethodInfo* method)
{
	typedef Component_t3819376471 * (*Component_GetComponent_m2473832642_ftn) (Component_t3819376471 *, String_t*);
	static Component_GetComponent_m2473832642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponent_m2473832642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponent(System.String)");
	return _il2cpp_icall_func(__this, ___type0);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t3819376471 * Component_GetComponentInChildren_m3925629424 (Component_t3819376471 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	Component_t3819376471 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t3819376471 * L_3 = GameObject_GetComponentInChildren_m4263325740(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Component_t3819376471 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponentInChildren_m3985003615 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	Component_t3819376471 * V_0 = NULL;
	{
		Type_t * L_0 = ___t0;
		Component_t3819376471 * L_1 = Component_GetComponentInChildren_m3925629424(__this, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Component_t3819376471 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInChildren_m843288020 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	ComponentU5BU5D_t4136971630* V_1 = NULL;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t4136971630* L_2 = Component_GetComponentsInChildren_m908027537(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		ComponentU5BU5D_t4136971630* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInChildren_m908027537 (Component_t3819376471 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	ComponentU5BU5D_t4136971630* V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t4136971630* L_3 = GameObject_GetComponentsInChildren_m993725821(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		ComponentU5BU5D_t4136971630* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponentInParent_m2799402500 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	Component_t3819376471 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t3819376471 * L_2 = GameObject_GetComponentInParent_m1235194528(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t3819376471 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInParent_m4192184629 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	ComponentU5BU5D_t4136971630* V_1 = NULL;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t4136971630* L_2 = Component_GetComponentsInParent_m1920178904(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		ComponentU5BU5D_t4136971630* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInParent_m1920178904 (Component_t3819376471 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	ComponentU5BU5D_t4136971630* V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t4136971630* L_3 = GameObject_GetComponentsInParent_m1568786844(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		ComponentU5BU5D_t4136971630* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponents(System.Type)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponents_m637589504 (Component_t3819376471 * __this, Type_t * ___type0, const MethodInfo* method)
{
	ComponentU5BU5D_t4136971630* V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		ComponentU5BU5D_t4136971630* L_2 = GameObject_GetComponents_m297658252(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		ComponentU5BU5D_t4136971630* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m4241828391 (Component_t3819376471 * __this, Type_t * ___searchType0, Il2CppObject * ___resultList1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m4241828391_ftn) (Component_t3819376471 *, Type_t *, Il2CppObject *);
	static Component_GetComponentsForListInternal_m4241828391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m4241828391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m3712441745 (Component_t3819376471 * __this, Type_t * ___type0, List_1_t3188497603 * ___results1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t3188497603 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m4241828391(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m357168014 (Component_t3819376471 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m1425941094(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Component::set_tag(System.String)
extern "C"  void Component_set_tag_m4292293503 (Component_t3819376471 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		GameObject_set_tag_m717375123(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C"  bool Component_CompareTag_m3443292365 (Component_t3819376471 * __this, String_t* ___tag0, const MethodInfo* method)
{
	typedef bool (*Component_CompareTag_m3443292365_ftn) (Component_t3819376471 *, String_t*);
	static Component_CompareTag_m3443292365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_CompareTag_m3443292365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::CompareTag(System.String)");
	return _il2cpp_icall_func(__this, ___tag0);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m2584088787 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessageUpwards_m2584088787_ftn) (Component_t3819376471 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessageUpwards_m2584088787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessageUpwards_m2584088787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object)
extern "C"  void Component_SendMessageUpwards_m325086847 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m2584088787(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String)
extern "C"  void Component_SendMessageUpwards_m2041012277 (Component_t3819376471 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m2584088787(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m19741277 (Component_t3819376471 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessageUpwards_m2584088787(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m2241432133 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessage_m2241432133_ftn) (Component_t3819376471 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessage_m2241432133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m2241432133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C"  void Component_SendMessage_m913946877 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessage_m2241432133(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C"  void Component_SendMessage_m3615678587 (Component_t3819376471 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessage_m2241432133(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m4199581575 (Component_t3819376471 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessage_m2241432133(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m2230184532 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_BroadcastMessage_m2230184532_ftn) (Component_t3819376471 *, String_t*, Il2CppObject *, int32_t);
	static Component_BroadcastMessage_m2230184532_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_BroadcastMessage_m2230184532_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___parameter1, ___options2);
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object)
extern "C"  void Component_BroadcastMessage_m1308086896 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___parameter1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m2230184532(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String)
extern "C"  void Component_BroadcastMessage_m1706240890 (Component_t3819376471 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m2230184532(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m444672650 (Component_t3819376471 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_BroadcastMessage_m2230184532(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m1253937571 (Coroutine_t2299508840 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m833118514 (Coroutine_t2299508840 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m833118514_ftn) (Coroutine_t2299508840 *);
	static Coroutine_ReleaseCoroutine_m833118514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m833118514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m3953619693 (Coroutine_t2299508840 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m833118514(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_pinvoke(const Coroutine_t2299508840& unmarshaled, Coroutine_t2299508840_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t2299508840_marshal_pinvoke_back(const Coroutine_t2299508840_marshaled_pinvoke& marshaled, Coroutine_t2299508840& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_pinvoke_cleanup(Coroutine_t2299508840_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_com(const Coroutine_t2299508840& unmarshaled, Coroutine_t2299508840_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t2299508840_marshal_com_back(const Coroutine_t2299508840_marshaled_com& marshaled, Coroutine_t2299508840& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_com_cleanup(Coroutine_t2299508840_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_Finalize_m3172731580_MetadataUsageId;
extern "C"  void CullingGroup_Finalize_m3172731580 (CullingGroup_t1091689465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m3172731580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Ptr_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_001e;
			}
		}

IL_0016:
		{
			CullingGroup_FinalizerFailure_m3675513936(__this, /*hidden argument*/NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m2629191995 (CullingGroup_t1091689465 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_Dispose_m2629191995_ftn) (CullingGroup_t1091689465 *);
	static CullingGroup_Dispose_m2629191995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m2629191995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m1292564468 (Il2CppObject * __this /* static, unused */, CullingGroup_t1091689465 * ___cullingGroup0, IntPtr_t ___eventsPtr1, int32_t ___count2, const MethodInfo* method)
{
	CullingGroupEvent_t1057617917 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m1888290092((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t1057617917 *)L_0;
		CullingGroup_t1091689465 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t2480912210 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0046;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0020:
	{
		CullingGroup_t1091689465 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t2480912210 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t1057617917 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = sizeof(CullingGroupEvent_t1057617917 );
		NullCheck(L_4);
		StateChanged_Invoke_m2308261448(L_4, (*(CullingGroupEvent_t1057617917 *)((CullingGroupEvent_t1057617917 *)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m3675513936 (CullingGroup_t1091689465 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m3675513936_ftn) (CullingGroup_t1091689465 *);
	static CullingGroup_FinalizerFailure_m3675513936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m3675513936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_pinvoke(const CullingGroup_t1091689465& unmarshaled, CullingGroup_t1091689465_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern Il2CppClass* StateChanged_t2480912210_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1091689465_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_back(const CullingGroup_t1091689465_marshaled_pinvoke& marshaled, CullingGroup_t1091689465& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1091689465_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2480912210>(marshaled.___m_OnStateChanged_1, StateChanged_t2480912210_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_cleanup(CullingGroup_t1091689465_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_com(const CullingGroup_t1091689465& unmarshaled, CullingGroup_t1091689465_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern Il2CppClass* StateChanged_t2480912210_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1091689465_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void CullingGroup_t1091689465_marshal_com_back(const CullingGroup_t1091689465_marshaled_com& marshaled, CullingGroup_t1091689465& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1091689465_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2480912210>(marshaled.___m_OnStateChanged_1, StateChanged_t2480912210_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_com_cleanup(CullingGroup_t1091689465_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m2322131884 (StateChanged_t2480912210 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m2308261448 (StateChanged_t2480912210 * __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m2308261448((StateChanged_t2480912210 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t2480912210 (StateChanged_t2480912210 * __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t1057617917 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___sphere0);

}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern Il2CppClass* CullingGroupEvent_t1057617917_il2cpp_TypeInfo_var;
extern const uint32_t StateChanged_BeginInvoke_m1716538087_MetadataUsageId;
extern "C"  Il2CppObject * StateChanged_BeginInvoke_m1716538087 (StateChanged_t2480912210 * __this, CullingGroupEvent_t1057617917  ___sphere0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m1716538087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t1057617917_il2cpp_TypeInfo_var, &___sphere0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m2186648314 (StateChanged_t2480912210 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.ILogger UnityEngine.Debug::get_logger()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug_get_logger_m4173808038_MetadataUsageId;
extern "C"  Il2CppObject * Debug_get_logger_m4173808038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_logger_m4173808038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Debug_t1368543263_StaticFields*)Debug_t1368543263_il2cpp_TypeInfo_var->static_fields)->get_s_Logger_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m920475918_MetadataUsageId;
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m920475918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogError_m3715728798_MetadataUsageId;
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m3715728798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogException_m1861430175_MetadataUsageId;
extern "C"  void Debug_LogException_m1861430175 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m1861430175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1927440687 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t1927440687 *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_0, L_1, (Object_t1021602117 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern Il2CppClass* DebugLogHandler_t865810509_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t3328995178_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m2981642087_MetadataUsageId;
extern "C"  void Debug__cctor_m2981642087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m2981642087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebugLogHandler_t865810509 * L_0 = (DebugLogHandler_t865810509 *)il2cpp_codegen_object_new(DebugLogHandler_t865810509_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m3134219506(L_0, /*hidden argument*/NULL);
		Logger_t3328995178 * L_1 = (Logger_t3328995178 *)il2cpp_codegen_object_new(Logger_t3328995178_il2cpp_TypeInfo_var);
		Logger__ctor_m3834134587(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t1368543263_StaticFields*)Debug_t1368543263_il2cpp_TypeInfo_var->static_fields)->set_s_Logger_0(L_1);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m3134219506 (DebugLogHandler_t865810509 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m3491540823 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t1021602117 * ___obj2, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m3491540823_ftn) (int32_t, String_t*, Object_t1021602117 *);
	static DebugLogHandler_Internal_Log_m3491540823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m3491540823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m317712981 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___obj1, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m317712981_ftn) (Exception_t1927440687 *, Object_t1021602117 *);
	static DebugLogHandler_Internal_LogException_m317712981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m317712981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLogHandler_LogFormat_m177245518_MetadataUsageId;
extern "C"  void DebugLogHandler_LogFormat_m177245518 (DebugLogHandler_t865810509 * __this, int32_t ___logType0, Object_t1021602117 * ___context1, String_t* ___format2, ObjectU5BU5D_t3614634134* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m177245518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t3614634134* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1263743648(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t1021602117 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m3491540823(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m769094553 (DebugLogHandler_t865810509 * __this, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ___exception0;
		Object_t1021602117 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m317712981(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m1561221759 (DefaultExecutionOrder_t2717914595 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CorderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m2167115811 (Display_t3666191348 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m2996690883(&L_0, 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m563295973 (Display_t3666191348 * __this, IntPtr_t ___nativeDisplay0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern Il2CppClass* DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var;
extern Il2CppClass* Display_t3666191348_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m3412638488_MetadataUsageId;
extern "C"  void Display_RecreateDisplayList_m3412638488 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t169632028* ___nativeDisplay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m3412638488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t169632028* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set_displays_1(((DisplayU5BU5D_t1314630077*)SZArrayNew(DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1314630077* L_1 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t169632028* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		IntPtr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t3666191348 * L_7 = (Display_t3666191348 *)il2cpp_codegen_object_new(Display_t3666191348_il2cpp_TypeInfo_var);
		Display__ctor_m563295973(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t3666191348 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t169632028* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1314630077* L_11 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Display_t3666191348 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern Il2CppClass* Display_t3666191348_il2cpp_TypeInfo_var;
extern const uint32_t Display_FireDisplaysUpdated_m3557250167_MetadataUsageId;
extern "C"  void Display_FireDisplaysUpdated_m3557250167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m3557250167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t3423469815 * L_0 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t3423469815 * L_1 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m3646339243(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern Il2CppClass* DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var;
extern Il2CppClass* Display_t3666191348_il2cpp_TypeInfo_var;
extern const uint32_t Display__cctor_m7440126_MetadataUsageId;
extern "C"  void Display__cctor_m7440126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m7440126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisplayU5BU5D_t1314630077* L_0 = ((DisplayU5BU5D_t1314630077*)SZArrayNew(DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t3666191348 * L_1 = (Display_t3666191348 *)il2cpp_codegen_object_new(Display_t3666191348_il2cpp_TypeInfo_var);
		Display__ctor_m2167115811(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t3666191348 *)L_1);
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set_displays_1(L_0);
		DisplayU5BU5D_t1314630077* L_2 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_2);
		int32_t L_3 = 0;
		Display_t3666191348 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_4);
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t3423469815 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m2851669167 (DisplaysUpdatedDelegate_t3423469815 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m3646339243 (DisplaysUpdatedDelegate_t3423469815 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m3646339243((DisplaysUpdatedDelegate_t3423469815 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 (DisplaysUpdatedDelegate_t3423469815 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DisplaysUpdatedDelegate_BeginInvoke_m2030617484 (DisplaysUpdatedDelegate_t3423469815 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m3234564837 (DisplaysUpdatedDelegate_t3423469815 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m484215967 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3774245231;
extern Il2CppCodeGenString* _stringLiteral1256080173;
extern Il2CppCodeGenString* _stringLiteral1653664622;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m1302102306_MetadataUsageId;
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m1302102306 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m1302102306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_009f;
	}

IL_0016:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_0();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m4251815737(L_2, _stringLiteral3774245231, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m4290821911(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003c:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_0();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m4251815737(L_8, _stringLiteral1256080173, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m4290821911(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_005c:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_0();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m4251815737(L_14, _stringLiteral1653664622, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m4290821911(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_007c:
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_008c;
		}
	}
	{
		goto IL_009f;
	}

IL_008c:
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_0();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m12482732(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_0(L_23);
	}

IL_009f:
	{
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m2819142469 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1302102306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m3936525937 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1302102306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern Il2CppClass* List_1_t1598685972_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m735894212_MethodInfo_var;
extern const uint32_t InvokableCallList__ctor_m1209277579_MetadataUsageId;
extern "C"  void InvokableCallList__ctor_m1209277579 (InvokableCallList_t2295673753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m1209277579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1598685972 * L_0 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_0, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t1598685972 * L_1 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_1, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t1598685972 * L_2 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_2, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo* List_1_Clear_m649507351_MethodInfo_var;
extern const uint32_t InvokableCallList_ClearPersistent_m3606847843_MetadataUsageId;
extern "C"  void InvokableCallList_ClearPersistent_m3606847843 (InvokableCallList_t2295673753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m3606847843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1598685972 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m649507351(L_0, /*hidden argument*/List_1_Clear_m649507351_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern Il2CppClass* ArgumentCache_t4810721_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m2079279187_MetadataUsageId;
extern "C"  void PersistentCall__ctor_m2079279187 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m2079279187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Mode_0(0);
		ArgumentCache_t4810721 * L_0 = (ArgumentCache_t4810721 *)il2cpp_codegen_object_new(ArgumentCache_t4810721_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m484215967(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_1(L_0);
		__this->set_m_CallState_2(2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern Il2CppClass* List_1_t3162557601_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m477348949_MethodInfo_var;
extern const uint32_t PersistentCallGroup__ctor_m3217457172_MetadataUsageId;
extern "C"  void PersistentCallGroup__ctor_m3217457172 (PersistentCallGroup_t339478082 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m3217457172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t3162557601 * L_0 = (List_1_t3162557601 *)il2cpp_codegen_object_new(List_1_t3162557601_il2cpp_TypeInfo_var);
		List_1__ctor_m477348949(L_0, /*hidden argument*/List_1__ctor_m477348949_MethodInfo_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent__ctor_m588741179_MetadataUsageId;
extern "C"  void UnityEvent__ctor_m588741179 (UnityEvent_t408735097 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent__ctor_m588741179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)));
		UnityEventBase__ctor_m4062111756(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern Il2CppClass* InvokableCallList_t2295673753_il2cpp_TypeInfo_var;
extern Il2CppClass* PersistentCallGroup_t339478082_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventBase__ctor_m4062111756_MetadataUsageId;
extern "C"  void UnityEventBase__ctor_m4062111756 (UnityEventBase_t828812576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase__ctor_m4062111756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_CallsDirty_3((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		InvokableCallList_t2295673753 * L_0 = (InvokableCallList_t2295673753 *)il2cpp_codegen_object_new(InvokableCallList_t2295673753_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m1209277579(L_0, /*hidden argument*/NULL);
		__this->set_m_Calls_0(L_0);
		PersistentCallGroup_t339478082 * L_1 = (PersistentCallGroup_t339478082 *)il2cpp_codegen_object_new(PersistentCallGroup_t339478082_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m3217457172(L_1, /*hidden argument*/NULL);
		__this->set_m_PersistentCalls_1(L_1);
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->set_m_TypeName_2(L_3);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1773826808 (UnityEventBase_t828812576 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3058501374 (UnityEventBase_t828812576 * __this, const MethodInfo* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m2596210672(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_m_TypeName_2(L_1);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m2596210672 (UnityEventBase_t828812576 * __this, const MethodInfo* method)
{
	{
		InvokableCallList_t2295673753 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m3606847843(L_0, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)1);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t UnityEventBase_ToString_m433343709_MetadataUsageId;
extern "C"  String_t* UnityEventBase_ToString_m433343709 (UnityEventBase_t828812576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_ToString_m433343709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m853381981(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, L_0, _stringLiteral372029310, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.AnimationPlayable::get_node()
extern "C"  Playable_t3667545548  AnimationPlayable_get_node_m1103082298 (AnimationPlayable_t1693994278 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = __this->get_handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Playable_t3667545548  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Playable_t3667545548  AnimationPlayable_get_node_m1103082298_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_get_node_m1103082298(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Experimental.Director.AnimationPlayable::AddInput(UnityEngine.Experimental.Director.Playable)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral65493076;
extern const uint32_t AnimationPlayable_AddInput_m709028173_MetadataUsageId;
extern "C"  int32_t AnimationPlayable_AddInput_m709028173 (AnimationPlayable_t1693994278 * __this, Playable_t3667545548  ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayable_AddInput_m709028173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		Playable_t3667545548  L_0 = ___input0;
		Playable_t3667545548  L_1 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		bool L_2 = Playable_Connect_m1305428597(NULL /*static, unused*/, L_0, L_1, (-1), (-1), /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_4, _stringLiteral65493076, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0026:
	{
		int32_t L_5 = AnimationPlayable_get_inputCount_m1225845575(__this, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_5-(int32_t)1));
		goto IL_0034;
	}

IL_0034:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
extern "C"  int32_t AnimationPlayable_AddInput_m709028173_AdjustorThunk (Il2CppObject * __this, Playable_t3667545548  ___input0, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_AddInput_m709028173(_thisAdjusted, ___input0, method);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayable::SetInput(UnityEngine.Experimental.Director.Playable,System.Int32)
extern "C"  bool AnimationPlayable_SetInput_m3281748385 (AnimationPlayable_t1693994278 * __this, Playable_t3667545548  ___source0, int32_t ___index1, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	Playable_t3667545548  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Playable_t3667545548  L_0 = AnimationPlayable_get_node_m1103082298(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___index1;
		bool L_2 = Playable_CheckInputBounds_m1458023351((&V_0), L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_005c;
	}

IL_001c:
	{
		int32_t L_3 = ___index1;
		Playable_t3667545548  L_4 = AnimationPlayable_GetInput_m1403317770(__this, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		bool L_5 = Playable_IsValid_m2102916683((&V_2), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		Playable_t3667545548  L_6 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		int32_t L_7 = ___index1;
		Playable_Disconnect_m2489180239(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0043:
	{
		Playable_t3667545548  L_8 = ___source0;
		Playable_t3667545548  L_9 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		bool L_11 = Playable_Connect_m1305428597(NULL /*static, unused*/, L_8, L_9, (-1), L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_005c;
	}

IL_005c:
	{
		bool L_12 = V_1;
		return L_12;
	}
}
extern "C"  bool AnimationPlayable_SetInput_m3281748385_AdjustorThunk (Il2CppObject * __this, Playable_t3667545548  ___source0, int32_t ___index1, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_SetInput_m3281748385(_thisAdjusted, ___source0, ___index1, method);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayable::SetInputs(System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.Director.Playable>)
extern Il2CppClass* IEnumerable_1_t3959672593_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1143069375_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t AnimationPlayable_SetInputs_m370531328_MetadataUsageId;
extern "C"  bool AnimationPlayable_SetInputs_m370531328 (AnimationPlayable_t1693994278 * __this, Il2CppObject* ___sources0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayable_SetInputs_m370531328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	Playable_t3667545548  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Il2CppObject* V_4 = NULL;
	Playable_t3667545548  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	Playable_t3667545548  V_7;
	memset(&V_7, 0, sizeof(V_7));
	bool V_8 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0008:
	{
		Playable_t3667545548  L_0 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		Playable_Disconnect_m2489180239(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_3 = V_0;
		int32_t L_4 = AnimationPlayable_get_inputCount_m1225845575(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_4)))
		{
			goto IL_0008;
		}
	}
	{
		V_1 = (bool)0;
		V_2 = 0;
		Il2CppObject* L_5 = ___sources0;
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.Director.Playable>::GetEnumerator() */, IEnumerable_1_t3959672593_il2cpp_TypeInfo_var, L_5);
		V_4 = L_6;
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a2;
		}

IL_003d:
		{
			Il2CppObject* L_7 = V_4;
			NullCheck(L_7);
			Playable_t3667545548  L_8 = InterfaceFuncInvoker0< Playable_t3667545548  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Experimental.Director.Playable>::get_Current() */, IEnumerator_1_t1143069375_il2cpp_TypeInfo_var, L_7);
			V_3 = L_8;
			int32_t L_9 = V_2;
			int32_t L_10 = AnimationPlayable_get_inputCount_m1225845575(__this, /*hidden argument*/NULL);
			if ((((int32_t)L_9) >= ((int32_t)L_10)))
			{
				goto IL_006f;
			}
		}

IL_0052:
		{
			bool L_11 = V_1;
			Playable_t3667545548  L_12 = V_3;
			Playable_t3667545548  L_13 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
			int32_t L_14 = V_2;
			bool L_15 = Playable_Connect_m1305428597(NULL /*static, unused*/, L_12, L_13, (-1), L_14, /*hidden argument*/NULL);
			V_1 = (bool)((int32_t)((int32_t)L_11|(int32_t)L_15));
			goto IL_0087;
		}

IL_006f:
		{
			bool L_16 = V_1;
			Playable_t3667545548  L_17 = V_3;
			Playable_t3667545548  L_18 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
			bool L_19 = Playable_Connect_m1305428597(NULL /*static, unused*/, L_17, L_18, (-1), (-1), /*hidden argument*/NULL);
			V_1 = (bool)((int32_t)((int32_t)L_16|(int32_t)L_19));
		}

IL_0087:
		{
			Playable_t3667545548  L_20 = AnimationPlayable_get_node_m1103082298(__this, /*hidden argument*/NULL);
			V_5 = L_20;
			int32_t L_21 = V_2;
			Playable_SetInputWeight_m3424075019((&V_5), L_21, (1.0f), /*hidden argument*/NULL);
			int32_t L_22 = V_2;
			V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
		}

IL_00a2:
		{
			Il2CppObject* L_23 = V_4;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_003d;
			}
		}

IL_00ae:
		{
			IL2CPP_LEAVE(0xC2, FINALLY_00b3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00b3;
	}

FINALLY_00b3:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_25 = V_4;
			if (!L_25)
			{
				goto IL_00c1;
			}
		}

IL_00ba:
		{
			Il2CppObject* L_26 = V_4;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_26);
		}

IL_00c1:
		{
			IL2CPP_END_FINALLY(179)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(179)
	{
		IL2CPP_JUMP_TBL(0xC2, IL_00c2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c2:
	{
		int32_t L_27 = V_2;
		V_6 = L_27;
		goto IL_00e9;
	}

IL_00ca:
	{
		Playable_t3667545548  L_28 = AnimationPlayable_get_node_m1103082298(__this, /*hidden argument*/NULL);
		V_7 = L_28;
		int32_t L_29 = V_6;
		Playable_SetInputWeight_m3424075019((&V_7), L_29, (0.0f), /*hidden argument*/NULL);
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00e9:
	{
		int32_t L_31 = V_6;
		int32_t L_32 = AnimationPlayable_get_inputCount_m1225845575(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_00ca;
		}
	}
	{
		bool L_33 = V_1;
		V_8 = L_33;
		goto IL_00fe;
	}

IL_00fe:
	{
		bool L_34 = V_8;
		return L_34;
	}
}
extern "C"  bool AnimationPlayable_SetInputs_m370531328_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___sources0, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_SetInputs_m370531328(_thisAdjusted, ___sources0, method);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayable::RemoveInput(System.Int32)
extern "C"  bool AnimationPlayable_RemoveInput_m2728059274 (AnimationPlayable_t1693994278 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548  L_0 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		bool L_2 = Playables_CheckInputBounds_m2953342617(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0036;
	}

IL_001e:
	{
		Playable_t3667545548  L_3 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		int32_t L_4 = ___index0;
		Playable_Disconnect_m2489180239(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0036;
	}

IL_0036:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
extern "C"  bool AnimationPlayable_RemoveInput_m2728059274_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_RemoveInput_m2728059274(_thisAdjusted, ___index0, method);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayable::RemoveAllInputs()
extern "C"  bool AnimationPlayable_RemoveAllInputs_m2002968067 (AnimationPlayable_t1693994278 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Playable_t3667545548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	bool V_3 = false;
	{
		Playable_t3667545548  L_0 = AnimationPlayable_get_node_m1103082298(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		int32_t L_1 = Playable_get_inputCount_m1751152777((&V_1), /*hidden argument*/NULL);
		V_0 = L_1;
		V_2 = 0;
		goto IL_0025;
	}

IL_0017:
	{
		int32_t L_2 = V_2;
		AnimationPlayable_RemoveInput_m2728059274(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_2;
		V_2 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_4 = V_2;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0017;
		}
	}
	{
		V_3 = (bool)1;
		goto IL_0033;
	}

IL_0033:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
extern "C"  bool AnimationPlayable_RemoveAllInputs_m2002968067_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_RemoveAllInputs_m2002968067(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayable::Equals(System.Object)
extern "C"  bool AnimationPlayable_Equals_m3235880962 (AnimationPlayable_t1693994278 * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548  L_0 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___p0;
		bool L_2 = Playables_Equals_m3918086298(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationPlayable_Equals_m3235880962_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_Equals_m3235880962(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Experimental.Director.AnimationPlayable::GetHashCode()
extern "C"  int32_t AnimationPlayable_GetHashCode_m347069006 (AnimationPlayable_t1693994278 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Playable_t3667545548  L_0 = AnimationPlayable_get_node_m1103082298(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Playable_GetHashCode_m291506040((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t AnimationPlayable_GetHashCode_m347069006_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_GetHashCode_m347069006(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.AnimationPlayable::op_Implicit(UnityEngine.Experimental.Director.AnimationPlayable)
extern "C"  Playable_t3667545548  AnimationPlayable_op_Implicit_m408821639 (Il2CppObject * __this /* static, unused */, AnimationPlayable_t1693994278  ___b0, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = AnimationPlayable_get_node_m1103082298((&___b0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Playable_t3667545548  L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayable::IsValid()
extern "C"  bool AnimationPlayable_IsValid_m1938227437 (AnimationPlayable_t1693994278 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548  L_0 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		bool L_1 = Playables_IsValid_m3689585029(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AnimationPlayable_IsValid_m1938227437_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_IsValid_m1938227437(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Experimental.Director.AnimationPlayable::get_inputCount()
extern Il2CppClass* AnimationPlayable_t1693994278_il2cpp_TypeInfo_var;
extern const uint32_t AnimationPlayable_get_inputCount_m1225845575_MetadataUsageId;
extern "C"  int32_t AnimationPlayable_get_inputCount_m1225845575 (AnimationPlayable_t1693994278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayable_get_inputCount_m1225845575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Playable_t3667545548  L_0 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		AnimationPlayable_t1693994278  L_1 = (*(AnimationPlayable_t1693994278 *)__this);
		Il2CppObject * L_2 = Box(AnimationPlayable_t1693994278_il2cpp_TypeInfo_var, &L_1);
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m191970594(L_2, /*hidden argument*/NULL);
		int32_t L_4 = Playables_GetInputCountValidated_m163517061(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0027;
	}

IL_0027:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
extern "C"  int32_t AnimationPlayable_get_inputCount_m1225845575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_get_inputCount_m1225845575(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.AnimationPlayable::GetInput(System.Int32)
extern Il2CppClass* AnimationPlayable_t1693994278_il2cpp_TypeInfo_var;
extern const uint32_t AnimationPlayable_GetInput_m1403317770_MetadataUsageId;
extern "C"  Playable_t3667545548  AnimationPlayable_GetInput_m1403317770 (AnimationPlayable_t1693994278 * __this, int32_t ___inputPort0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayable_GetInput_m1403317770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, (*(AnimationPlayable_t1693994278 *)__this), /*hidden argument*/NULL);
		int32_t L_1 = ___inputPort0;
		AnimationPlayable_t1693994278  L_2 = (*(AnimationPlayable_t1693994278 *)__this);
		Il2CppObject * L_3 = Box(AnimationPlayable_t1693994278_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		Playable_t3667545548  L_5 = Playables_GetInputValidated_m2433189843(NULL /*static, unused*/, L_0, L_1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0028;
	}

IL_0028:
	{
		Playable_t3667545548  L_6 = V_0;
		return L_6;
	}
}
extern "C"  Playable_t3667545548  AnimationPlayable_GetInput_m1403317770_AdjustorThunk (Il2CppObject * __this, int32_t ___inputPort0, const MethodInfo* method)
{
	AnimationPlayable_t1693994278 * _thisAdjusted = reinterpret_cast<AnimationPlayable_t1693994278 *>(__this + 1);
	return AnimationPlayable_GetInput_m1403317770(_thisAdjusted, ___inputPort0, method);
}
// System.Int32 UnityEngine.Experimental.Director.AnimationPlayableUtilities::AddInputValidated(UnityEngine.Experimental.Director.AnimationPlayable,UnityEngine.Experimental.Director.Playable,System.Type)
extern "C"  int32_t AnimationPlayableUtilities_AddInputValidated_m3676524933 (Il2CppObject * __this /* static, unused */, AnimationPlayable_t1693994278  ___target0, Playable_t3667545548  ___input1, Type_t * ___typeofTarget2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548  L_0 = ___input1;
		int32_t L_1 = AnimationPlayable_AddInput_m709028173((&___target0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayableUtilities::SetInputValidated(UnityEngine.Experimental.Director.AnimationPlayable,UnityEngine.Experimental.Director.Playable,System.Int32,System.Type)
extern "C"  bool AnimationPlayableUtilities_SetInputValidated_m2748997465 (Il2CppObject * __this /* static, unused */, AnimationPlayable_t1693994278  ___target0, Playable_t3667545548  ___source1, int32_t ___index2, Type_t * ___typeofTarget3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548  L_0 = ___source1;
		int32_t L_1 = ___index2;
		bool L_2 = AnimationPlayable_SetInput_m3281748385((&___target0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayableUtilities::SetInputsValidated(UnityEngine.Experimental.Director.AnimationPlayable,System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.Director.Playable>,System.Type)
extern "C"  bool AnimationPlayableUtilities_SetInputsValidated_m2696856860 (Il2CppObject * __this /* static, unused */, AnimationPlayable_t1693994278  ___target0, Il2CppObject* ___sources1, Type_t * ___typeofTarget2, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject* L_0 = ___sources1;
		bool L_1 = AnimationPlayable_SetInputs_m370531328((&___target0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayableUtilities::RemoveInputValidated(UnityEngine.Experimental.Director.AnimationPlayable,System.Int32,System.Type)
extern "C"  bool AnimationPlayableUtilities_RemoveInputValidated_m438691908 (Il2CppObject * __this /* static, unused */, AnimationPlayable_t1693994278  ___target0, int32_t ___index1, Type_t * ___typeofTarget2, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___index1;
		bool L_1 = AnimationPlayable_RemoveInput_m2728059274((&___target0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationPlayableUtilities::RemoveAllInputsValidated(UnityEngine.Experimental.Director.AnimationPlayable,System.Type)
extern "C"  bool AnimationPlayableUtilities_RemoveAllInputsValidated_m1936894983 (Il2CppObject * __this /* static, unused */, AnimationPlayable_t1693994278  ___target0, Type_t * ___typeofTarget1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = AnimationPlayable_RemoveAllInputs_m2002968067((&___target0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_node()
extern "C"  Playable_t3667545548  AnimatorControllerPlayable_get_node_m127621219 (AnimatorControllerPlayable_t4078305555 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		AnimationPlayable_t1693994278 * L_0 = __this->get_address_of_handle_0();
		Playable_t3667545548  L_1 = AnimationPlayable_get_node_m1103082298(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Playable_t3667545548  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Playable_t3667545548  AnimatorControllerPlayable_get_node_m127621219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorControllerPlayable_t4078305555 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4078305555 *>(__this + 1);
	return AnimatorControllerPlayable_get_node_m127621219(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.AnimatorControllerPlayable::op_Implicit(UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  Playable_t3667545548  AnimatorControllerPlayable_op_Implicit_m1305773959 (Il2CppObject * __this /* static, unused */, AnimatorControllerPlayable_t4078305555  ___s0, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = AnimatorControllerPlayable_get_node_m127621219((&___s0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Playable_t3667545548  L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::Equals(System.Object)
extern "C"  bool AnimatorControllerPlayable_Equals_m2027287369 (AnimatorControllerPlayable_t4078305555 * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548  L_0 = AnimatorControllerPlayable_op_Implicit_m1305773959(NULL /*static, unused*/, (*(AnimatorControllerPlayable_t4078305555 *)__this), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___p0;
		bool L_2 = Playables_Equals_m3918086298(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorControllerPlayable_Equals_m2027287369_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	AnimatorControllerPlayable_t4078305555 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4078305555 *>(__this + 1);
	return AnimatorControllerPlayable_Equals_m2027287369(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetHashCode()
extern "C"  int32_t AnimatorControllerPlayable_GetHashCode_m3511829943 (AnimatorControllerPlayable_t4078305555 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Playable_t3667545548  L_0 = AnimatorControllerPlayable_get_node_m127621219(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Playable_GetHashCode_m291506040((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t AnimatorControllerPlayable_GetHashCode_m3511829943_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorControllerPlayable_t4078305555 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t4078305555 *>(__this + 1);
	return AnimatorControllerPlayable_GetHashCode_m3511829943(_thisAdjusted, method);
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::.ctor()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1300558257;
extern Il2CppCodeGenString* _stringLiteral3023979642;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern const uint32_t CustomAnimationPlayable__ctor_m2580931540_MetadataUsageId;
extern "C"  void CustomAnimationPlayable__ctor_m2580931540 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomAnimationPlayable__ctor_m2580931540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ScriptPlayable__ctor_m1712674040(__this, /*hidden argument*/NULL);
		AnimationPlayable_t1693994278 * L_0 = __this->get_address_of_handle_0();
		bool L_1 = AnimationPlayable_IsValid_m1938227437(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0059;
		}
	}
	{
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_0 = L_3;
		StringU5BU5D_t1642385972* L_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		String_t* L_5 = V_0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1300558257);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1300558257);
		StringU5BU5D_t1642385972* L_7 = L_6;
		String_t* L_8 = V_0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_8);
		StringU5BU5D_t1642385972* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral3023979642);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3023979642);
		StringU5BU5D_t1642385972* L_10 = L_9;
		String_t* L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029316);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral372029316);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_14 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_14, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0059:
	{
		return;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.CustomAnimationPlayable::get_node()
extern "C"  Playable_t3667545548  CustomAnimationPlayable_get_node_m2279367825 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		AnimationPlayable_t1693994278  L_0 = __this->get_handle_0();
		Playable_t3667545548  L_1 = AnimationPlayable_op_Implicit_m408821639(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Playable_t3667545548  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::SetHandle(System.Int32,System.IntPtr)
extern "C"  void CustomAnimationPlayable_SetHandle_m1081373665 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___version0, IntPtr_t ___playableHandle1, const MethodInfo* method)
{
	{
		AnimationPlayable_t1693994278 * L_0 = __this->get_address_of_handle_0();
		Playable_t3667545548 * L_1 = L_0->get_address_of_handle_0();
		IntPtr_t L_2 = ___playableHandle1;
		L_1->set_m_Handle_0(L_2);
		AnimationPlayable_t1693994278 * L_3 = __this->get_address_of_handle_0();
		Playable_t3667545548 * L_4 = L_3->get_address_of_handle_0();
		int32_t L_5 = ___version0;
		L_4->set_m_Version_1(L_5);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::Destroy()
extern "C"  void CustomAnimationPlayable_Destroy_m4140105698 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_get_node_m2279367825(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Playable_Destroy_m3675363825((&V_0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.CustomAnimationPlayable::op_Implicit(UnityEngine.Experimental.Director.CustomAnimationPlayable)
extern Il2CppClass* Playable_t3667545548_il2cpp_TypeInfo_var;
extern const uint32_t CustomAnimationPlayable_op_Implicit_m2651548371_MetadataUsageId;
extern "C"  Playable_t3667545548  CustomAnimationPlayable_op_Implicit_m2651548371 (Il2CppObject * __this /* static, unused */, CustomAnimationPlayable_t3423099547 * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomAnimationPlayable_op_Implicit_m2651548371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t3667545548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Playable_t3667545548  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Playable_t3667545548  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Initobj (Playable_t3667545548_il2cpp_TypeInfo_var, (&V_0));
		CustomAnimationPlayable_t3423099547 * L_0 = ___s0;
		NullCheck(L_0);
		Playable_t3667545548  L_1 = CustomAnimationPlayable_get_node_m2279367825(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		IntPtr_t L_2 = (&V_1)->get_m_Handle_0();
		(&V_0)->set_m_Handle_0(L_2);
		CustomAnimationPlayable_t3423099547 * L_3 = ___s0;
		NullCheck(L_3);
		Playable_t3667545548  L_4 = CustomAnimationPlayable_get_node_m2279367825(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = (&V_2)->get_m_Version_1();
		(&V_0)->set_m_Version_1(L_5);
		Playable_t3667545548  L_6 = V_0;
		V_3 = L_6;
		goto IL_003a;
	}

IL_003a:
	{
		Playable_t3667545548  L_7 = V_3;
		return L_7;
	}
}
// UnityEngine.Experimental.Director.AnimationPlayable UnityEngine.Experimental.Director.CustomAnimationPlayable::op_Implicit(UnityEngine.Experimental.Director.CustomAnimationPlayable)
extern "C"  AnimationPlayable_t1693994278  CustomAnimationPlayable_op_Implicit_m3677069457 (Il2CppObject * __this /* static, unused */, CustomAnimationPlayable_t3423099547 * ___s0, const MethodInfo* method)
{
	AnimationPlayable_t1693994278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CustomAnimationPlayable_t3423099547 * L_0 = ___s0;
		NullCheck(L_0);
		AnimationPlayable_t1693994278  L_1 = L_0->get_handle_0();
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		AnimationPlayable_t1693994278  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::PrepareFrame(UnityEngine.Experimental.Director.FrameData)
extern "C"  void CustomAnimationPlayable_PrepareFrame_m919887762 (CustomAnimationPlayable_t3423099547 * __this, FrameData_t1120735295  ___info0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::OnSetTime(System.Single)
extern "C"  void CustomAnimationPlayable_OnSetTime_m462197633 (CustomAnimationPlayable_t3423099547 * __this, float ___localTime0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::OnSetPlayState(UnityEngine.Experimental.Director.PlayState)
extern "C"  void CustomAnimationPlayable_OnSetPlayState_m1903832042 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___newState0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 UnityEngine.Experimental.Director.CustomAnimationPlayable::get_inputCount()
extern "C"  int32_t CustomAnimationPlayable_get_inputCount_m1506970144 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		int32_t L_2 = Playables_GetInputCountValidated_m163517061(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.CustomAnimationPlayable::GetInput(System.Int32)
extern "C"  Playable_t3667545548  CustomAnimationPlayable_GetInput_m627183837 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___inputPort0, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = ___inputPort0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		Playable_t3667545548  L_3 = Playables_GetInputValidated_m2433189843(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		Playable_t3667545548  L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Experimental.Director.CustomAnimationPlayable::get_outputCount()
extern "C"  int32_t CustomAnimationPlayable_get_outputCount_m1726567977 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		int32_t L_2 = Playables_GetOutputCountValidated_m3428930850(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.CustomAnimationPlayable::GetOutput(System.Int32)
extern "C"  Playable_t3667545548  CustomAnimationPlayable_GetOutput_m2109551724 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___outputPort0, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = ___outputPort0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		Playable_t3667545548  L_3 = Playables_GetOutputValidated_m1886200224(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		Playable_t3667545548  L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Experimental.Director.CustomAnimationPlayable::GetInputWeight(System.Int32)
extern "C"  float CustomAnimationPlayable_GetInputWeight_m4080635817 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___index0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		float L_3 = Playables_GetInputWeightValidated_m2453142047(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::SetInputWeight(System.Int32,System.Single)
extern "C"  void CustomAnimationPlayable_SetInputWeight_m2832273536 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___inputIndex0, float ___weight1, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = ___inputIndex0;
		float L_2 = ___weight1;
		Type_t * L_3 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		Playables_SetInputWeightValidated_m3780238668(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Experimental.Director.PlayState UnityEngine.Experimental.Director.CustomAnimationPlayable::get_state()
extern "C"  int32_t CustomAnimationPlayable_get_state_m4168755227 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		int32_t L_2 = Playables_GetPlayStateValidated_m2756684446(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::set_state(UnityEngine.Experimental.Director.PlayState)
extern "C"  void CustomAnimationPlayable_set_state_m3118907084 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = ___value0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		Playables_SetPlayStateValidated_m770460097(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Experimental.Director.CustomAnimationPlayable::get_time()
extern "C"  double CustomAnimationPlayable_get_time_m2832957799 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		double L_2 = Playables_GetTimeValidated_m788360268(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		double L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::set_time(System.Double)
extern "C"  void CustomAnimationPlayable_set_time_m1063048916 (CustomAnimationPlayable_t3423099547 * __this, double ___value0, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		double L_1 = ___value0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		Playables_SetTimeValidated_m3858295249(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Experimental.Director.CustomAnimationPlayable::get_duration()
extern "C"  double CustomAnimationPlayable_get_duration_m347792240 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		double L_2 = Playables_GetDurationValidated_m4005203433(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		double L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.CustomAnimationPlayable::set_duration(System.Double)
extern "C"  void CustomAnimationPlayable_set_duration_m1049843503 (CustomAnimationPlayable_t3423099547 * __this, double ___value0, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = CustomAnimationPlayable_op_Implicit_m2651548371(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		double L_1 = ___value0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		Playables_SetDurationValidated_m1466172352(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Experimental.Director.CustomAnimationPlayable::AddInput(UnityEngine.Experimental.Director.Playable)
extern "C"  int32_t CustomAnimationPlayable_AddInput_m3120035390 (CustomAnimationPlayable_t3423099547 * __this, Playable_t3667545548  ___input0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AnimationPlayable_t1693994278  L_0 = CustomAnimationPlayable_op_Implicit_m3677069457(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Playable_t3667545548  L_1 = ___input0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		int32_t L_3 = AnimationPlayableUtilities_AddInputValidated_m3676524933(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Experimental.Director.CustomAnimationPlayable::SetInput(UnityEngine.Experimental.Director.Playable,System.Int32)
extern "C"  bool CustomAnimationPlayable_SetInput_m2588862758 (CustomAnimationPlayable_t3423099547 * __this, Playable_t3667545548  ___source0, int32_t ___index1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		AnimationPlayable_t1693994278  L_0 = CustomAnimationPlayable_op_Implicit_m3677069457(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Playable_t3667545548  L_1 = ___source0;
		int32_t L_2 = ___index1;
		Type_t * L_3 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		bool L_4 = AnimationPlayableUtilities_SetInputValidated_m2748997465(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Experimental.Director.CustomAnimationPlayable::SetInputs(System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.Director.Playable>)
extern "C"  bool CustomAnimationPlayable_SetInputs_m1393448423 (CustomAnimationPlayable_t3423099547 * __this, Il2CppObject* ___sources0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		AnimationPlayable_t1693994278  L_0 = CustomAnimationPlayable_op_Implicit_m3677069457(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___sources0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		bool L_3 = AnimationPlayableUtilities_SetInputsValidated_m2696856860(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Experimental.Director.CustomAnimationPlayable::RemoveInput(System.Int32)
extern "C"  bool CustomAnimationPlayable_RemoveInput_m238782147 (CustomAnimationPlayable_t3423099547 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		AnimationPlayable_t1693994278  L_0 = CustomAnimationPlayable_op_Implicit_m3677069457(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		Type_t * L_2 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		bool L_3 = AnimationPlayableUtilities_RemoveInputValidated_m438691908(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Experimental.Director.CustomAnimationPlayable::RemoveAllInputs()
extern "C"  bool CustomAnimationPlayable_RemoveAllInputs_m636888422 (CustomAnimationPlayable_t3423099547 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		AnimationPlayable_t1693994278  L_0 = CustomAnimationPlayable_op_Implicit_m3677069457(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		bool L_2 = AnimationPlayableUtilities_RemoveAllInputsValidated_m1936894983(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.GenericMixerPlayable::get_node()
extern "C"  Playable_t3667545548  GenericMixerPlayable_get_node_m1677789206 (GenericMixerPlayable_t788733994 * __this, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Playable_t3667545548  L_0 = __this->get_handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Playable_t3667545548  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Playable_t3667545548  GenericMixerPlayable_get_node_m1677789206_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GenericMixerPlayable_t788733994 * _thisAdjusted = reinterpret_cast<GenericMixerPlayable_t788733994 *>(__this + 1);
	return GenericMixerPlayable_get_node_m1677789206(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.GenericMixerPlayable UnityEngine.Experimental.Director.GenericMixerPlayable::Create()
extern Il2CppClass* GenericMixerPlayable_t788733994_il2cpp_TypeInfo_var;
extern const uint32_t GenericMixerPlayable_Create_m2816314293_MetadataUsageId;
extern "C"  GenericMixerPlayable_t788733994  GenericMixerPlayable_Create_m2816314293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericMixerPlayable_Create_m2816314293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GenericMixerPlayable_t788733994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GenericMixerPlayable_t788733994  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (GenericMixerPlayable_t788733994_il2cpp_TypeInfo_var, (&V_0));
		GenericMixerPlayable_InternalCreate_m559491731(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		GenericMixerPlayable_t788733994  L_0 = V_0;
		V_1 = L_0;
		goto IL_0017;
	}

IL_0017:
	{
		GenericMixerPlayable_t788733994  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Director.GenericMixerPlayable::InternalCreate(UnityEngine.Experimental.Director.GenericMixerPlayable&)
extern "C"  void GenericMixerPlayable_InternalCreate_m559491731 (Il2CppObject * __this /* static, unused */, GenericMixerPlayable_t788733994 * ___playable0, const MethodInfo* method)
{
	typedef void (*GenericMixerPlayable_InternalCreate_m559491731_ftn) (GenericMixerPlayable_t788733994 *);
	static GenericMixerPlayable_InternalCreate_m559491731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GenericMixerPlayable_InternalCreate_m559491731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.GenericMixerPlayable::InternalCreate(UnityEngine.Experimental.Director.GenericMixerPlayable&)");
	_il2cpp_icall_func(___playable0);
}
// System.Void UnityEngine.Experimental.Director.GenericMixerPlayable::Destroy()
extern "C"  void GenericMixerPlayable_Destroy_m3608330239 (GenericMixerPlayable_t788733994 * __this, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = __this->get_address_of_handle_0();
		Playable_Destroy_m3675363825(L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void GenericMixerPlayable_Destroy_m3608330239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GenericMixerPlayable_t788733994 * _thisAdjusted = reinterpret_cast<GenericMixerPlayable_t788733994 *>(__this + 1);
	GenericMixerPlayable_Destroy_m3608330239(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.GenericMixerPlayable::op_Implicit(UnityEngine.Experimental.Director.GenericMixerPlayable)
extern Il2CppClass* Playable_t3667545548_il2cpp_TypeInfo_var;
extern const uint32_t GenericMixerPlayable_op_Implicit_m4171147111_MetadataUsageId;
extern "C"  Playable_t3667545548  GenericMixerPlayable_op_Implicit_m4171147111 (Il2CppObject * __this /* static, unused */, GenericMixerPlayable_t788733994  ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericMixerPlayable_op_Implicit_m4171147111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t3667545548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (Playable_t3667545548_il2cpp_TypeInfo_var, (&V_0));
		Playable_t3667545548 * L_0 = (&___s0)->get_address_of_handle_0();
		IntPtr_t L_1 = L_0->get_m_Handle_0();
		(&V_0)->set_m_Handle_0(L_1);
		Playable_t3667545548 * L_2 = (&___s0)->get_address_of_handle_0();
		int32_t L_3 = L_2->get_m_Version_1();
		(&V_0)->set_m_Version_1(L_3);
		Playable_t3667545548  L_4 = V_0;
		V_1 = L_4;
		goto IL_0036;
	}

IL_0036:
	{
		Playable_t3667545548  L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::Destroy()
extern "C"  void Playable_Destroy_m3675363825 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	{
		Playables_InternalDestroy_m3873600434(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Playable_Destroy_m3675363825_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	Playable_Destroy_m3675363825(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::IsValid()
extern "C"  bool Playable_IsValid_m2102916683 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = Playable_IsValidInternal_m2681498367(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool Playable_IsValid_m2102916683_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_IsValid_m2102916683(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::IsValidInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  bool Playable_IsValidInternal_m2681498367 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		bool L_1 = Playable_INTERNAL_CALL_IsValidInternal_m3509544226(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_IsValidInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  bool Playable_INTERNAL_CALL_IsValidInternal_m3509544226 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef bool (*Playable_INTERNAL_CALL_IsValidInternal_m3509544226_ftn) (Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_IsValidInternal_m3509544226_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_IsValidInternal_m3509544226_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_IsValidInternal(UnityEngine.Experimental.Director.Playable&)");
	return _il2cpp_icall_func(___playable0);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::Connect(UnityEngine.Experimental.Director.Playable,UnityEngine.Experimental.Director.Playable,System.Int32,System.Int32)
extern "C"  bool Playable_Connect_m1305428597 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___source0, Playable_t3667545548  ___target1, int32_t ___sourceOutputPort2, int32_t ___targetInputPort3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___sourceOutputPort2;
		int32_t L_1 = ___targetInputPort3;
		bool L_2 = Playables_ConnectInternal_m2792280857(NULL /*static, unused*/, (&___source0), (&___target1), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::Disconnect(UnityEngine.Experimental.Director.Playable,System.Int32)
extern "C"  void Playable_Disconnect_m2489180239 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___target0, int32_t ___inputPort1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___inputPort1;
		bool L_1 = Playable_CheckInputBounds_m1458023351((&___target0), L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		goto IL_001b;
	}

IL_0013:
	{
		int32_t L_2 = ___inputPort1;
		Playables_DisconnectInternal_m21696045(NULL /*static, unused*/, (&___target0), L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Int32 UnityEngine.Experimental.Director.Playable::get_inputCount()
extern "C"  int32_t Playable_get_inputCount_m1751152777 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Playable_GetInputCountInternal_m3354921878(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Playable_get_inputCount_m1751152777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_get_inputCount_m1751152777(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Experimental.Director.Playable::get_outputCount()
extern "C"  int32_t Playable_get_outputCount_m4289390470 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Playable_GetOutputCountInternal_m354236047(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Playable_get_outputCount_m4289390470_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_get_outputCount_m4289390470(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.PlayState UnityEngine.Experimental.Director.Playable::get_state()
extern "C"  int32_t Playable_get_state_m2895939980 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Playable_GetPlayStateInternal_m1333214605(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Playable_get_state_m2895939980_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_get_state_m2895939980(_thisAdjusted, method);
}
// System.Void UnityEngine.Experimental.Director.Playable::set_state(UnityEngine.Experimental.Director.PlayState)
extern "C"  void Playable_set_state_m628503053 (Playable_t3667545548 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Playable_SetPlayStateInternal_m2982991950(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Playable_set_state_m628503053_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	Playable_set_state_m628503053(_thisAdjusted, ___value0, method);
}
// System.Double UnityEngine.Experimental.Director.Playable::get_time()
extern "C"  double Playable_get_time_m188535056 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = Playable_GetTimeInternal_m4193977093(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		double L_1 = V_0;
		return L_1;
	}
}
extern "C"  double Playable_get_time_m188535056_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_get_time_m188535056(_thisAdjusted, method);
}
// System.Void UnityEngine.Experimental.Director.Playable::set_time(System.Double)
extern "C"  void Playable_set_time_m1337741429 (Playable_t3667545548 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		Playable_SetTimeInternal_m1923258884(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Playable_set_time_m1337741429_AdjustorThunk (Il2CppObject * __this, double ___value0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	Playable_set_time_m1337741429(_thisAdjusted, ___value0, method);
}
// UnityEngine.Experimental.Director.PlayState UnityEngine.Experimental.Director.Playable::GetPlayStateInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  int32_t Playable_GetPlayStateInternal_m1333214605 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = Playable_INTERNAL_CALL_GetPlayStateInternal_m3560305748(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Experimental.Director.PlayState UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetPlayStateInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  int32_t Playable_INTERNAL_CALL_GetPlayStateInternal_m3560305748 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef int32_t (*Playable_INTERNAL_CALL_GetPlayStateInternal_m3560305748_ftn) (Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetPlayStateInternal_m3560305748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetPlayStateInternal_m3560305748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetPlayStateInternal(UnityEngine.Experimental.Director.Playable&)");
	return _il2cpp_icall_func(___playable0);
}
// System.Void UnityEngine.Experimental.Director.Playable::SetPlayStateInternal(UnityEngine.Experimental.Director.Playable&,UnityEngine.Experimental.Director.PlayState)
extern "C"  void Playable_SetPlayStateInternal_m2982991950 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___playState1, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = ___playState1;
		Playable_INTERNAL_CALL_SetPlayStateInternal_m1141121823(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetPlayStateInternal(UnityEngine.Experimental.Director.Playable&,UnityEngine.Experimental.Director.PlayState)
extern "C"  void Playable_INTERNAL_CALL_SetPlayStateInternal_m1141121823 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___playState1, const MethodInfo* method)
{
	typedef void (*Playable_INTERNAL_CALL_SetPlayStateInternal_m1141121823_ftn) (Playable_t3667545548 *, int32_t);
	static Playable_INTERNAL_CALL_SetPlayStateInternal_m1141121823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_SetPlayStateInternal_m1141121823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetPlayStateInternal(UnityEngine.Experimental.Director.Playable&,UnityEngine.Experimental.Director.PlayState)");
	_il2cpp_icall_func(___playable0, ___playState1);
}
// System.Double UnityEngine.Experimental.Director.Playable::GetTimeInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  double Playable_GetTimeInternal_m4193977093 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		double L_1 = Playable_INTERNAL_CALL_GetTimeInternal_m1561742350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		double L_2 = V_0;
		return L_2;
	}
}
// System.Double UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetTimeInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  double Playable_INTERNAL_CALL_GetTimeInternal_m1561742350 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef double (*Playable_INTERNAL_CALL_GetTimeInternal_m1561742350_ftn) (Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetTimeInternal_m1561742350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetTimeInternal_m1561742350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetTimeInternal(UnityEngine.Experimental.Director.Playable&)");
	return _il2cpp_icall_func(___playable0);
}
// System.Void UnityEngine.Experimental.Director.Playable::SetTimeInternal(UnityEngine.Experimental.Director.Playable&,System.Double)
extern "C"  void Playable_SetTimeInternal_m1923258884 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, double ___time1, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = ___playable0;
		double L_1 = ___time1;
		Playable_INTERNAL_CALL_SetTimeInternal_m2302691195(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetTimeInternal(UnityEngine.Experimental.Director.Playable&,System.Double)
extern "C"  void Playable_INTERNAL_CALL_SetTimeInternal_m2302691195 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, double ___time1, const MethodInfo* method)
{
	typedef void (*Playable_INTERNAL_CALL_SetTimeInternal_m2302691195_ftn) (Playable_t3667545548 *, double);
	static Playable_INTERNAL_CALL_SetTimeInternal_m2302691195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_SetTimeInternal_m2302691195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetTimeInternal(UnityEngine.Experimental.Director.Playable&,System.Double)");
	_il2cpp_icall_func(___playable0, ___time1);
}
// System.Double UnityEngine.Experimental.Director.Playable::get_duration()
extern "C"  double Playable_get_duration_m4155589845 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = Playable_GetDurationInternal_m3180992642(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		double L_1 = V_0;
		return L_1;
	}
}
extern "C"  double Playable_get_duration_m4155589845_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_get_duration_m4155589845(_thisAdjusted, method);
}
// System.Void UnityEngine.Experimental.Director.Playable::set_duration(System.Double)
extern "C"  void Playable_set_duration_m3772907818 (Playable_t3667545548 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		Playable_SetDurationInternal_m3664705881(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Playable_set_duration_m3772907818_AdjustorThunk (Il2CppObject * __this, double ___value0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	Playable_set_duration_m3772907818(_thisAdjusted, ___value0, method);
}
// System.Double UnityEngine.Experimental.Director.Playable::GetDurationInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  double Playable_GetDurationInternal_m3180992642 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		double L_1 = Playable_INTERNAL_CALL_GetDurationInternal_m2061318387(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		double L_2 = V_0;
		return L_2;
	}
}
// System.Double UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetDurationInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  double Playable_INTERNAL_CALL_GetDurationInternal_m2061318387 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef double (*Playable_INTERNAL_CALL_GetDurationInternal_m2061318387_ftn) (Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetDurationInternal_m2061318387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetDurationInternal_m2061318387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetDurationInternal(UnityEngine.Experimental.Director.Playable&)");
	return _il2cpp_icall_func(___playable0);
}
// System.Void UnityEngine.Experimental.Director.Playable::SetDurationInternal(UnityEngine.Experimental.Director.Playable&,System.Double)
extern "C"  void Playable_SetDurationInternal_m3664705881 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, double ___duration1, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = ___playable0;
		double L_1 = ___duration1;
		Playable_INTERNAL_CALL_SetDurationInternal_m1052618056(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetDurationInternal(UnityEngine.Experimental.Director.Playable&,System.Double)
extern "C"  void Playable_INTERNAL_CALL_SetDurationInternal_m1052618056 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, double ___duration1, const MethodInfo* method)
{
	typedef void (*Playable_INTERNAL_CALL_SetDurationInternal_m1052618056_ftn) (Playable_t3667545548 *, double);
	static Playable_INTERNAL_CALL_SetDurationInternal_m1052618056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_SetDurationInternal_m1052618056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetDurationInternal(UnityEngine.Experimental.Director.Playable&,System.Double)");
	_il2cpp_icall_func(___playable0, ___duration1);
}
// System.Int32 UnityEngine.Experimental.Director.Playable::GetInputCountInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  int32_t Playable_GetInputCountInternal_m3354921878 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = Playable_INTERNAL_CALL_GetInputCountInternal_m2921284003(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetInputCountInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  int32_t Playable_INTERNAL_CALL_GetInputCountInternal_m2921284003 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef int32_t (*Playable_INTERNAL_CALL_GetInputCountInternal_m2921284003_ftn) (Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetInputCountInternal_m2921284003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetInputCountInternal_m2921284003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetInputCountInternal(UnityEngine.Experimental.Director.Playable&)");
	return _il2cpp_icall_func(___playable0);
}
// System.Int32 UnityEngine.Experimental.Director.Playable::GetOutputCountInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  int32_t Playable_GetOutputCountInternal_m354236047 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = Playable_INTERNAL_CALL_GetOutputCountInternal_m224211058(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetOutputCountInternal(UnityEngine.Experimental.Director.Playable&)
extern "C"  int32_t Playable_INTERNAL_CALL_GetOutputCountInternal_m224211058 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef int32_t (*Playable_INTERNAL_CALL_GetOutputCountInternal_m224211058_ftn) (Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetOutputCountInternal_m224211058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetOutputCountInternal_m224211058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetOutputCountInternal(UnityEngine.Experimental.Director.Playable&)");
	return _il2cpp_icall_func(___playable0);
}
// UnityEngine.Experimental.Director.Playable[] UnityEngine.Experimental.Director.Playable::GetInputs()
extern Il2CppClass* List_1_t3036666680_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3602334893_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3878686313_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1197439731_MethodInfo_var;
extern const uint32_t Playable_GetInputs_m2701651284_MetadataUsageId;
extern "C"  PlayableU5BU5D_t4034110853* Playable_GetInputs_m2701651284 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_GetInputs_m2701651284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3036666680 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	PlayableU5BU5D_t4034110853* V_3 = NULL;
	{
		List_1_t3036666680 * L_0 = (List_1_t3036666680 *)il2cpp_codegen_object_new(List_1_t3036666680_il2cpp_TypeInfo_var);
		List_1__ctor_m3602334893(L_0, /*hidden argument*/List_1__ctor_m3602334893_MethodInfo_var);
		V_0 = L_0;
		int32_t L_1 = Playable_get_inputCount_m1751152777(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		List_1_t3036666680 * L_2 = V_0;
		int32_t L_3 = V_2;
		Playable_t3667545548  L_4 = Playable_GetInput_m2343344472(__this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Add_m3878686313(L_2, L_4, /*hidden argument*/List_1_Add_m3878686313_MethodInfo_var);
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0015;
		}
	}
	{
		List_1_t3036666680 * L_8 = V_0;
		NullCheck(L_8);
		PlayableU5BU5D_t4034110853* L_9 = List_1_ToArray_m1197439731(L_8, /*hidden argument*/List_1_ToArray_m1197439731_MethodInfo_var);
		V_3 = L_9;
		goto IL_003b;
	}

IL_003b:
	{
		PlayableU5BU5D_t4034110853* L_10 = V_3;
		return L_10;
	}
}
extern "C"  PlayableU5BU5D_t4034110853* Playable_GetInputs_m2701651284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_GetInputs_m2701651284(_thisAdjusted, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playable::GetInput(System.Int32)
extern "C"  Playable_t3667545548  Playable_GetInput_m2343344472 (Playable_t3667545548 * __this, int32_t ___inputPort0, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___inputPort0;
		Playable_t3667545548  L_1 = Playable_GetInputInternal_m2831827086(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Playable_t3667545548  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Playable_t3667545548  Playable_GetInput_m2343344472_AdjustorThunk (Il2CppObject * __this, int32_t ___inputPort0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_GetInput_m2343344472(_thisAdjusted, ___inputPort0, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playable::GetInputInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)
extern "C"  Playable_t3667545548  Playable_GetInputInternal_m2831827086 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t3667545548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = ___index1;
		Playable_INTERNAL_CALL_GetInputInternal_m1720243548(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Playable_t3667545548  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Playable_t3667545548  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetInputInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,UnityEngine.Experimental.Director.Playable&)
extern "C"  void Playable_INTERNAL_CALL_GetInputInternal_m1720243548 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, Playable_t3667545548 * ___value2, const MethodInfo* method)
{
	typedef void (*Playable_INTERNAL_CALL_GetInputInternal_m1720243548_ftn) (Playable_t3667545548 *, int32_t, Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetInputInternal_m1720243548_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetInputInternal_m1720243548_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetInputInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,UnityEngine.Experimental.Director.Playable&)");
	_il2cpp_icall_func(___playable0, ___index1, ___value2);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playable::GetOutput(System.Int32)
extern "C"  Playable_t3667545548  Playable_GetOutput_m1530310341 (Playable_t3667545548 * __this, int32_t ___outputPort0, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___outputPort0;
		Playable_t3667545548  L_1 = Playable_GetOutputInternal_m1788163321(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Playable_t3667545548  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Playable_t3667545548  Playable_GetOutput_m1530310341_AdjustorThunk (Il2CppObject * __this, int32_t ___outputPort0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_GetOutput_m1530310341(_thisAdjusted, ___outputPort0, method);
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playable::GetOutputInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)
extern "C"  Playable_t3667545548  Playable_GetOutputInternal_m1788163321 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t3667545548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = ___index1;
		Playable_INTERNAL_CALL_GetOutputInternal_m1864790245(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Playable_t3667545548  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Playable_t3667545548  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetOutputInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,UnityEngine.Experimental.Director.Playable&)
extern "C"  void Playable_INTERNAL_CALL_GetOutputInternal_m1864790245 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, Playable_t3667545548 * ___value2, const MethodInfo* method)
{
	typedef void (*Playable_INTERNAL_CALL_GetOutputInternal_m1864790245_ftn) (Playable_t3667545548 *, int32_t, Playable_t3667545548 *);
	static Playable_INTERNAL_CALL_GetOutputInternal_m1864790245_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetOutputInternal_m1864790245_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetOutputInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,UnityEngine.Experimental.Director.Playable&)");
	_il2cpp_icall_func(___playable0, ___index1, ___value2);
}
// System.Void UnityEngine.Experimental.Director.Playable::SetInputWeightFromIndexInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,System.Single)
extern "C"  void Playable_SetInputWeightFromIndexInternal_m1264185407 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, float ___weight2, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = ___index1;
		float L_2 = ___weight2;
		Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal_m2039337128(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetInputWeightFromIndexInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,System.Single)
extern "C"  void Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal_m2039337128 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, float ___weight2, const MethodInfo* method)
{
	typedef void (*Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal_m2039337128_ftn) (Playable_t3667545548 *, int32_t, float);
	static Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal_m2039337128_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal_m2039337128_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_SetInputWeightFromIndexInternal(UnityEngine.Experimental.Director.Playable&,System.Int32,System.Single)");
	_il2cpp_icall_func(___playable0, ___index1, ___weight2);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::SetInputWeight(System.Int32,System.Single)
extern "C"  bool Playable_SetInputWeight_m3424075019 (Playable_t3667545548 * __this, int32_t ___inputIndex0, float ___weight1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___inputIndex0;
		bool L_1 = Playable_CheckInputBounds_m1458023351(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = ___inputIndex0;
		float L_3 = ___weight1;
		Playable_SetInputWeightFromIndexInternal_m1264185407(NULL /*static, unused*/, __this, L_2, L_3, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0024;
	}

IL_001d:
	{
		V_0 = (bool)0;
		goto IL_0024;
	}

IL_0024:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Playable_SetInputWeight_m3424075019_AdjustorThunk (Il2CppObject * __this, int32_t ___inputIndex0, float ___weight1, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_SetInputWeight_m3424075019(_thisAdjusted, ___inputIndex0, ___weight1, method);
}
// System.Single UnityEngine.Experimental.Director.Playable::GetInputWeight(System.Int32)
extern "C"  float Playable_GetInputWeight_m745267058 (Playable_t3667545548 * __this, int32_t ___index0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		float L_1 = Playable_GetInputWeightInternal_m381407588(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Playable_GetInputWeight_m745267058_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_GetInputWeight_m745267058(_thisAdjusted, ___index0, method);
}
// System.Single UnityEngine.Experimental.Director.Playable::GetInputWeightInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)
extern "C"  float Playable_GetInputWeightInternal_m381407588 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Playable_t3667545548 * L_0 = ___playable0;
		int32_t L_1 = ___index1;
		float L_2 = Playable_INTERNAL_CALL_GetInputWeightInternal_m3329579253(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetInputWeightInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)
extern "C"  float Playable_INTERNAL_CALL_GetInputWeightInternal_m3329579253 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, int32_t ___index1, const MethodInfo* method)
{
	typedef float (*Playable_INTERNAL_CALL_GetInputWeightInternal_m3329579253_ftn) (Playable_t3667545548 *, int32_t);
	static Playable_INTERNAL_CALL_GetInputWeightInternal_m3329579253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playable_INTERNAL_CALL_GetInputWeightInternal_m3329579253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playable::INTERNAL_CALL_GetInputWeightInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)");
	return _il2cpp_icall_func(___playable0, ___index1);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::Equals(System.Object)
extern "C"  bool Playable_Equals_m3780922516 (Playable_t3667545548 * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___p0;
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = ___p0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		int32_t L_3 = Playable_GetHashCode_m291506040(__this, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0024;
	}

IL_0024:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Playable_Equals_m3780922516_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_Equals_m3780922516(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Experimental.Director.Playable::GetHashCode()
extern "C"  int32_t Playable_GetHashCode_m291506040 (Playable_t3667545548 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_Handle_0();
		int32_t L_1 = IntPtr_op_Explicit_m1458664696(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Version_1();
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_2));
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
extern "C"  int32_t Playable_GetHashCode_m291506040_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_GetHashCode_m291506040(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::CheckInputBounds(System.Int32)
extern "C"  bool Playable_CheckInputBounds_m1458023351 (Playable_t3667545548 * __this, int32_t ___inputIndex0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___inputIndex0;
		bool L_1 = Playable_CheckInputBounds_m1095657528(__this, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool Playable_CheckInputBounds_m1458023351_AdjustorThunk (Il2CppObject * __this, int32_t ___inputIndex0, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_CheckInputBounds_m1458023351(_thisAdjusted, ___inputIndex0, method);
}
// System.Boolean UnityEngine.Experimental.Director.Playable::CheckInputBounds(System.Int32,System.Boolean)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3559423067;
extern Il2CppCodeGenString* _stringLiteral2960325978;
extern Il2CppCodeGenString* _stringLiteral2167948046;
extern Il2CppCodeGenString* _stringLiteral1617874531;
extern const uint32_t Playable_CheckInputBounds_m1095657528_MetadataUsageId;
extern "C"  bool Playable_CheckInputBounds_m1095657528 (Playable_t3667545548 * __this, int32_t ___inputIndex0, bool ___acceptAny1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_CheckInputBounds_m1095657528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	PlayableU5BU5D_t4034110853* V_1 = NULL;
	{
		int32_t L_0 = ___inputIndex0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = ___acceptAny1;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_007d;
	}

IL_0015:
	{
		int32_t L_2 = ___inputIndex0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		IndexOutOfRangeException_t3527622107 * L_3 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_3, _stringLiteral3559423067, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		PlayableU5BU5D_t4034110853* L_4 = Playable_GetInputs_m2701651284(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		PlayableU5BU5D_t4034110853* L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = ___inputIndex0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) > ((int32_t)L_6)))
		{
			goto IL_0076;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2960325978);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2960325978);
		ObjectU5BU5D_t3614634134* L_8 = L_7;
		int32_t L_9 = ___inputIndex0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral2167948046);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2167948046);
		ObjectU5BU5D_t3614634134* L_13 = L_12;
		PlayableU5BU5D_t4034110853* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = (((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))));
		Il2CppObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = L_13;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral1617874531);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1617874531);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3881798623(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3527622107 * L_19 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_19, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0076:
	{
		V_0 = (bool)1;
		goto IL_007d;
	}

IL_007d:
	{
		bool L_20 = V_0;
		return L_20;
	}
}
extern "C"  bool Playable_CheckInputBounds_m1095657528_AdjustorThunk (Il2CppObject * __this, int32_t ___inputIndex0, bool ___acceptAny1, const MethodInfo* method)
{
	Playable_t3667545548 * _thisAdjusted = reinterpret_cast<Playable_t3667545548 *>(__this + 1);
	return Playable_CheckInputBounds_m1095657528(_thisAdjusted, ___inputIndex0, ___acceptAny1, method);
}
// System.Object UnityEngine.Experimental.Director.Playables::CastToInternal(System.Type,System.IntPtr,System.Int32)
extern "C"  Il2CppObject * Playables_CastToInternal_m2786985142 (Il2CppObject * __this /* static, unused */, Type_t * ___castType0, IntPtr_t ___handle1, int32_t ___version2, const MethodInfo* method)
{
	typedef Il2CppObject * (*Playables_CastToInternal_m2786985142_ftn) (Type_t *, IntPtr_t, int32_t);
	static Playables_CastToInternal_m2786985142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playables_CastToInternal_m2786985142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playables::CastToInternal(System.Type,System.IntPtr,System.Int32)");
	return _il2cpp_icall_func(___castType0, ___handle1, ___version2);
}
// System.Void UnityEngine.Experimental.Director.Playables::InternalDestroy(UnityEngine.Experimental.Director.Playable&)
extern "C"  void Playables_InternalDestroy_m3873600434 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = ___playable0;
		Playables_INTERNAL_CALL_InternalDestroy_m763661895(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.Playables::INTERNAL_CALL_InternalDestroy(UnityEngine.Experimental.Director.Playable&)
extern "C"  void Playables_INTERNAL_CALL_InternalDestroy_m763661895 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___playable0, const MethodInfo* method)
{
	typedef void (*Playables_INTERNAL_CALL_InternalDestroy_m763661895_ftn) (Playable_t3667545548 *);
	static Playables_INTERNAL_CALL_InternalDestroy_m763661895_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playables_INTERNAL_CALL_InternalDestroy_m763661895_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playables::INTERNAL_CALL_InternalDestroy(UnityEngine.Experimental.Director.Playable&)");
	_il2cpp_icall_func(___playable0);
}
// System.Boolean UnityEngine.Experimental.Director.Playables::ConnectInternal(UnityEngine.Experimental.Director.Playable&,UnityEngine.Experimental.Director.Playable&,System.Int32,System.Int32)
extern "C"  bool Playables_ConnectInternal_m2792280857 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___source0, Playable_t3667545548 * ___target1, int32_t ___sourceOutputPort2, int32_t ___targetInputPort3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Playable_t3667545548 * L_0 = ___source0;
		Playable_t3667545548 * L_1 = ___target1;
		int32_t L_2 = ___sourceOutputPort2;
		int32_t L_3 = ___targetInputPort3;
		bool L_4 = Playables_INTERNAL_CALL_ConnectInternal_m3834915264(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Experimental.Director.Playables::INTERNAL_CALL_ConnectInternal(UnityEngine.Experimental.Director.Playable&,UnityEngine.Experimental.Director.Playable&,System.Int32,System.Int32)
extern "C"  bool Playables_INTERNAL_CALL_ConnectInternal_m3834915264 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___source0, Playable_t3667545548 * ___target1, int32_t ___sourceOutputPort2, int32_t ___targetInputPort3, const MethodInfo* method)
{
	typedef bool (*Playables_INTERNAL_CALL_ConnectInternal_m3834915264_ftn) (Playable_t3667545548 *, Playable_t3667545548 *, int32_t, int32_t);
	static Playables_INTERNAL_CALL_ConnectInternal_m3834915264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playables_INTERNAL_CALL_ConnectInternal_m3834915264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playables::INTERNAL_CALL_ConnectInternal(UnityEngine.Experimental.Director.Playable&,UnityEngine.Experimental.Director.Playable&,System.Int32,System.Int32)");
	return _il2cpp_icall_func(___source0, ___target1, ___sourceOutputPort2, ___targetInputPort3);
}
// System.Void UnityEngine.Experimental.Director.Playables::DisconnectInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)
extern "C"  void Playables_DisconnectInternal_m21696045 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___target0, int32_t ___inputPort1, const MethodInfo* method)
{
	{
		Playable_t3667545548 * L_0 = ___target0;
		int32_t L_1 = ___inputPort1;
		Playables_INTERNAL_CALL_DisconnectInternal_m2526207778(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.Playables::INTERNAL_CALL_DisconnectInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)
extern "C"  void Playables_INTERNAL_CALL_DisconnectInternal_m2526207778 (Il2CppObject * __this /* static, unused */, Playable_t3667545548 * ___target0, int32_t ___inputPort1, const MethodInfo* method)
{
	typedef void (*Playables_INTERNAL_CALL_DisconnectInternal_m2526207778_ftn) (Playable_t3667545548 *, int32_t);
	static Playables_INTERNAL_CALL_DisconnectInternal_m2526207778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Playables_INTERNAL_CALL_DisconnectInternal_m2526207778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.Playables::INTERNAL_CALL_DisconnectInternal(UnityEngine.Experimental.Director.Playable&,System.Int32)");
	_il2cpp_icall_func(___target0, ___inputPort1);
}
// System.Boolean UnityEngine.Experimental.Director.Playables::CheckInputBounds(UnityEngine.Experimental.Director.Playable,System.Int32)
extern "C"  bool Playables_CheckInputBounds_m2953342617 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, int32_t ___inputIndex1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___inputIndex1;
		bool L_1 = Playable_CheckInputBounds_m1458023351((&___playable0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.Playables::IsValid(UnityEngine.Experimental.Director.Playable)
extern "C"  bool Playables_IsValid_m3689585029 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = Playable_IsValid_m2102916683((&___playable0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Experimental.Director.Playables::GetInputCountValidated(UnityEngine.Experimental.Director.Playable,System.Type)
extern "C"  int32_t Playables_GetInputCountValidated_m163517061 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, Type_t * ___typeofPlayable1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Playable_get_inputCount_m1751152777((&___playable0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Experimental.Director.Playables::GetOutputCountValidated(UnityEngine.Experimental.Director.Playable,System.Type)
extern "C"  int32_t Playables_GetOutputCountValidated_m3428930850 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, Type_t * ___typeofPlayable1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Playable_get_outputCount_m4289390470((&___playable0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Experimental.Director.PlayState UnityEngine.Experimental.Director.Playables::GetPlayStateValidated(UnityEngine.Experimental.Director.Playable,System.Type)
extern "C"  int32_t Playables_GetPlayStateValidated_m2756684446 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, Type_t * ___typeofPlayable1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Playable_get_state_m2895939980((&___playable0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Director.Playables::SetPlayStateValidated(UnityEngine.Experimental.Director.Playable,UnityEngine.Experimental.Director.PlayState,System.Type)
extern "C"  void Playables_SetPlayStateValidated_m770460097 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, int32_t ___playState1, Type_t * ___typeofPlayable2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___playState1;
		Playable_set_state_m628503053((&___playable0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Experimental.Director.Playables::GetTimeValidated(UnityEngine.Experimental.Director.Playable,System.Type)
extern "C"  double Playables_GetTimeValidated_m788360268 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, Type_t * ___typeofPlayable1, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = Playable_get_time_m188535056((&___playable0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		double L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Director.Playables::SetTimeValidated(UnityEngine.Experimental.Director.Playable,System.Double,System.Type)
extern "C"  void Playables_SetTimeValidated_m3858295249 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, double ___time1, Type_t * ___typeofPlayable2, const MethodInfo* method)
{
	{
		double L_0 = ___time1;
		Playable_set_time_m1337741429((&___playable0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Experimental.Director.Playables::GetDurationValidated(UnityEngine.Experimental.Director.Playable,System.Type)
extern "C"  double Playables_GetDurationValidated_m4005203433 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, Type_t * ___typeofPlayable1, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = Playable_get_duration_m4155589845((&___playable0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		double L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Director.Playables::SetDurationValidated(UnityEngine.Experimental.Director.Playable,System.Double,System.Type)
extern "C"  void Playables_SetDurationValidated_m1466172352 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, double ___duration1, Type_t * ___typeofPlayable2, const MethodInfo* method)
{
	{
		double L_0 = ___duration1;
		Playable_set_duration_m3772907818((&___playable0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playables::GetInputValidated(UnityEngine.Experimental.Director.Playable,System.Int32,System.Type)
extern "C"  Playable_t3667545548  Playables_GetInputValidated_m2433189843 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, int32_t ___inputPort1, Type_t * ___typeofPlayable2, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___inputPort1;
		Playable_t3667545548  L_1 = Playable_GetInput_m2343344472((&___playable0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Playable_t3667545548  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playables::GetOutputValidated(UnityEngine.Experimental.Director.Playable,System.Int32,System.Type)
extern "C"  Playable_t3667545548  Playables_GetOutputValidated_m1886200224 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, int32_t ___outputPort1, Type_t * ___typeofPlayable2, const MethodInfo* method)
{
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___outputPort1;
		Playable_t3667545548  L_1 = Playable_GetOutput_m1530310341((&___playable0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Playable_t3667545548  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Experimental.Director.Playables::SetInputWeightValidated(UnityEngine.Experimental.Director.Playable,System.Int32,System.Single,System.Type)
extern "C"  void Playables_SetInputWeightValidated_m3780238668 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, int32_t ___inputIndex1, float ___weight2, Type_t * ___typeofPlayable3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___inputIndex1;
		float L_1 = ___weight2;
		Playable_SetInputWeight_m3424075019((&___playable0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Experimental.Director.Playables::GetInputWeightValidated(UnityEngine.Experimental.Director.Playable,System.Int32,System.Type)
extern "C"  float Playables_GetInputWeightValidated_m2453142047 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___playable0, int32_t ___index1, Type_t * ___typeofPlayable2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index1;
		float L_1 = Playable_GetInputWeight_m745267058((&___playable0), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.Playables::Equals(UnityEngine.Experimental.Director.Playable,System.Object)
extern "C"  bool Playables_Equals_m3918086298 (Il2CppObject * __this /* static, unused */, Playable_t3667545548  ___isAPlayable0, Il2CppObject * ___mightBeAnythingOrNull1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___mightBeAnythingOrNull1;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_1 = ___mightBeAnythingOrNull1;
		bool L_2 = Playable_Equals_m3780922516((&___isAPlayable0), L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		V_0 = (bool)G_B3_0;
		goto IL_001e;
	}

IL_001e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Experimental.Director.ScriptPlayable::.ctor()
extern "C"  void ScriptPlayable__ctor_m1712674040 (ScriptPlayable_t4067966717 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t3819376471 * GameObject_GetComponent_m306258075 (GameObject_t1756533147 * __this, Type_t * ___type0, const MethodInfo* method)
{
	typedef Component_t3819376471 * (*GameObject_GetComponent_m306258075_ftn) (GameObject_t1756533147 *, Type_t *);
	static GameObject_GetComponent_m306258075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m306258075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type0);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t3819376471 * GameObject_GetComponentInChildren_m4263325740 (GameObject_t1756533147 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method)
{
	typedef Component_t3819376471 * (*GameObject_GetComponentInChildren_m4263325740_ftn) (GameObject_t1756533147 *, Type_t *, bool);
	static GameObject_GetComponentInChildren_m4263325740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInChildren_m4263325740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)");
	return _il2cpp_icall_func(__this, ___type0, ___includeInactive1);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t3819376471 * GameObject_GetComponentInParent_m1235194528 (GameObject_t1756533147 * __this, Type_t * ___type0, const MethodInfo* method)
{
	typedef Component_t3819376471 * (*GameObject_GetComponentInParent_m1235194528_ftn) (GameObject_t1756533147 *, Type_t *);
	static GameObject_GetComponentInParent_m1235194528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInParent_m1235194528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInParent(System.Type)");
	return _il2cpp_icall_func(__this, ___type0);
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponents(System.Type)
extern Il2CppClass* ComponentU5BU5D_t4136971630_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_m297658252_MetadataUsageId;
extern "C"  ComponentU5BU5D_t4136971630* GameObject_GetComponents_m297658252 (GameObject_t1756533147 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_m297658252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ComponentU5BU5D_t4136971630* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m3486524399(__this, L_0, (bool)0, (bool)0, (bool)1, (bool)0, NULL, /*hidden argument*/NULL);
		V_0 = ((ComponentU5BU5D_t4136971630*)Castclass(L_1, ComponentU5BU5D_t4136971630_il2cpp_TypeInfo_var));
		goto IL_0018;
	}

IL_0018:
	{
		ComponentU5BU5D_t4136971630* L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type,System.Boolean)
extern Il2CppClass* ComponentU5BU5D_t4136971630_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_m993725821_MetadataUsageId;
extern "C"  ComponentU5BU5D_t4136971630* GameObject_GetComponentsInChildren_m993725821 (GameObject_t1756533147 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_m993725821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ComponentU5BU5D_t4136971630* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		bool L_1 = ___includeInactive1;
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m3486524399(__this, L_0, (bool)0, (bool)1, L_1, (bool)0, NULL, /*hidden argument*/NULL);
		V_0 = ((ComponentU5BU5D_t4136971630*)Castclass(L_2, ComponentU5BU5D_t4136971630_il2cpp_TypeInfo_var));
		goto IL_0018;
	}

IL_0018:
	{
		ComponentU5BU5D_t4136971630* L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInParent(System.Type,System.Boolean)
extern Il2CppClass* ComponentU5BU5D_t4136971630_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_m1568786844_MetadataUsageId;
extern "C"  ComponentU5BU5D_t4136971630* GameObject_GetComponentsInParent_m1568786844 (GameObject_t1756533147 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_m1568786844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ComponentU5BU5D_t4136971630* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		bool L_1 = ___includeInactive1;
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m3486524399(__this, L_0, (bool)0, (bool)1, L_1, (bool)1, NULL, /*hidden argument*/NULL);
		V_0 = ((ComponentU5BU5D_t4136971630*)Castclass(L_2, ComponentU5BU5D_t4136971630_il2cpp_TypeInfo_var));
		goto IL_0018;
	}

IL_0018:
	{
		ComponentU5BU5D_t4136971630* L_3 = V_0;
		return L_3;
	}
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C"  Il2CppArray * GameObject_GetComponentsInternal_m3486524399 (GameObject_t1756533147 * __this, Type_t * ___type0, bool ___useSearchTypeAsArrayReturnType1, bool ___recursive2, bool ___includeInactive3, bool ___reverse4, Il2CppObject * ___resultList5, const MethodInfo* method)
{
	typedef Il2CppArray * (*GameObject_GetComponentsInternal_m3486524399_ftn) (GameObject_t1756533147 *, Type_t *, bool, bool, bool, bool, Il2CppObject *);
	static GameObject_GetComponentsInternal_m3486524399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m3486524399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type0, ___useSearchTypeAsArrayReturnType1, ___recursive2, ___includeInactive3, ___reverse4, ___resultList5);
}
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m1425941094 (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	typedef String_t* (*GameObject_get_tag_m1425941094_ftn) (GameObject_t1756533147 *);
	static GameObject_get_tag_m1425941094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_tag_m1425941094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_tag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C"  void GameObject_set_tag_m717375123 (GameObject_t1756533147 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*GameObject_set_tag_m717375123_ftn) (GameObject_t1756533147 *, String_t*);
	static GameObject_set_tag_m717375123_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_tag_m717375123_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_tag(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m71956653 (GameObject_t1756533147 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m71956653_ftn) (GameObject_t1756533147 *, String_t*, Il2CppObject *, int32_t);
	static GameObject_SendMessage_m71956653_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m71956653_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*GameObject_Find_m836511350_ftn) (String_t*);
	static GameObject_Find_m836511350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m836511350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m954570311 (Gradient_t3600583008 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Gradient_Init_m4156899649(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m4156899649 (Gradient_t3600583008 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Init_m4156899649_ftn) (Gradient_t3600583008 *);
	static Gradient_Init_m4156899649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m4156899649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m3573871739 (Gradient_t3600583008 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Cleanup_m3573871739_ftn) (Gradient_t3600583008 *);
	static Gradient_Cleanup_m3573871739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m3573871739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C"  void Gradient_Finalize_m2023716701 (Gradient_t3600583008 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m3573871739(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3600583008_marshal_pinvoke(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(const Gradient_t3600583008_marshaled_pinvoke& marshaled, Gradient_t3600583008& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(Gradient_t3600583008_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3600583008_marshal_com(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Gradient_t3600583008_marshal_com_back(const Gradient_t3600583008_marshaled_com& marshaled, Gradient_t3600583008& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t3600583008_marshal_com_cleanup(Gradient_t3600583008_marshaled_com& marshaled)
{
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t3381083099 * GUILayer_HitTest_m2960428006 (GUILayer_t3254902478 * __this, Vector3_t2243707580  ___screenPosition0, const MethodInfo* method)
{
	GUIElement_t3381083099 * V_0 = NULL;
	{
		GUIElement_t3381083099 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m693512502(NULL /*static, unused*/, __this, (&___screenPosition0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		GUIElement_t3381083099 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t3381083099 * GUILayer_INTERNAL_CALL_HitTest_m693512502 (Il2CppObject * __this /* static, unused */, GUILayer_t3254902478 * ___self0, Vector3_t2243707580 * ___screenPosition1, const MethodInfo* method)
{
	typedef GUIElement_t3381083099 * (*GUILayer_INTERNAL_CALL_HitTest_m693512502_ftn) (GUILayer_t3254902478 *, Vector3_t2243707580 *);
	static GUILayer_INTERNAL_CALL_HitTest_m693512502_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m693512502_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self0, ___screenPosition1);
}
// System.Void UnityEngine.HostData::.ctor()
extern "C"  void HostData__ctor_m3658760313 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.HostData::get_useNat()
extern "C"  bool HostData_get_useNat_m913506468 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Nat_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_useNat(System.Boolean)
extern "C"  void HostData_set_useNat_m293953067 (HostData_t3480691970 * __this, bool ___value0, const MethodInfo* method)
{
	HostData_t3480691970 * G_B2_0 = NULL;
	HostData_t3480691970 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	HostData_t3480691970 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_Nat_0(G_B3_0);
		return;
	}
}
// System.String UnityEngine.HostData::get_gameType()
extern "C"  String_t* HostData_get_gameType_m3171179991 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_GameType_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_gameType(System.String)
extern "C"  void HostData_set_gameType_m2782030850 (HostData_t3480691970 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_GameType_1(L_0);
		return;
	}
}
// System.String UnityEngine.HostData::get_gameName()
extern "C"  String_t* HostData_get_gameName_m2327200150 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_GameName_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_gameName(System.String)
extern "C"  void HostData_set_gameName_m155735243 (HostData_t3480691970 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_GameName_2(L_0);
		return;
	}
}
// System.Int32 UnityEngine.HostData::get_connectedPlayers()
extern "C"  int32_t HostData_get_connectedPlayers_m3304130837 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_ConnectedPlayers_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_connectedPlayers(System.Int32)
extern "C"  void HostData_set_connectedPlayers_m10216132 (HostData_t3480691970 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_ConnectedPlayers_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.HostData::get_playerLimit()
extern "C"  int32_t HostData_get_playerLimit_m778685484 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_PlayerLimit_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_playerLimit(System.Int32)
extern "C"  void HostData_set_playerLimit_m2757196607 (HostData_t3480691970 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_PlayerLimit_4(L_0);
		return;
	}
}
// System.String[] UnityEngine.HostData::get_ip()
extern "C"  StringU5BU5D_t1642385972* HostData_get_ip_m1062944386 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	StringU5BU5D_t1642385972* V_0 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_m_IP_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		StringU5BU5D_t1642385972* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_ip(System.String[])
extern "C"  void HostData_set_ip_m2429308365 (HostData_t3480691970 * __this, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_m_IP_5(L_0);
		return;
	}
}
// System.Int32 UnityEngine.HostData::get_port()
extern "C"  int32_t HostData_get_port_m1540456595 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Port_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_port(System.Int32)
extern "C"  void HostData_set_port_m676790380 (HostData_t3480691970 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_Port_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.HostData::get_passwordProtected()
extern "C"  bool HostData_get_passwordProtected_m2203608057 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_PasswordProtected_7();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_passwordProtected(System.Boolean)
extern "C"  void HostData_set_passwordProtected_m1048660848 (HostData_t3480691970 * __this, bool ___value0, const MethodInfo* method)
{
	HostData_t3480691970 * G_B2_0 = NULL;
	HostData_t3480691970 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	HostData_t3480691970 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_PasswordProtected_7(G_B3_0);
		return;
	}
}
// System.String UnityEngine.HostData::get_comment()
extern "C"  String_t* HostData_get_comment_m2816571654 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Comment_8();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_comment(System.String)
extern "C"  void HostData_set_comment_m609360741 (HostData_t3480691970 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_Comment_8(L_0);
		return;
	}
}
// System.String UnityEngine.HostData::get_guid()
extern "C"  String_t* HostData_get_guid_m3116013306 (HostData_t3480691970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_GUID_9();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.HostData::set_guid(System.String)
extern "C"  void HostData_set_guid_m103287381 (HostData_t3480691970 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_GUID_9(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HostData
extern "C" void HostData_t3480691970_marshal_pinvoke(const HostData_t3480691970& unmarshaled, HostData_t3480691970_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_IP_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_IP' of type 'HostData'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_IP_5Exception);
}
extern "C" void HostData_t3480691970_marshal_pinvoke_back(const HostData_t3480691970_marshaled_pinvoke& marshaled, HostData_t3480691970& unmarshaled)
{
	Il2CppCodeGenException* ___m_IP_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_IP' of type 'HostData'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_IP_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.HostData
extern "C" void HostData_t3480691970_marshal_pinvoke_cleanup(HostData_t3480691970_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.HostData
extern "C" void HostData_t3480691970_marshal_com(const HostData_t3480691970& unmarshaled, HostData_t3480691970_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_IP_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_IP' of type 'HostData'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_IP_5Exception);
}
extern "C" void HostData_t3480691970_marshal_com_back(const HostData_t3480691970_marshaled_com& marshaled, HostData_t3480691970& unmarshaled)
{
	Il2CppCodeGenException* ___m_IP_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_IP' of type 'HostData'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_IP_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.HostData
extern "C" void HostData_t3480691970_marshal_com_cleanup(HostData_t3480691970_marshaled_com& marshaled)
{
}
// System.String UnityEngine.HumanBone::get_boneName()
extern "C"  String_t* HumanBone_get_boneName_m1281040133 (HumanBone_t1529896151 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_BoneName_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* HumanBone_get_boneName_m1281040133_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	HumanBone_t1529896151 * _thisAdjusted = reinterpret_cast<HumanBone_t1529896151 *>(__this + 1);
	return HumanBone_get_boneName_m1281040133(_thisAdjusted, method);
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C"  void HumanBone_set_boneName_m2410239828 (HumanBone_t1529896151 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_BoneName_0(L_0);
		return;
	}
}
extern "C"  void HumanBone_set_boneName_m2410239828_AdjustorThunk (Il2CppObject * __this, String_t* ___value0, const MethodInfo* method)
{
	HumanBone_t1529896151 * _thisAdjusted = reinterpret_cast<HumanBone_t1529896151 *>(__this + 1);
	HumanBone_set_boneName_m2410239828(_thisAdjusted, ___value0, method);
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C"  String_t* HumanBone_get_humanName_m2091758568 (HumanBone_t1529896151 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_HumanName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* HumanBone_get_humanName_m2091758568_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	HumanBone_t1529896151 * _thisAdjusted = reinterpret_cast<HumanBone_t1529896151 *>(__this + 1);
	return HumanBone_get_humanName_m2091758568(_thisAdjusted, method);
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C"  void HumanBone_set_humanName_m1385708911 (HumanBone_t1529896151 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_HumanName_1(L_0);
		return;
	}
}
extern "C"  void HumanBone_set_humanName_m1385708911_AdjustorThunk (Il2CppObject * __this, String_t* ___value0, const MethodInfo* method)
{
	HumanBone_t1529896151 * _thisAdjusted = reinterpret_cast<HumanBone_t1529896151 *>(__this + 1);
	HumanBone_set_humanName_m1385708911(_thisAdjusted, ___value0, method);
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t1529896151_marshal_pinvoke(const HumanBone_t1529896151& unmarshaled, HumanBone_t1529896151_marshaled_pinvoke& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_BoneName_0());
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_m_HumanName_1());
	marshaled.___limit_2 = unmarshaled.get_limit_2();
}
extern "C" void HumanBone_t1529896151_marshal_pinvoke_back(const HumanBone_t1529896151_marshaled_pinvoke& marshaled, HumanBone_t1529896151& unmarshaled)
{
	unmarshaled.set_m_BoneName_0(il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0));
	unmarshaled.set_m_HumanName_1(il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1));
	HumanLimit_t250797648  unmarshaled_limit_temp_2;
	memset(&unmarshaled_limit_temp_2, 0, sizeof(unmarshaled_limit_temp_2));
	unmarshaled_limit_temp_2 = marshaled.___limit_2;
	unmarshaled.set_limit_2(unmarshaled_limit_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t1529896151_marshal_pinvoke_cleanup(HumanBone_t1529896151_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t1529896151_marshal_com(const HumanBone_t1529896151& unmarshaled, HumanBone_t1529896151_marshaled_com& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_BoneName_0());
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_HumanName_1());
	marshaled.___limit_2 = unmarshaled.get_limit_2();
}
extern "C" void HumanBone_t1529896151_marshal_com_back(const HumanBone_t1529896151_marshaled_com& marshaled, HumanBone_t1529896151& unmarshaled)
{
	unmarshaled.set_m_BoneName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_BoneName_0));
	unmarshaled.set_m_HumanName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___m_HumanName_1));
	HumanLimit_t250797648  unmarshaled_limit_temp_2;
	memset(&unmarshaled_limit_temp_2, 0, sizeof(unmarshaled_limit_temp_2));
	unmarshaled_limit_temp_2 = marshaled.___limit_2;
	unmarshaled.set_limit_2(unmarshaled_limit_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t1529896151_marshal_com_cleanup(HumanBone_t1529896151_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// System.Boolean UnityEngine.Input::GetKeyDownString(System.String)
extern "C"  bool Input_GetKeyDownString_m529184407 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyDownString_m529184407_ftn) (String_t*);
	static Input_GetKeyDownString_m529184407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownString_m529184407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownString(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m2098048324 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method)
{
	typedef float (*Input_GetAxis_m2098048324_ftn) (String_t*);
	static Input_GetAxis_m2098048324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxis_m2098048324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxis(System.String)");
	return _il2cpp_icall_func(___axisName0);
}
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Input_GetKeyDown_m1749539436_MetadataUsageId;
extern "C"  bool Input_GetKeyDown_m1749539436 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetKeyDown_m1749539436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownString_m529184407(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m464100923 (Il2CppObject * __this /* static, unused */, int32_t ___button0, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m464100923_ftn) (int32_t);
	static Input_GetMouseButton_m464100923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m464100923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button0);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m47917805 (Il2CppObject * __this /* static, unused */, int32_t ___button0, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m47917805_ftn) (int32_t);
	static Input_GetMouseButtonDown_m47917805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m47917805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button0);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Input_get_mousePosition_m146923508_MetadataUsageId;
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mousePosition_m146923508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m2302165941(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m2302165941 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m2302165941_ftn) (Vector3_t2243707580 *);
	static Input_INTERNAL_get_mousePosition_m2302165941_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m2302165941_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Input_GetTouch_m1463942798_MetadataUsageId;
extern "C"  Touch_t407273883  Input_GetTouch_m1463942798 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetTouch_m1463942798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Input_INTERNAL_CALL_GetTouch_m1737132542(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Touch_t407273883  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Touch_t407273883  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m1737132542 (Il2CppObject * __this /* static, unused */, int32_t ___index0, Touch_t407273883 * ___value1, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_CALL_GetTouch_m1737132542_ftn) (int32_t, Touch_t407273883 *);
	static Input_INTERNAL_CALL_GetTouch_m1737132542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_CALL_GetTouch_m1737132542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)");
	_il2cpp_icall_func(___index0, ___value1);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m2050827666 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m2050827666_ftn) ();
	static Input_get_touchCount_m2050827666_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m2050827666_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::.cctor()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Input__cctor_m829848544_MetadataUsageId;
extern "C"  void Input__cctor_m829848544 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input__cctor_m829848544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Input_t1785128008_StaticFields*)Input_t1785128008_il2cpp_TypeInfo_var->static_fields)->set_m_MainGyro_0((Gyroscope_t1705362817 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m4191464344 (DefaultValueAttribute_t1027170048 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  Il2CppObject * DefaultValueAttribute_get_Value_m397428899 (DefaultValueAttribute_t1027170048 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_DefaultValue_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern Il2CppClass* DefaultValueAttribute_t1027170048_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute_Equals_m3216982933_MetadataUsageId;
extern "C"  bool DefaultValueAttribute_Equals_m3216982933 (DefaultValueAttribute_t1027170048 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute_Equals_m3216982933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t1027170048 * V_0 = NULL;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = ((DefaultValueAttribute_t1027170048 *)IsInstClass(L_0, DefaultValueAttribute_t1027170048_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t1027170048 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0046;
	}

IL_0015:
	{
		Il2CppObject * L_2 = __this->get_DefaultValue_0();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		DefaultValueAttribute_t1027170048 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = DefaultValueAttribute_get_Value_m397428899(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((Il2CppObject*)(Il2CppObject *)L_4) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0046;
	}

IL_002f:
	{
		Il2CppObject * L_5 = __this->get_DefaultValue_0();
		DefaultValueAttribute_t1027170048 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = DefaultValueAttribute_get_Value_m397428899(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		V_1 = L_8;
		goto IL_0046;
	}

IL_0046:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C"  int32_t DefaultValueAttribute_GetHashCode_m1495299559 (DefaultValueAttribute_t1027170048 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = __this->get_DefaultValue_0();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m2653962112(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0029;
	}

IL_0018:
	{
		Il2CppObject * L_2 = __this->get_DefaultValue_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_0029:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C"  void ExcludeFromDocsAttribute__ctor_m1684225175 (ExcludeFromDocsAttribute_t665825653 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::.ctor()
extern "C"  void LocalNotification__ctor_m4030077664 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		LocalNotification_InitWrapper_m2867883691(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.iOS.LocalNotification::GetFireDate()
extern "C"  double LocalNotification_GetFireDate_m2090610709 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef double (*LocalNotification_GetFireDate_m2090610709_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_GetFireDate_m2090610709_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_GetFireDate_m2090610709_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::GetFireDate()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::SetFireDate(System.Double)
extern "C"  void LocalNotification_SetFireDate_m3822573886 (LocalNotification_t317971878 * __this, double ___dt0, const MethodInfo* method)
{
	typedef void (*LocalNotification_SetFireDate_m3822573886_ftn) (LocalNotification_t317971878 *, double);
	static LocalNotification_SetFireDate_m3822573886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_SetFireDate_m3822573886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::SetFireDate(System.Double)");
	_il2cpp_icall_func(__this, ___dt0);
}
// System.DateTime UnityEngine.iOS.LocalNotification::get_fireDate()
extern Il2CppClass* LocalNotification_t317971878_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification_get_fireDate_m1789441812_MetadataUsageId;
extern "C"  DateTime_t693205669  LocalNotification_get_fireDate_m1789441812 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification_get_fireDate_m1789441812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		double L_0 = LocalNotification_GetFireDate_m2090610709(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LocalNotification_t317971878_il2cpp_TypeInfo_var);
		int64_t L_1 = ((LocalNotification_t317971878_StaticFields*)LocalNotification_t317971878_il2cpp_TypeInfo_var->static_fields)->get_m_NSReferenceDateTicks_1();
		DateTime_t693205669  L_2;
		memset(&L_2, 0, sizeof(L_2));
		DateTime__ctor_m2586249130(&L_2, ((int64_t)((int64_t)(((int64_t)((int64_t)((double)((double)L_0*(double)(10000000.0))))))+(int64_t)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0023;
	}

IL_0023:
	{
		DateTime_t693205669  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::set_fireDate(System.DateTime)
extern Il2CppClass* LocalNotification_t317971878_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification_set_fireDate_m462195765_MetadataUsageId;
extern "C"  void LocalNotification_set_fireDate_m462195765 (LocalNotification_t317971878 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification_set_fireDate_m462195765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = DateTime_ToUniversalTime_m1815024752((&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		int64_t L_1 = DateTime_get_Ticks_m310281298((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LocalNotification_t317971878_il2cpp_TypeInfo_var);
		int64_t L_2 = ((LocalNotification_t317971878_StaticFields*)LocalNotification_t317971878_il2cpp_TypeInfo_var->static_fields)->get_m_NSReferenceDateTicks_1();
		LocalNotification_SetFireDate_m3822573886(__this, ((double)((double)(((double)((double)((int64_t)((int64_t)L_1-(int64_t)L_2)))))/(double)(10000000.0))), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.iOS.LocalNotification::get_timeZone()
extern "C"  String_t* LocalNotification_get_timeZone_m165284569 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_timeZone_m165284569_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_timeZone_m165284569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_timeZone_m165284569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_timeZone()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_timeZone(System.String)
extern "C"  void LocalNotification_set_timeZone_m1306235542 (LocalNotification_t317971878 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_timeZone_m1306235542_ftn) (LocalNotification_t317971878 *, String_t*);
	static LocalNotification_set_timeZone_m1306235542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_timeZone_m1306235542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_timeZone(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.iOS.CalendarUnit UnityEngine.iOS.LocalNotification::get_repeatInterval()
extern "C"  int32_t LocalNotification_get_repeatInterval_m523532072 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef int32_t (*LocalNotification_get_repeatInterval_m523532072_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_repeatInterval_m523532072_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_repeatInterval_m523532072_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_repeatInterval()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_repeatInterval(UnityEngine.iOS.CalendarUnit)
extern "C"  void LocalNotification_set_repeatInterval_m2654714759 (LocalNotification_t317971878 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_repeatInterval_m2654714759_ftn) (LocalNotification_t317971878 *, int32_t);
	static LocalNotification_set_repeatInterval_m2654714759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_repeatInterval_m2654714759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_repeatInterval(UnityEngine.iOS.CalendarUnit)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.iOS.CalendarIdentifier UnityEngine.iOS.LocalNotification::get_repeatCalendar()
extern "C"  int32_t LocalNotification_get_repeatCalendar_m3906163638 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef int32_t (*LocalNotification_get_repeatCalendar_m3906163638_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_repeatCalendar_m3906163638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_repeatCalendar_m3906163638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_repeatCalendar()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_repeatCalendar(UnityEngine.iOS.CalendarIdentifier)
extern "C"  void LocalNotification_set_repeatCalendar_m1866253719 (LocalNotification_t317971878 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_repeatCalendar_m1866253719_ftn) (LocalNotification_t317971878 *, int32_t);
	static LocalNotification_set_repeatCalendar_m1866253719_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_repeatCalendar_m1866253719_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_repeatCalendar(UnityEngine.iOS.CalendarIdentifier)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_alertBody()
extern "C"  String_t* LocalNotification_get_alertBody_m3389080544 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_alertBody_m3389080544_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_alertBody_m3389080544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_alertBody_m3389080544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_alertBody()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_alertBody(System.String)
extern "C"  void LocalNotification_set_alertBody_m1054552499 (LocalNotification_t317971878 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_alertBody_m1054552499_ftn) (LocalNotification_t317971878 *, String_t*);
	static LocalNotification_set_alertBody_m1054552499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_alertBody_m1054552499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_alertBody(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_alertAction()
extern "C"  String_t* LocalNotification_get_alertAction_m1761754846 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_alertAction_m1761754846_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_alertAction_m1761754846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_alertAction_m1761754846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_alertAction()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_alertAction(System.String)
extern "C"  void LocalNotification_set_alertAction_m3106365785 (LocalNotification_t317971878 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_alertAction_m3106365785_ftn) (LocalNotification_t317971878 *, String_t*);
	static LocalNotification_set_alertAction_m3106365785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_alertAction_m3106365785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_alertAction(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.iOS.LocalNotification::get_hasAction()
extern "C"  bool LocalNotification_get_hasAction_m2700471435 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef bool (*LocalNotification_get_hasAction_m2700471435_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_hasAction_m2700471435_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_hasAction_m2700471435_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_hasAction()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_hasAction(System.Boolean)
extern "C"  void LocalNotification_set_hasAction_m3772904808 (LocalNotification_t317971878 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_hasAction_m3772904808_ftn) (LocalNotification_t317971878 *, bool);
	static LocalNotification_set_hasAction_m3772904808_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_hasAction_m3772904808_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_hasAction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_alertLaunchImage()
extern "C"  String_t* LocalNotification_get_alertLaunchImage_m1987036946 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_alertLaunchImage_m1987036946_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_alertLaunchImage_m1987036946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_alertLaunchImage_m1987036946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_alertLaunchImage()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_alertLaunchImage(System.String)
extern "C"  void LocalNotification_set_alertLaunchImage_m1613118657 (LocalNotification_t317971878 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_alertLaunchImage_m1613118657_ftn) (LocalNotification_t317971878 *, String_t*);
	static LocalNotification_set_alertLaunchImage_m1613118657_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_alertLaunchImage_m1613118657_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_alertLaunchImage(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.iOS.LocalNotification::get_applicationIconBadgeNumber()
extern "C"  int32_t LocalNotification_get_applicationIconBadgeNumber_m2229540452 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef int32_t (*LocalNotification_get_applicationIconBadgeNumber_m2229540452_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_applicationIconBadgeNumber_m2229540452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_applicationIconBadgeNumber_m2229540452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_applicationIconBadgeNumber()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_applicationIconBadgeNumber(System.Int32)
extern "C"  void LocalNotification_set_applicationIconBadgeNumber_m1023929213 (LocalNotification_t317971878 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_applicationIconBadgeNumber_m1023929213_ftn) (LocalNotification_t317971878 *, int32_t);
	static LocalNotification_set_applicationIconBadgeNumber_m1023929213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_applicationIconBadgeNumber_m1023929213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_applicationIconBadgeNumber(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_soundName()
extern "C"  String_t* LocalNotification_get_soundName_m4273831850 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_soundName_m4273831850_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_soundName_m4273831850_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_soundName_m4273831850_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_soundName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_soundName(System.String)
extern "C"  void LocalNotification_set_soundName_m1522089625 (LocalNotification_t317971878 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_soundName_m1522089625_ftn) (LocalNotification_t317971878 *, String_t*);
	static LocalNotification_set_soundName_m1522089625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_soundName_m1522089625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_soundName(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_defaultSoundName()
extern "C"  String_t* LocalNotification_get_defaultSoundName_m1631057317 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_defaultSoundName_m1631057317_ftn) ();
	static LocalNotification_get_defaultSoundName_m1631057317_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_defaultSoundName_m1631057317_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_defaultSoundName()");
	return _il2cpp_icall_func();
}
// System.Collections.IDictionary UnityEngine.iOS.LocalNotification::get_userInfo()
extern "C"  Il2CppObject * LocalNotification_get_userInfo_m2319244588 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef Il2CppObject * (*LocalNotification_get_userInfo_m2319244588_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_get_userInfo_m2319244588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_userInfo_m2319244588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_userInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_userInfo(System.Collections.IDictionary)
extern "C"  void LocalNotification_set_userInfo_m3890141201 (LocalNotification_t317971878 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_userInfo_m3890141201_ftn) (LocalNotification_t317971878 *, Il2CppObject *);
	static LocalNotification_set_userInfo_m3890141201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_userInfo_m3890141201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_userInfo(System.Collections.IDictionary)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m215220946 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef void (*LocalNotification_Destroy_m215220946_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_Destroy_m215220946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_Destroy_m215220946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::Finalize()
extern "C"  void LocalNotification_Finalize_m1087615408 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		LocalNotification_Destroy_m215220946(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::InitWrapper()
extern "C"  void LocalNotification_InitWrapper_m2867883691 (LocalNotification_t317971878 * __this, const MethodInfo* method)
{
	typedef void (*LocalNotification_InitWrapper_m2867883691_ftn) (LocalNotification_t317971878 *);
	static LocalNotification_InitWrapper_m2867883691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_InitWrapper_m2867883691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::InitWrapper()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern Il2CppClass* LocalNotification_t317971878_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification__cctor_m1459868805_MetadataUsageId;
extern "C"  void LocalNotification__cctor_m1459868805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification__cctor_m1459868805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m3270618252((&V_0), ((int32_t)2001), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		int64_t L_0 = DateTime_get_Ticks_m310281298((&V_0), /*hidden argument*/NULL);
		((LocalNotification_t317971878_StaticFields*)LocalNotification_t317971878_il2cpp_TypeInfo_var->static_fields)->set_m_NSReferenceDateTicks_1(L_0);
		return;
	}
}
// System.Void UnityEngine.iOS.RemoteNotification::.ctor()
extern "C"  void RemoteNotification__ctor_m4012285747 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.iOS.RemoteNotification::get_alertBody()
extern "C"  String_t* RemoteNotification_get_alertBody_m2415664629 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	typedef String_t* (*RemoteNotification_get_alertBody_m2415664629_ftn) (RemoteNotification_t2254252895 *);
	static RemoteNotification_get_alertBody_m2415664629_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_alertBody_m2415664629_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_alertBody()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.iOS.RemoteNotification::get_hasAction()
extern "C"  bool RemoteNotification_get_hasAction_m1155297768 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	typedef bool (*RemoteNotification_get_hasAction_m1155297768_ftn) (RemoteNotification_t2254252895 *);
	static RemoteNotification_get_hasAction_m1155297768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_hasAction_m1155297768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_hasAction()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.iOS.RemoteNotification::get_applicationIconBadgeNumber()
extern "C"  int32_t RemoteNotification_get_applicationIconBadgeNumber_m181124595 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	typedef int32_t (*RemoteNotification_get_applicationIconBadgeNumber_m181124595_ftn) (RemoteNotification_t2254252895 *);
	static RemoteNotification_get_applicationIconBadgeNumber_m181124595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_applicationIconBadgeNumber_m181124595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_applicationIconBadgeNumber()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.iOS.RemoteNotification::get_soundName()
extern "C"  String_t* RemoteNotification_get_soundName_m3129902515 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	typedef String_t* (*RemoteNotification_get_soundName_m3129902515_ftn) (RemoteNotification_t2254252895 *);
	static RemoteNotification_get_soundName_m3129902515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_soundName_m3129902515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_soundName()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.IDictionary UnityEngine.iOS.RemoteNotification::get_userInfo()
extern "C"  Il2CppObject * RemoteNotification_get_userInfo_m1861413979 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	typedef Il2CppObject * (*RemoteNotification_get_userInfo_m1861413979_ftn) (RemoteNotification_t2254252895 *);
	static RemoteNotification_get_userInfo_m1861413979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_userInfo_m1861413979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_userInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m1272240769 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	typedef void (*RemoteNotification_Destroy_m1272240769_ftn) (RemoteNotification_t2254252895 *);
	static RemoteNotification_Destroy_m1272240769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_Destroy_m1272240769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Finalize()
extern "C"  void RemoteNotification_Finalize_m2712637425 (RemoteNotification_t2254252895 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		RemoteNotification_Destroy_m1272240769(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C"  void Keyframe__ctor_m2042404667 (Keyframe_t1449471340 * __this, float ___time0, float ___value1, const MethodInfo* method)
{
	{
		float L_0 = ___time0;
		__this->set_m_Time_0(L_0);
		float L_1 = ___value1;
		__this->set_m_Value_1(L_1);
		__this->set_m_InTangent_2((0.0f));
		__this->set_m_OutTangent_3((0.0f));
		return;
	}
}
extern "C"  void Keyframe__ctor_m2042404667_AdjustorThunk (Il2CppObject * __this, float ___time0, float ___value1, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe__ctor_m2042404667(_thisAdjusted, ___time0, ___value1, method);
}
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Keyframe__ctor_m140082843 (Keyframe_t1449471340 * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const MethodInfo* method)
{
	{
		float L_0 = ___time0;
		__this->set_m_Time_0(L_0);
		float L_1 = ___value1;
		__this->set_m_Value_1(L_1);
		float L_2 = ___inTangent2;
		__this->set_m_InTangent_2(L_2);
		float L_3 = ___outTangent3;
		__this->set_m_OutTangent_3(L_3);
		return;
	}
}
extern "C"  void Keyframe__ctor_m140082843_AdjustorThunk (Il2CppObject * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe__ctor_m140082843(_thisAdjusted, ___time0, ___value1, ___inTangent2, ___outTangent3, method);
}
// System.Single UnityEngine.Keyframe::get_time()
extern "C"  float Keyframe_get_time_m2226372497 (Keyframe_t1449471340 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Time_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Keyframe_get_time_m2226372497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	return Keyframe_get_time_m2226372497(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_time(System.Single)
extern "C"  void Keyframe_set_time_m848496062 (Keyframe_t1449471340 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_time_m848496062_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe_set_time_m848496062(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Keyframe::get_value()
extern "C"  float Keyframe_get_value_m979894315 (Keyframe_t1449471340 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Value_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Keyframe_get_value_m979894315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	return Keyframe_get_value_m979894315(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_value(System.Single)
extern "C"  void Keyframe_set_value_m2184486356 (Keyframe_t1449471340 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Value_1(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_value_m2184486356_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe_set_value_m2184486356(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Keyframe::get_inTangent()
extern "C"  float Keyframe_get_inTangent_m3256944616 (Keyframe_t1449471340 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_InTangent_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Keyframe_get_inTangent_m3256944616_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	return Keyframe_get_inTangent_m3256944616(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_inTangent(System.Single)
extern "C"  void Keyframe_set_inTangent_m4280114775 (Keyframe_t1449471340 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_InTangent_2(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_inTangent_m4280114775_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe_set_inTangent_m4280114775(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Keyframe::get_outTangent()
extern "C"  float Keyframe_get_outTangent_m1894374085 (Keyframe_t1449471340 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_OutTangent_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Keyframe_get_outTangent_m1894374085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	return Keyframe_get_outTangent_m1894374085(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_outTangent(System.Single)
extern "C"  void Keyframe_set_outTangent_m1054927214 (Keyframe_t1449471340 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_OutTangent_3(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_outTangent_m1054927214_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe_set_outTangent_m1054927214(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.Keyframe::get_tangentMode()
extern "C"  int32_t Keyframe_get_tangentMode_m1869200796 (Keyframe_t1449471340 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0008;
	}

IL_0008:
	{
		int32_t L_0 = V_0;
		return L_0;
	}
}
extern "C"  int32_t Keyframe_get_tangentMode_m1869200796_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	return Keyframe_get_tangentMode_m1869200796(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_tangentMode(System.Int32)
extern "C"  void Keyframe_set_tangentMode_m1073266123 (Keyframe_t1449471340 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void Keyframe_set_tangentMode_m1073266123_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Keyframe_t1449471340 * _thisAdjusted = reinterpret_cast<Keyframe_t1449471340 *>(__this + 1);
	Keyframe_set_tangentMode_m1073266123(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m3834134587 (Logger_t3328995178 * __this, Il2CppObject * ___logHandler0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___logHandler0;
		Logger_set_logHandler_m2851576632(__this, L_0, /*hidden argument*/NULL);
		Logger_set_logEnabled_m3852234466(__this, (bool)1, /*hidden argument*/NULL);
		Logger_set_filterLogType_m1452353615(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  Il2CppObject * Logger_get_logHandler_m4190583509 (Logger_t3328995178 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U3ClogHandlerU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m2851576632 (Logger_t3328995178 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3ClogHandlerU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m3807759477 (Logger_t3328995178 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3ClogEnabledU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m3852234466 (Logger_t3328995178 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ClogEnabledU3Ek__BackingField_1(L_0);
		return;
	}
}
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m3672438698 (Logger_t3328995178 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CfilterLogTypeU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m1452353615 (Logger_t3328995178 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfilterLogTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m1750132386 (Logger_t3328995178 * __this, int32_t ___logType0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		bool L_0 = Logger_get_logEnabled_m3807759477(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_1 = ___logType0;
		int32_t L_2 = Logger_get_filterLogType_m3672438698(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_3 = ___logType0;
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)4))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B4_0 = 1;
	}

IL_001f:
	{
		V_0 = (bool)G_B4_0;
		goto IL_002c;
	}

IL_0025:
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.String UnityEngine.Logger::GetString(System.Object)
extern Il2CppCodeGenString* _stringLiteral1743625299;
extern const uint32_t Logger_GetString_m4086587133_MetadataUsageId;
extern "C"  String_t* Logger_GetString_m4086587133 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_GetString_m4086587133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral1743625299;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001d;
	}

IL_001d:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104529068;
extern const uint32_t Logger_Log_m3587255568_MetadataUsageId;
extern "C"  void Logger_Log_m3587255568 (Logger_t3328995178 * __this, int32_t ___logType0, Il2CppObject * ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m3587255568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1750132386(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		ObjectU5BU5D_t3614634134* L_4 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_5 = ___message1;
		String_t* L_6 = Logger_GetString_m4086587133(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1021602117 *, String_t*, ObjectU5BU5D_t3614634134* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_2, L_3, (Object_t1021602117 *)NULL, _stringLiteral104529068, L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Logger_LogFormat_m193464629_MetadataUsageId;
extern "C"  void Logger_LogFormat_m193464629 (Logger_t3328995178 * __this, int32_t ___logType0, Object_t1021602117 * ___context1, String_t* ___format2, ObjectU5BU5D_t3614634134* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m193464629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1750132386(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t1021602117 * L_4 = ___context1;
		String_t* L_5 = ___format2;
		ObjectU5BU5D_t3614634134* L_6 = ___args3;
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1021602117 *, String_t*, ObjectU5BU5D_t3614634134* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Logger_LogException_m206035446_MetadataUsageId;
extern "C"  void Logger_LogException_m206035446 (Logger_t3328995178 * __this, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogException_m206035446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Logger_get_logEnabled_m3807759477(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject * L_1 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		Exception_t1927440687 * L_2 = ___exception0;
		Object_t1021602117 * L_3 = ___context1;
		NullCheck(L_1);
		InterfaceActionInvoker2< Exception_t1927440687 *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_1, L_2, L_3);
	}

IL_0019:
	{
		return;
	}
}
// System.Single UnityEngine.Mathf::Asin(System.Single)
extern "C"  float Mathf_Asin_m1367675601 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = asin((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern "C"  float Mathf_Atan2_m496071197 (Il2CppObject * __this /* static, unused */, float ___y0, float ___x1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___y0;
		float L_1 = ___x1;
		double L_2 = atan2((((double)((double)L_0))), (((double)((double)L_1))));
		V_0 = (((float)((float)L_2)));
		goto IL_0011;
	}

IL_0011:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m1942863256 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		V_0 = (((float)((float)L_1)));
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2564622569 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Approximately_m1064446634_MetadataUsageId;
extern "C"  bool Mathf_Approximately_m1064446634 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m1064446634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t2336485820_StaticFields*)Mathf_t2336485820_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		float L_9 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
		goto IL_0038;
	}

IL_0038:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Mathf::.cctor()
extern Il2CppClass* MathfInternal_t715669973_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m326403828_MetadataUsageId;
extern "C"  void Mathf__cctor_m326403828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m326403828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t715669973_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t715669973_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t715669973_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t2336485820_StaticFields*)Mathf_t2336485820_il2cpp_TypeInfo_var->static_fields)->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C"  void Mesh_Clear_m3100797454 (Mesh_t1356156583 * __this, bool ___keepVertexLayout0, const MethodInfo* method)
{
	typedef void (*Mesh_Clear_m3100797454_ftn) (Mesh_t1356156583 *, bool);
	static Mesh_Clear_m3100797454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Clear_m3100797454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout0);
}
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m231813403 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		Mesh_Clear_m3100797454(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m2763498171 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef bool (*Mesh_get_canAccess_m2763498171_ftn) (Mesh_t1356156583 *);
	static Mesh_get_canAccess_m2763498171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_canAccess_m2763498171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_canAccess()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessMesh(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessMesh_m2827771108 (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	typedef void (*Mesh_PrintErrorCantAccessMesh_m2827771108_ftn) (Mesh_t1356156583 *, int32_t);
	static Mesh_PrintErrorCantAccessMesh_m2827771108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorCantAccessMesh_m2827771108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorCantAccessMesh(UnityEngine.Mesh/InternalShaderChannel)");
	_il2cpp_icall_func(__this, ___channel0);
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessMeshForIndices()
extern "C"  void Mesh_PrintErrorCantAccessMeshForIndices_m3276222682 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_PrintErrorCantAccessMeshForIndices_m3276222682_ftn) (Mesh_t1356156583 *);
	static Mesh_PrintErrorCantAccessMeshForIndices_m3276222682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorCantAccessMeshForIndices_m3276222682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorCantAccessMeshForIndices()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetArrayForChannelImpl_m271696022 (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, Il2CppArray * ___values3, int32_t ___arraySize4, const MethodInfo* method)
{
	typedef void (*Mesh_SetArrayForChannelImpl_m271696022_ftn) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, Il2CppArray *, int32_t);
	static Mesh_SetArrayForChannelImpl_m271696022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetArrayForChannelImpl_m271696022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)");
	_il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2, ___values3, ___arraySize4);
}
// System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Il2CppArray * Mesh_GetAllocArrayFromChannelImpl_m1663415136 (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	typedef Il2CppArray * (*Mesh_GetAllocArrayFromChannelImpl_m1663415136_ftn) (Mesh_t1356156583 *, int32_t, int32_t, int32_t);
	static Mesh_GetAllocArrayFromChannelImpl_m1663415136_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetAllocArrayFromChannelImpl_m1663415136_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)");
	return _il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2);
}
// System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  bool Mesh_HasChannel_m3616583481 (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	typedef bool (*Mesh_HasChannel_m3616583481_ftn) (Mesh_t1356156583 *, int32_t);
	static Mesh_HasChannel_m3616583481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_HasChannel_m3616583481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)");
	return _il2cpp_icall_func(__this, ___channel0);
}
// System.Int32[] UnityEngine.Mesh::GetTrianglesImpl(System.Int32)
extern "C"  Int32U5BU5D_t3030399641* Mesh_GetTrianglesImpl_m3976840238 (Mesh_t1356156583 * __this, int32_t ___submesh0, const MethodInfo* method)
{
	typedef Int32U5BU5D_t3030399641* (*Mesh_GetTrianglesImpl_m3976840238_ftn) (Mesh_t1356156583 *, int32_t);
	static Mesh_GetTrianglesImpl_m3976840238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetTrianglesImpl_m3976840238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetTrianglesImpl(System.Int32)");
	return _il2cpp_icall_func(__this, ___submesh0);
}
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTrianglesImpl_m2743099196 (Mesh_t1356156583 * __this, int32_t ___submesh0, Il2CppArray * ___triangles1, int32_t ___arraySize2, bool ___calculateBounds3, const MethodInfo* method)
{
	typedef void (*Mesh_SetTrianglesImpl_m2743099196_ftn) (Mesh_t1356156583 *, int32_t, Il2CppArray *, int32_t, bool);
	static Mesh_SetTrianglesImpl_m2743099196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTrianglesImpl_m2743099196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___submesh0, ___triangles1, ___arraySize2, ___calculateBounds3);
}
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetTrianglesImpl_m540573335 (Mesh_t1356156583 * __this, int32_t ___submesh0, Il2CppArray * ___triangles1, int32_t ___arraySize2, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___submesh0;
		Il2CppArray * L_1 = ___triangles1;
		int32_t L_2 = ___arraySize2;
		bool L_3 = V_0;
		Mesh_SetTrianglesImpl_m2743099196(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m3559909024 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateBounds_m3559909024_ftn) (Mesh_t1356156583 *);
	static Mesh_RecalculateBounds_m3559909024_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateBounds_m3559909024_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateBounds()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C"  void Mesh_RecalculateNormals_m1034493793 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateNormals_m1034493793_ftn) (Mesh_t1356156583 *);
	static Mesh_RecalculateNormals_m1034493793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateNormals_m1034493793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateNormals()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Mesh/InternalShaderChannel)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1066778515;
extern Il2CppCodeGenString* _stringLiteral3637194755;
extern const uint32_t Mesh_DefaultDimensionForChannel_m153181993_MetadataUsageId;
extern "C"  int32_t Mesh_DefaultDimensionForChannel_m153181993 (Il2CppObject * __this /* static, unused */, int32_t ___channel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_DefaultDimensionForChannel_m153181993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___channel0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___channel0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0015;
		}
	}

IL_000e:
	{
		V_0 = 3;
		goto IL_004f;
	}

IL_0015:
	{
		int32_t L_2 = ___channel0;
		if ((((int32_t)L_2) < ((int32_t)3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = ___channel0;
		if ((((int32_t)L_3) > ((int32_t)6)))
		{
			goto IL_002a;
		}
	}
	{
		V_0 = 2;
		goto IL_004f;
	}

IL_002a:
	{
		int32_t L_4 = ___channel0;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_5 = ___channel0;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003f;
		}
	}

IL_0038:
	{
		V_0 = 4;
		goto IL_004f;
	}

IL_003f:
	{
		ArgumentException_t3259014390 * L_6 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_6, _stringLiteral1066778515, _stringLiteral3637194755, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_004f:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Int32 UnityEngine.Mesh::SafeLength(System.Array)
extern "C"  int32_t Mesh_SafeLength_m3028452891 (Mesh_t1356156583 * __this, Il2CppArray * ___values0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		Il2CppArray * L_0 = ___values0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppArray * L_1 = ___values0;
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m1498215565(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		V_0 = G_B3_0;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetSizedArrayForChannel_m299035387 (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, Il2CppArray * ___values3, int32_t ___valuesCount4, const MethodInfo* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m2763498171(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = ___channel0;
		int32_t L_2 = ___format1;
		int32_t L_3 = ___dim2;
		Il2CppArray * L_4 = ___values3;
		int32_t L_5 = ___valuesCount4;
		Mesh_SetArrayForChannelImpl_m271696022(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001e:
	{
		int32_t L_6 = ___channel0;
		Mesh_PrintErrorCantAccessMesh_m2827771108(__this, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_MethodInfo_var;
extern const uint32_t Mesh_get_vertices_m626989480_MetadataUsageId;
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_vertices_m626989480 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_vertices_m626989480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		Vector3U5BU5D_t1172311765* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537(__this, 0, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_MethodInfo_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector3U5BU5D_t1172311765* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern const MethodInfo* Mesh_SetArrayForChannel_TisVector3_t2243707580_m3518652704_MethodInfo_var;
extern const uint32_t Mesh_set_vertices_m2936804213_MetadataUsageId;
extern "C"  void Mesh_set_vertices_m2936804213 (Mesh_t1356156583 * __this, Vector3U5BU5D_t1172311765* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_set_vertices_m2936804213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3U5BU5D_t1172311765* L_0 = ___value0;
		Mesh_SetArrayForChannel_TisVector3_t2243707580_m3518652704(__this, 0, L_0, /*hidden argument*/Mesh_SetArrayForChannel_TisVector3_t2243707580_m3518652704_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_MethodInfo_var;
extern const uint32_t Mesh_get_uv_m3811151337_MetadataUsageId;
extern "C"  Vector2U5BU5D_t686124026* Mesh_get_uv_m3811151337 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv_m3811151337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t686124026* V_0 = NULL;
	{
		Vector2U5BU5D_t686124026* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716(__this, 3, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_MethodInfo_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t686124026* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern const MethodInfo* Mesh_SetArrayForChannel_TisVector2_t2243707579_m1051036475_MethodInfo_var;
extern const uint32_t Mesh_set_uv_m1497318906_MetadataUsageId;
extern "C"  void Mesh_set_uv_m1497318906 (Mesh_t1356156583 * __this, Vector2U5BU5D_t686124026* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_set_uv_m1497318906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2U5BU5D_t686124026* L_0 = ___value0;
		Mesh_SetArrayForChannel_TisVector2_t2243707579_m1051036475(__this, 3, L_0, /*hidden argument*/Mesh_SetArrayForChannel_TisVector2_t2243707579_m1051036475_MethodInfo_var);
		return;
	}
}
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t Mesh_get_triangles_m3988715512_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* Mesh_get_triangles_m3988715512 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_triangles_m3988715512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		bool L_0 = Mesh_get_canAccess_m2763498171(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_1 = Mesh_GetTrianglesImpl_m3976840238(__this, (-1), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_002b;
	}

IL_0019:
	{
		Mesh_PrintErrorCantAccessMeshForIndices_m3276222682(__this, /*hidden argument*/NULL);
		V_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0));
		goto IL_002b;
	}

IL_002b:
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C"  void Mesh_set_triangles_m3244966865 (Mesh_t1356156583 * __this, Int32U5BU5D_t3030399641* ___value0, const MethodInfo* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m2763498171(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_1 = ___value0;
		Int32U5BU5D_t3030399641* L_2 = ___value0;
		int32_t L_3 = Mesh_SafeLength_m3028452891(__this, (Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		Mesh_SetTrianglesImpl_m540573335(__this, (-1), (Il2CppArray *)(Il2CppArray *)L_1, L_3, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		Mesh_PrintErrorCantAccessMeshForIndices_m3276222682(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_mesh_m977520135 (MeshFilter_t3026937449 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*MeshFilter_get_mesh_m977520135_ftn) (MeshFilter_t3026937449 *);
	static MeshFilter_get_mesh_m977520135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m977520135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m2699265412(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m3154116776 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3504849565 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m666563676 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m666563676_ftn) (MonoBehaviour_t1158329972 *, String_t*, float);
	static MonoBehaviour_Invoke_m666563676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m666563676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m3468262484 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m3468262484_ftn) (MonoBehaviour_t1158329972 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m3468262484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m3468262484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m744713777 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m3154116776(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2508161963 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2508161963_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2508161963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2508161963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1469271462 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1469271462_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_IsInvoking_m1469271462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1469271462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m345622956 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3504849565(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	Coroutine_t2299508840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t2299508840 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_m1744905232 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	Coroutine_t2299508840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t2299508840 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m296997955 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_m296997955_ftn) (MonoBehaviour_t1158329972 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m296997955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m296997955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m1399371129 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Coroutine_t2299508840 * V_1 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t2299508840 * L_2 = MonoBehaviour_StartCoroutine_m296997955(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Coroutine_t2299508840 * L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m987450539 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m987450539_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_StopCoroutine_m987450539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m987450539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1170478282 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1668572632 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t2299508840 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1923670638(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1923670638 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn) (MonoBehaviour_t1158329972 *, Coroutine_t2299508840 *);
	static MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1675795839 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1675795839_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_StopAllCoroutines_m1675795839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1675795839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m3437620292_MetadataUsageId;
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m3437620292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m524237270 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m524237270_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_get_useGUILayout_m524237270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m524237270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m2666356651 (MonoBehaviour_t1158329972 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m2666356651_ftn) (MonoBehaviour_t1158329972 *, bool);
	static MonoBehaviour_set_useGUILayout_m2666356651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m2666356651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Double UnityEngine.NetworkMessageInfo::get_timestamp()
extern "C"  double NetworkMessageInfo_get_timestamp_m462964950 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = __this->get_m_TimeStamp_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		double L_1 = V_0;
		return L_1;
	}
}
extern "C"  double NetworkMessageInfo_get_timestamp_m462964950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_timestamp_m462964950(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkMessageInfo::get_sender()
extern "C"  NetworkPlayer_t1243528291  NetworkMessageInfo_get_sender_m2366451889 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkPlayer_t1243528291  L_0 = __this->get_m_Sender_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		NetworkPlayer_t1243528291  L_1 = V_0;
		return L_1;
	}
}
extern "C"  NetworkPlayer_t1243528291  NetworkMessageInfo_get_sender_m2366451889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_sender_m2366451889(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::get_networkView()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3523052579;
extern const uint32_t NetworkMessageInfo_get_networkView_m1180629659_MetadataUsageId;
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_get_networkView_m1180629659 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkMessageInfo_get_networkView_m1180629659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkView_t172525251 * V_0 = NULL;
	{
		NetworkViewID_t3942988548  L_0 = __this->get_m_ViewID_2();
		NetworkViewID_t3942988548  L_1 = NetworkViewID_get_unassigned_m2814913999(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = NetworkViewID_op_Equality_m4173239775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3523052579, /*hidden argument*/NULL);
		NetworkView_t172525251 * L_3 = NetworkMessageInfo_NullNetworkView_m230600625(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_003e;
	}

IL_002d:
	{
		NetworkViewID_t3942988548  L_4 = __this->get_m_ViewID_2();
		NetworkView_t172525251 * L_5 = NetworkView_Find_m143547961(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_003e;
	}

IL_003e:
	{
		NetworkView_t172525251 * L_6 = V_0;
		return L_6;
	}
}
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_get_networkView_m1180629659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_networkView_m1180629659(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_NullNetworkView_m230600625 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	typedef NetworkView_t172525251 * (*NetworkMessageInfo_NullNetworkView_m230600625_ftn) (NetworkMessageInfo_t614064059 *);
	static NetworkMessageInfo_NullNetworkView_m230600625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkMessageInfo_NullNetworkView_m230600625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkMessageInfo::NullNetworkView()");
	return _il2cpp_icall_func(__this);
}
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_NullNetworkView_m230600625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_NullNetworkView_m230600625(_thisAdjusted, method);
}
// System.Void UnityEngine.NetworkPlayer::.ctor(System.String,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1640566403;
extern const uint32_t NetworkPlayer__ctor_m3970319947_MetadataUsageId;
extern "C"  void NetworkPlayer__ctor_m3970319947 (NetworkPlayer_t1243528291 * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer__ctor_m3970319947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1640566403, /*hidden argument*/NULL);
		__this->set_index_0(0);
		return;
	}
}
extern "C"  void NetworkPlayer__ctor_m3970319947_AdjustorThunk (Il2CppObject * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	NetworkPlayer__ctor_m3970319947(_thisAdjusted, ___ip0, ___port1, method);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetIPAddress_m2394140501 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn) (int32_t);
	static NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
extern "C"  int32_t NetworkPlayer_Internal_GetPort_m3852797570 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPort_m3852797570_ftn) (int32_t);
	static NetworkPlayer_Internal_GetPort_m3852797570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPort_m3852797570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetExternalIP_m2789058499 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn) ();
	static NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetExternalPort_m2755622672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn) ();
	static NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalPort()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalIP_m2626871687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn) ();
	static NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetLocalPort_m2373269974 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn) ();
	static NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalPort()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
extern "C"  int32_t NetworkPlayer_Internal_GetPlayerIndex_m2582225917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn) ();
	static NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetGUID_m281231101 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetGUID_m281231101_ftn) (int32_t);
	static NetworkPlayer_Internal_GetGUID_m281231101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetGUID_m281231101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalGUID_m49665029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn) ();
	static NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalGUID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.NetworkPlayer::op_Equality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Equality_m2104137122 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___lhs0, NetworkPlayer_t1243528291  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_0017;
	}

IL_0017:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Inequality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Inequality_m3070881435 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___lhs0, NetworkPlayer_t1243528291  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.NetworkPlayer::GetHashCode()
extern "C"  int32_t NetworkPlayer_GetHashCode_m2788349399 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		int32_t L_1 = Int32_GetHashCode_m1381647448(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t NetworkPlayer_GetHashCode_m2788349399_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_GetHashCode_m2788349399(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkPlayer::Equals(System.Object)
extern Il2CppClass* NetworkPlayer_t1243528291_il2cpp_TypeInfo_var;
extern const uint32_t NetworkPlayer_Equals_m1960901089_MetadataUsageId;
extern "C"  bool NetworkPlayer_Equals_m1960901089 (NetworkPlayer_t1243528291 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer_Equals_m1960901089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	NetworkPlayer_t1243528291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkPlayer_t1243528291_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(NetworkPlayer_t1243528291 *)((NetworkPlayer_t1243528291 *)UnBox (L_1, NetworkPlayer_t1243528291_il2cpp_TypeInfo_var))));
		int32_t L_2 = (&V_1)->get_index_0();
		int32_t L_3 = __this->get_index_0();
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool NetworkPlayer_Equals_m1960901089_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_Equals_m1960901089(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.NetworkPlayer::get_ipAddress()
extern "C"  String_t* NetworkPlayer_get_ipAddress_m3020320555 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalIP_m2626871687(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetIPAddress_m2394140501(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
extern "C"  String_t* NetworkPlayer_get_ipAddress_m3020320555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_ipAddress_m3020320555(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_port()
extern "C"  int32_t NetworkPlayer_get_port_m2797602516 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = NetworkPlayer_Internal_GetLocalPort_m2373269974(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_index_0();
		int32_t L_4 = NetworkPlayer_Internal_GetPort_m3852797570(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
extern "C"  int32_t NetworkPlayer_get_port_m2797602516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_port_m2797602516(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_guid()
extern "C"  String_t* NetworkPlayer_get_guid_m1047639783 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalGUID_m49665029(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetGUID_m281231101(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
extern "C"  String_t* NetworkPlayer_get_guid_m1047639783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_guid_m1047639783(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::ToString()
extern "C"  String_t* NetworkPlayer_ToString_m537794039 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		String_t* L_1 = Int32_ToString_m2960866144(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
extern "C"  String_t* NetworkPlayer_ToString_m537794039_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_ToString_m537794039(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_externalIP()
extern "C"  String_t* NetworkPlayer_get_externalIP_m343056966 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkPlayer_Internal_GetExternalIP_m2789058499(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* NetworkPlayer_get_externalIP_m343056966_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_externalIP_m343056966(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_externalPort()
extern "C"  int32_t NetworkPlayer_get_externalPort_m114464875 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = NetworkPlayer_Internal_GetExternalPort_m2755622672(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t NetworkPlayer_get_externalPort_m114464875_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_externalPort_m114464875(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkPlayer::get_unassigned()
extern "C"  NetworkPlayer_t1243528291  NetworkPlayer_get_unassigned_m3961648431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkPlayer_t1243528291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		(&V_0)->set_index_0((-1));
		NetworkPlayer_t1243528291  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		NetworkPlayer_t1243528291  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t172525251 * NetworkView_Find_m143547961 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___viewID0, const MethodInfo* method)
{
	NetworkView_t172525251 * V_0 = NULL;
	{
		NetworkView_t172525251 * L_0 = NetworkView_INTERNAL_CALL_Find_m526018730(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		NetworkView_t172525251 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t172525251 * NetworkView_INTERNAL_CALL_Find_m526018730 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	typedef NetworkView_t172525251 * (*NetworkView_INTERNAL_CALL_Find_m526018730_ftn) (NetworkViewID_t3942988548 *);
	static NetworkView_INTERNAL_CALL_Find_m526018730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m526018730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t3942988548  NetworkViewID_get_unassigned_m2814913999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkViewID_t3942988548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		NetworkViewID_INTERNAL_get_unassigned_m132572206(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3942988548  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		NetworkViewID_t3942988548  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m132572206 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m763014699 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m89862041 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, NetworkPlayer_t1243528291 * ___player1, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, NetworkPlayer_t1243528291 * ___player1, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn) (NetworkViewID_t3942988548 *, NetworkPlayer_t1243528291 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m403971590 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m3248992772 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___lhs0, NetworkViewID_t3942988548 * ___rhs1, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn) (NetworkViewID_t3942988548 *, NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___lhs0, ___rhs1);
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m4173239775 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		NetworkViewID_t3942988548  L_0 = ___lhs0;
		NetworkViewID_t3942988548  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m309368134 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		NetworkViewID_t3942988548  L_0 = ___lhs0;
		NetworkViewID_t3942988548  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m3141878442 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m3141878442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_GetHashCode_m3141878442(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern Il2CppClass* NetworkViewID_t3942988548_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m809788370_MetadataUsageId;
extern "C"  bool NetworkViewID_Equals_m809788370 (NetworkViewID_t3942988548 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m809788370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	NetworkViewID_t3942988548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkViewID_t3942988548_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(NetworkViewID_t3942988548 *)((NetworkViewID_t3942988548 *)UnBox (L_1, NetworkViewID_t3942988548_il2cpp_TypeInfo_var))));
		NetworkViewID_t3942988548  L_2 = V_1;
		bool L_3 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool NetworkViewID_Equals_m809788370_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_Equals_m809788370(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m1234363003 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m763014699(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m1234363003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_get_isMine_m1234363003(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t1243528291  NetworkViewID_get_owner_m1900957708 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkPlayer_t1243528291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		NetworkViewID_Internal_GetOwner_m89862041(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t1243528291  L_0 = V_0;
		V_1 = L_0;
		goto IL_0015;
	}

IL_0015:
	{
		NetworkPlayer_t1243528291  L_1 = V_1;
		return L_1;
	}
}
extern "C"  NetworkPlayer_t1243528291  NetworkViewID_get_owner_m1900957708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_get_owner_m1900957708(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m3348378544 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m403971590(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m3348378544_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_ToString_m3348378544(_thisAdjusted, method);
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m197157284 (Object_t1021602117 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingle_m260620116 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingle_m260620116_ftn) (Object_t1021602117 *);
	static Object_Internal_CloneSingle_m260620116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m260620116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingleWithParent_m665572246 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingleWithParent_m665572246_ftn) (Object_t1021602117 *, Transform_t3275118058 *, bool);
	static Object_Internal_CloneSingleWithParent_m665572246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m665572246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	return _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingle_m2776302597 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t1021602117 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580 * ___pos1, Quaternion_t4030073918 * ___rot2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn) (Object_t1021602117 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingleWithParent_m509082884 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580  ___pos2, Quaternion_t4030073918  ___rot3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___data0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580 * ___pos2, Quaternion_t4030073918 * ___rot3, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn) (Object_t1021602117 *, Transform_t3275118058 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m3042842193 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m3042842193_ftn) (Object_t1021602117 *);
	static Object_EnsureRunningOnMainThread_m3042842193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m3042842193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m4279412553_ftn) (Object_t1021602117 *, float);
	static Object_Destroy_m4279412553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m4279412553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Destroy_m4145850038_MetadataUsageId;
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m4145850038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3563317232 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m3563317232_ftn) (Object_t1021602117 *, bool);
	static Object_DestroyImmediate_m3563317232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m3563317232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyImmediate_m95027445_MetadataUsageId;
extern "C"  void Object_DestroyImmediate_m95027445 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m95027445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t1021602117 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3563317232(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m2121813744 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfType_m2121813744_ftn) (Type_t *);
	static Object_FindObjectsOfType_m2121813744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m2121813744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m2079638459_ftn) (Object_t1021602117 *);
	static Object_get_name_m2079638459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m2079638459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m4157836998_ftn) (Object_t1021602117 *, String_t*);
	static Object_set_name_m4157836998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m4157836998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m2330762974_ftn) (Object_t1021602117 *);
	static Object_DontDestroyOnLoad_m2330762974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m2330762974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m4158950869 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m4158950869_ftn) (Object_t1021602117 *);
	static Object_get_hideFlags_m4158950869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m4158950869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2204253440 (Object_t1021602117 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m2204253440_ftn) (Object_t1021602117 *, int32_t);
	static Object_set_hideFlags_m2204253440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2204253440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m282495858 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m282495858_ftn) (Object_t1021602117 *, float);
	static Object_DestroyObject_m282495858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m282495858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyObject_m2343493981_MetadataUsageId;
extern "C"  void Object_DestroyObject_m2343493981 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m2343493981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m282495858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindSceneObjectsOfType_m1833688338 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindSceneObjectsOfType_m1833688338_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m1833688338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m1833688338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfTypeIncludingAssets_m3988851426 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m1947404527 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m1947404527_ftn) (Object_t1021602117 *);
	static Object_ToString_m1947404527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m1947404527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m2570795274 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m2570795274_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m2570795274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m2570795274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m1920497914_MetadataUsageId;
extern "C"  int32_t Object_GetInstanceID_m1920497914 (Object_t1021602117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m1920497914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_0032:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m39971741(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m3803259710(&L_8, ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = (*((int32_t*)L_9));
		goto IL_0056;
	}

IL_0056:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m3431642059 (Object_t1021602117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m1715190285(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Equals_m4029628913_MetadataUsageId;
extern "C"  bool Object_Equals_m4029628913 (Object_t1021602117 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m4029628913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = ___other0;
		V_0 = ((Object_t1021602117 *)IsInstClass(L_0, Object_t1021602117_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_4 = ___other0;
		if (((Object_t1021602117 *)IsInstClass(L_4, Object_t1021602117_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t1021602117 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Implicit_m2856731593_MetadataUsageId;
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m2856731593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_CompareBaseObjects_m3953996214_MetadataUsageId;
extern "C"  bool Object_CompareBaseObjects_m3953996214 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___lhs0, Object_t1021602117 * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3953996214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t1021602117 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t1021602117 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t1021602117 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t1021602117 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t1021602117 * L_10 = ___lhs0;
		Object_t1021602117 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m4056217615_MetadataUsageId;
extern "C"  bool Object_IsNativeObjectAlive_m4056217615 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m4056217615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m943750213(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m943750213 (Object_t1021602117 * __this, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		IntPtr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ScriptableObject_t1975622470_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern Il2CppCodeGenString* _stringLiteral1912870611;
extern const uint32_t Object_Instantiate_m938141395_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m938141395 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m938141395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		if (!((ScriptableObject_t1975622470 *)IsInstClass(L_1, ScriptableObject_t1975622470_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, _stringLiteral1912870611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t1021602117 * L_3 = ___original0;
		Vector3_t2243707580  L_4 = ___position1;
		Quaternion_t4030073918  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_6 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t1021602117 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2160322936_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2160322936 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2160322936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		Vector3_t2243707580  L_3 = ___position1;
		Quaternion_t4030073918  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_5 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0035;
	}

IL_001b:
	{
		Object_t1021602117 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_6, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_7 = ___original0;
		Transform_t3275118058 * L_8 = ___parent3;
		Vector3_t2243707580  L_9 = ___position1;
		Quaternion_t4030073918  L_10 = ___rotation2;
		Object_t1021602117 * L_11 = Object_Internal_InstantiateSingleWithParent_m509082884(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0035;
	}

IL_0035:
	{
		Object_t1021602117 * L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2439155489_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2439155489 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2439155489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		Object_t1021602117 * L_2 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_m2177117080_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2177117080 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2177117080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_Instantiate_m2489341053(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2489341053_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2489341053 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2489341053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0032;
	}

IL_0019:
	{
		Object_t1021602117 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_4, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_5 = ___original0;
		Transform_t3275118058 * L_6 = ___parent1;
		bool L_7 = ___instantiateInWorldSpace2;
		Object_t1021602117 * L_8 = Object_Internal_CloneSingleWithParent_m665572246(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0032;
	}

IL_0032:
	{
		Object_t1021602117 * L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Object_CheckNullArgument_m1711119106_MetadataUsageId;
extern "C"  void Object_CheckNullArgument_m1711119106 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m1711119106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_m2330404063_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_FindObjectOfType_m2330404063 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m2330404063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t4217747464* V_0 = NULL;
	Object_t1021602117 * V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t4217747464* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		ObjectU5BU5D_t4217747464* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		Object_t1021602117 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		goto IL_0021;
	}

IL_001a:
	{
		V_1 = (Object_t1021602117 *)NULL;
		goto IL_0021;
	}

IL_0021:
	{
		Object_t1021602117 * L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Equality_m3764089466_MetadataUsageId;
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m3764089466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Inequality_m2402264703_MetadataUsageId;
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m2402264703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object__cctor_m2991092887_MetadataUsageId;
extern "C"  void Object__cctor_m2991092887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m2991092887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_pinvoke_back(const Object_t1021602117_marshaled_pinvoke& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(Object_t1021602117_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_com_back(const Object_t1021602117_marshaled_com& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com_cleanup(Object_t1021602117_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.PreferBinarySerialization::.ctor()
extern "C"  void PreferBinarySerialization__ctor_m2043201510 (PreferBinarySerialization_t2472773525 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m580875098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m580875098_ftn) ();
	static QualitySettings_get_activeColorSpace_m580875098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m580875098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3196903881 (Quaternion_t4030073918 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m3196903881_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	Quaternion__ctor_m3196903881(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_Inverse_m3931399088 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Inverse_m1043108654(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1043108654 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t4030073918  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Internal_FromEulerRad_m1121344272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___euler0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn) (Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_op_Multiply_m2426727589 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t4030073918  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m3196903881(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t4030073918  L_33 = V_0;
		return L_33;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2270520528 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m3102305584(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m2270520528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_GetHashCode_m2270520528(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3730391696_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3730391696 (Quaternion_t4030073918 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3730391696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t4030073918_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox (L_1, Quaternion_t4030073918_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m3730391696_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_Equals_m3730391696(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587482509;
extern const uint32_t Quaternion_ToString_m2638853272_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m2638853272 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m2638853272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3587482509, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m2638853272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_ToString_m2638853272(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_direction_m4059191533(_thisAdjusted, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1807026812;
extern const uint32_t Ray_ToString_m2019179238_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m2019179238 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m2019179238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t2243707580  L_1 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		Vector3_t2243707580  L_5 = __this->get_m_Direction_1();
		Vector3_t2243707580  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1807026812, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m2019179238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_ToString_m2019179238(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m1220545469_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect__ctor_m1220545469(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m1393582490 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m1393582490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_x_m1393582490(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1393582395 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m1393582395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_y_m1393582395(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1138015702 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m1138015702_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_width_m1138015702(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m3128694305 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m3128694305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_height_m3128694305(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m1161102488 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m1161102488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMin_m1161102488(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1161103577 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m1161103577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMin_m1161103577(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2915145014 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m2915145014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMax_m2915145014(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2915146103 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m2915146103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMax_m2915146103(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1334685291 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m1334685291_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685291(_thisAdjusted, ___point0, method);
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m559954498 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3102305584((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3102305584((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3102305584((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3102305584((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m559954498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_GetHashCode_m559954498(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m3806390726_MetadataUsageId;
extern "C"  bool Rect_Equals_m3806390726 (Rect_t3681755626 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m3806390726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t3681755626_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox (L_1, Rect_t3681755626_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m3359827399((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m1393582395((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m3359827399((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m1138015702((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m3359827399((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m3359827399((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m3806390726_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Equals_m3806390726(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1853817013;
extern const uint32_t Rect_ToString_m2728794442_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2728794442 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2728794442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1853817013, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m2728794442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_ToString_m2728794442(_thisAdjusted, method);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m90487700 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t3349966182 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m1090213637(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m210072638 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m1090213637 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m1090213637((ReapplyDrivenProperties_t2020713228 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m2337529776 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m2375002944 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.RemoteSettings::CallOnUpdate()
extern Il2CppClass* RemoteSettings_t392466225_il2cpp_TypeInfo_var;
extern const uint32_t RemoteSettings_CallOnUpdate_m1624968574_MetadataUsageId;
extern "C"  void RemoteSettings_CallOnUpdate_m1624968574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_CallOnUpdate_m1624968574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdatedEventHandler_t3033456180 * V_0 = NULL;
	{
		UpdatedEventHandler_t3033456180 * L_0 = ((RemoteSettings_t392466225_StaticFields*)RemoteSettings_t392466225_il2cpp_TypeInfo_var->static_fields)->get_Updated_0();
		V_0 = L_0;
		UpdatedEventHandler_t3033456180 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UpdatedEventHandler_t3033456180 * L_2 = V_0;
		NullCheck(L_2);
		UpdatedEventHandler_Invoke_m159598802(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdatedEventHandler__ctor_m1393569768 (UpdatedEventHandler_t3033456180 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m159598802 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdatedEventHandler_Invoke_m159598802((UpdatedEventHandler_t3033456180 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.RemoteSettings/UpdatedEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdatedEventHandler_BeginInvoke_m4238510153 (UpdatedEventHandler_t3033456180 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdatedEventHandler_EndInvoke_m224684362 (UpdatedEventHandler_t3033456180 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
