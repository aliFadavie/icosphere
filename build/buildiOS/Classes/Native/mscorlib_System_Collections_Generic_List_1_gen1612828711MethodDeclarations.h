﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t2535834624;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t4014198702;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
struct ICollection_1_t3195782884;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558385.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C"  void List_1__ctor_m3511181530_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1__ctor_m3511181530(__this, method) ((  void (*) (List_1_t1612828711 *, const MethodInfo*))List_1__ctor_m3511181530_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m798345663_gshared (List_1_t1612828711 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m798345663(__this, ___collection0, method) ((  void (*) (List_1_t1612828711 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m798345663_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4213097859_gshared (List_1_t1612828711 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4213097859(__this, ___capacity0, method) ((  void (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1__ctor_m4213097859_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.cctor()
extern "C"  void List_1__cctor_m258195429_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m258195429(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m258195429_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3625278020_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3625278020(__this, method) ((  Il2CppObject* (*) (List_1_t1612828711 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3625278020_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3846687822_gshared (List_1_t1612828711 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3846687822(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1612828711 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3846687822_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1422822879_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1422822879(__this, method) ((  Il2CppObject * (*) (List_1_t1612828711 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1422822879_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1820917634_gshared (List_1_t1612828711 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1820917634(__this, ___item0, method) ((  int32_t (*) (List_1_t1612828711 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1820917634_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m780443244_gshared (List_1_t1612828711 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m780443244(__this, ___item0, method) ((  bool (*) (List_1_t1612828711 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m780443244_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3713885384_gshared (List_1_t1612828711 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3713885384(__this, ___item0, method) ((  int32_t (*) (List_1_t1612828711 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3713885384_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m941505143_gshared (List_1_t1612828711 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m941505143(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1612828711 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m941505143_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3763718607_gshared (List_1_t1612828711 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3763718607(__this, ___item0, method) ((  void (*) (List_1_t1612828711 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3763718607_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1453178827_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1453178827(__this, method) ((  bool (*) (List_1_t1612828711 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1453178827_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1804424478_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1804424478(__this, method) ((  Il2CppObject * (*) (List_1_t1612828711 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1804424478_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2378573511_gshared (List_1_t1612828711 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2378573511(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2378573511_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2480767060_gshared (List_1_t1612828711 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2480767060(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1612828711 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2480767060_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(T)
extern "C"  void List_1_Add_m1515754733_gshared (List_1_t1612828711 * __this, Vector2_t2243707579  ___item0, const MethodInfo* method);
#define List_1_Add_m1515754733(__this, ___item0, method) ((  void (*) (List_1_t1612828711 *, Vector2_t2243707579 , const MethodInfo*))List_1_Add_m1515754733_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2239402788_gshared (List_1_t1612828711 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2239402788(__this, ___newCount0, method) ((  void (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2239402788_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m767358372_gshared (List_1_t1612828711 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m767358372(__this, ___collection0, method) ((  void (*) (List_1_t1612828711 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m767358372_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1062096212_gshared (List_1_t1612828711 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1062096212(__this, ___enumerable0, method) ((  void (*) (List_1_t1612828711 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1062096212_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C"  void List_1_Clear_m3309552103_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_Clear_m3309552103(__this, method) ((  void (*) (List_1_t1612828711 *, const MethodInfo*))List_1_Clear_m3309552103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Contains(T)
extern "C"  bool List_1_Contains_m2079304621_gshared (List_1_t1612828711 * __this, Vector2_t2243707579  ___item0, const MethodInfo* method);
#define List_1_Contains_m2079304621(__this, ___item0, method) ((  bool (*) (List_1_t1612828711 *, Vector2_t2243707579 , const MethodInfo*))List_1_Contains_m2079304621_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1896864447_gshared (List_1_t1612828711 * __this, Vector2U5BU5D_t686124026* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1896864447(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1612828711 *, Vector2U5BU5D_t686124026*, int32_t, const MethodInfo*))List_1_CopyTo_m1896864447_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetEnumerator()
extern "C"  Enumerator_t1147558385  List_1_GetEnumerator_m3339340714_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3339340714(__this, method) ((  Enumerator_t1147558385  (*) (List_1_t1612828711 *, const MethodInfo*))List_1_GetEnumerator_m3339340714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2019441835_gshared (List_1_t1612828711 * __this, Vector2_t2243707579  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2019441835(__this, ___item0, method) ((  int32_t (*) (List_1_t1612828711 *, Vector2_t2243707579 , const MethodInfo*))List_1_IndexOf_m2019441835_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3083454298_gshared (List_1_t1612828711 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3083454298(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1612828711 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3083454298_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m402842271_gshared (List_1_t1612828711 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m402842271(__this, ___index0, method) ((  void (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1_CheckIndex_m402842271_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1176952016_gshared (List_1_t1612828711 * __this, int32_t ___index0, Vector2_t2243707579  ___item1, const MethodInfo* method);
#define List_1_Insert_m1176952016(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1612828711 *, int32_t, Vector2_t2243707579 , const MethodInfo*))List_1_Insert_m1176952016_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2220107869_gshared (List_1_t1612828711 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2220107869(__this, ___collection0, method) ((  void (*) (List_1_t1612828711 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2220107869_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Remove(T)
extern "C"  bool List_1_Remove_m1237648310_gshared (List_1_t1612828711 * __this, Vector2_t2243707579  ___item0, const MethodInfo* method);
#define List_1_Remove_m1237648310(__this, ___item0, method) ((  bool (*) (List_1_t1612828711 *, Vector2_t2243707579 , const MethodInfo*))List_1_Remove_m1237648310_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2331128844_gshared (List_1_t1612828711 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2331128844(__this, ___index0, method) ((  void (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2331128844_gshared)(__this, ___index0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C"  Vector2U5BU5D_t686124026* List_1_ToArray_m4024240619_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_ToArray_m4024240619(__this, method) ((  Vector2U5BU5D_t686124026* (*) (List_1_t1612828711 *, const MethodInfo*))List_1_ToArray_m4024240619_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2443158653_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2443158653(__this, method) ((  int32_t (*) (List_1_t1612828711 *, const MethodInfo*))List_1_get_Capacity_m2443158653_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3053659972_gshared (List_1_t1612828711 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3053659972(__this, ___value0, method) ((  void (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3053659972_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C"  int32_t List_1_get_Count_m1149731058_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
#define List_1_get_Count_m1149731058(__this, method) ((  int32_t (*) (List_1_t1612828711 *, const MethodInfo*))List_1_get_Count_m1149731058_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C"  Vector2_t2243707579  List_1_get_Item_m3661469506_gshared (List_1_t1612828711 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3661469506(__this, ___index0, method) ((  Vector2_t2243707579  (*) (List_1_t1612828711 *, int32_t, const MethodInfo*))List_1_get_Item_m3661469506_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3332810891_gshared (List_1_t1612828711 * __this, int32_t ___index0, Vector2_t2243707579  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3332810891(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1612828711 *, int32_t, Vector2_t2243707579 , const MethodInfo*))List_1_set_Item_m3332810891_gshared)(__this, ___index0, ___value1, method)
