﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t2535834625;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4014198703;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t3195782885;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558386.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m1739470559_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1__ctor_m1739470559(__this, method) ((  void (*) (List_1_t1612828712 *, const MethodInfo*))List_1__ctor_m1739470559_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1244050974_gshared (List_1_t1612828712 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1244050974(__this, ___collection0, method) ((  void (*) (List_1_t1612828712 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1244050974_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3997225032_gshared (List_1_t1612828712 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3997225032(__this, ___capacity0, method) ((  void (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1__ctor_m3997225032_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C"  void List_1__cctor_m2095067232_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2095067232(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2095067232_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2219076127_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2219076127(__this, method) ((  Il2CppObject* (*) (List_1_t1612828712 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2219076127_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1178805395_gshared (List_1_t1612828712 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1178805395(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1612828712 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1178805395_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m413886046_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m413886046(__this, method) ((  Il2CppObject * (*) (List_1_t1612828712 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m413886046_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2038396515_gshared (List_1_t1612828712 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2038396515(__this, ___item0, method) ((  int32_t (*) (List_1_t1612828712 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2038396515_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4009806475_gshared (List_1_t1612828712 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4009806475(__this, ___item0, method) ((  bool (*) (List_1_t1612828712 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4009806475_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2526560425_gshared (List_1_t1612828712 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2526560425(__this, ___item0, method) ((  int32_t (*) (List_1_t1612828712 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2526560425_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2469433788_gshared (List_1_t1612828712 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2469433788(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1612828712 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2469433788_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m4068476586_gshared (List_1_t1612828712 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m4068476586(__this, ___item0, method) ((  void (*) (List_1_t1612828712 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m4068476586_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2164218762_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2164218762(__this, method) ((  bool (*) (List_1_t1612828712 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2164218762_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1429670979_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1429670979(__this, method) ((  Il2CppObject * (*) (List_1_t1612828712 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1429670979_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3333265164_gshared (List_1_t1612828712 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3333265164(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3333265164_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1722990777_gshared (List_1_t1612828712 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1722990777(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1612828712 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1722990777_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C"  void List_1_Add_m1552520334_gshared (List_1_t1612828712 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define List_1_Add_m1552520334(__this, ___item0, method) ((  void (*) (List_1_t1612828712 *, Vector3_t2243707580 , const MethodInfo*))List_1_Add_m1552520334_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3656820735_gshared (List_1_t1612828712 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3656820735(__this, ___newCount0, method) ((  void (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3656820735_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m257454527_gshared (List_1_t1612828712 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m257454527(__this, ___collection0, method) ((  void (*) (List_1_t1612828712 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m257454527_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m36504111_gshared (List_1_t1612828712 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m36504111(__this, ___enumerable0, method) ((  void (*) (List_1_t1612828712 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m36504111_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C"  void List_1_Clear_m1701318956_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_Clear_m1701318956(__this, method) ((  void (*) (List_1_t1612828712 *, const MethodInfo*))List_1_Clear_m1701318956_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C"  bool List_1_Contains_m1363332942_gshared (List_1_t1612828712 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define List_1_Contains_m1363332942(__this, ___item0, method) ((  bool (*) (List_1_t1612828712 *, Vector3_t2243707580 , const MethodInfo*))List_1_Contains_m1363332942_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2750189956_gshared (List_1_t1612828712 * __this, Vector3U5BU5D_t1172311765* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2750189956(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1612828712 *, Vector3U5BU5D_t1172311765*, int32_t, const MethodInfo*))List_1_CopyTo_m2750189956_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C"  Enumerator_t1147558386  List_1_GetEnumerator_m1075582447_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1075582447(__this, method) ((  Enumerator_t1147558386  (*) (List_1_t1612828712 *, const MethodInfo*))List_1_GetEnumerator_m1075582447_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1520537898_gshared (List_1_t1612828712 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1520537898(__this, ___item0, method) ((  int32_t (*) (List_1_t1612828712 *, Vector3_t2243707580 , const MethodInfo*))List_1_IndexOf_m1520537898_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3453072415_gshared (List_1_t1612828712 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3453072415(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1612828712 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3453072415_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m483190820_gshared (List_1_t1612828712 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m483190820(__this, ___index0, method) ((  void (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_CheckIndex_m483190820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m432478581_gshared (List_1_t1612828712 * __this, int32_t ___index0, Vector3_t2243707580  ___item1, const MethodInfo* method);
#define List_1_Insert_m432478581(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1612828712 *, int32_t, Vector3_t2243707580 , const MethodInfo*))List_1_Insert_m432478581_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m818371234_gshared (List_1_t1612828712 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m818371234(__this, ___collection0, method) ((  void (*) (List_1_t1612828712 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m818371234_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C"  bool List_1_Remove_m1738717045_gshared (List_1_t1612828712 * __this, Vector3_t2243707580  ___item0, const MethodInfo* method);
#define List_1_Remove_m1738717045(__this, ___item0, method) ((  bool (*) (List_1_t1612828712 *, Vector3_t2243707580 , const MethodInfo*))List_1_Remove_m1738717045_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2929488689_gshared (List_1_t1612828712 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2929488689(__this, ___index0, method) ((  void (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2929488689_gshared)(__this, ___index0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t1172311765* List_1_ToArray_m2543904144_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_ToArray_m2543904144(__this, method) ((  Vector3U5BU5D_t1172311765* (*) (List_1_t1612828712 *, const MethodInfo*))List_1_ToArray_m2543904144_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3166663676_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3166663676(__this, method) ((  int32_t (*) (List_1_t1612828712 *, const MethodInfo*))List_1_get_Capacity_m3166663676_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1734764639_gshared (List_1_t1612828712 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1734764639(__this, ___value0, method) ((  void (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1734764639_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C"  int32_t List_1_get_Count_m40859504_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
#define List_1_get_Count_m40859504(__this, method) ((  int32_t (*) (List_1_t1612828712 *, const MethodInfo*))List_1_get_Count_m40859504_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C"  Vector3_t2243707580  List_1_get_Item_m4187614919_gshared (List_1_t1612828712 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4187614919(__this, ___index0, method) ((  Vector3_t2243707580  (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_get_Item_m4187614919_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3999530054_gshared (List_1_t1612828712 * __this, int32_t ___index0, Vector3_t2243707580  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3999530054(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1612828712 *, int32_t, Vector3_t2243707580 , const MethodInfo*))List_1_set_Item_m3999530054_gshared)(__this, ___index0, ___value1, method)
